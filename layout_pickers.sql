-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2021 at 05:34 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `layout_pickers`
--

-- --------------------------------------------------------

--
-- Table structure for table `layout_concept`
--

CREATE TABLE `layout_concept` (
  `layout_concept_id` varchar(15) NOT NULL,
  `information_id` varchar(15) NOT NULL,
  `trimline_2d` varchar(200) NOT NULL,
  `mechline_2d` varchar(200) NOT NULL,
  `trimline_3d` varchar(200) NOT NULL,
  `mechline_3d` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `layout_concept`
--

INSERT INTO `layout_concept` (`layout_concept_id`, `information_id`, `trimline_2d`, `mechline_2d`, `trimline_3d`, `mechline_3d`) VALUES
('2fLyGIPgpWXkrxi', 'F8PsxM2zeAu4pSG', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('2LwK8QBr9hIxNeY', 'pJnWKfPEVH18sAj', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('3ktDeTH6Gixozub', 'ojVAemZq7FtCvE0', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('3PAdyiQm2aDoEcZ', '6uNU0iC4LYvbpz2', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('3VwFLet1R5r4b0T', 'LuB0DMyAcQPF4fI', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('5GYbOU0mwSq2zt8', 'Fk85ji7wsX0MWz4', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('6y9WCUGuDOXAzch', 'eHTwPCZ31dxbqW9', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('9K5WHsz0Enc7QLk', 'krnBsRMG2IEyL6K', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('9lKU5Tu4D1r7Yaf', 'Es9UDNT6p4xWrJq', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('9s4ntxklebVBQy5', 'uDQdcafLZgj9ERO', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('A1NS8neIap3wZLq', '4vxwteWcJZ0Bya3', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('a5fLz8VFAIl7kpg', 'BJoPd5H2batzTIQ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('aKej23yk65dn4fE', 'ezlnv32uWMpUXVD', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('amPjp7NlK6dSbq5', 'xYyJKspSX1OAZ6T', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('c034n2aZmWAxgFN', '0RNvKLrHSm8uVXO', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('Cp0TmKZtDyRSNX4', 'gvZY0IAQaLrblUJ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('cqu1Lw6fH5YzbMI', '9oRFWUmeG5zcnHg', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('DJ6RlgWHGh9zZnQ', 'YeQ5sLCoNuh297q', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('FftVwDcrNqn5Uoz', 'pFlIM1Po5zGBurW', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('fykdC2UjgQJXupE', 'Divfzo1VaO4kspI', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('GI2zHZndlMv0JcU', 'UdhG8CKkj94lLui', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('H14iZsEnOCDk8NI', 'H7G1DIAczNyM64w', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('h9vXZrjnOCPql2N', 'vBzMyY2WHJhPxiZ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('HkOv9miyWAgh01L', 'sDK2UItT9XGaEzZ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('HnNIz0Z7gjJMaCK', 'jVUtG5M8peS9CRN', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('In0loAjKRxbMCgd', '4In6VgpvQe5Azi2', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('inydb3PK1QVBjCf', 'DhiQPnH1mUtVgMk', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('IzKSihGFvwXjqZJ', 'glUYmNeJvzbTVju', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('jDgEqPvB4MOTmFG', 'z2WwsMXtIEfaFbB', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('JEgK50uS7YkfaUM', 'c0v8ei4WMsqAP6f', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('jFksr9Xi3Lv2u1b', 'BNTUcE4zwgaOrHd', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('Kqoxe7dkvcD4m1L', 'EjtJAChKZ5O40lW', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('KSBLMJkEPsjt4Gp', '4Gc1UdsiS87LBWp', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('Loplex8JNsYbihB', 'MS5jCcr8PGk7QyB', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('MkLqFWXKpzxvAR8', 'jRNo2gGCsiWvbeO', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('Mn83xTHDrCELO12', 'NCBn7vO9U2hI45k', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('mOoQBCInf5aVAWD', 'uRqQzrjKaETeFWJ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('mP0roXpeFgQiOks', 'xM7Xs50n6biyDZl', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('MXJ3oHdsINWkeB9', 'q6fctM2Lah3yNUX', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('mZnLqGOHyA52IgM', 'f6FsdtPXDEk7GOy', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('NkOPJT0EAU8LC5y', 'Mlq9Qt0rTXeK2PC', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('Nn47HQSdJUgtCDV', 'sT7Q4VpxLjzySW3', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('ONnG9Pskz4b3FLJ', 'gajZAuznbr5EvGQ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('PqTD7NFrz9O6oeW', 'a6Q4IdfyYB8W1Zr', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('PZnxW7TcvlEmwBs', 'EeGawDFTxkoMQYv', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('QeVyK63Jd2trhfz', 'uZxJOD0iIlLd1H3', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('qpBQHDNTLuWFGs6', 'HQ1wSTY9ln2i5rq', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('RJnK6dg1DkFtwBz', '8hnxqGtsNyKjraQ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('sSuHJVTyYqFt7QR', 'On20xZmHL1TKjru', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('SWHsAc8aDnTqFud', 'FpwctGh4NSOEbWz', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('t7dFkPj9fe0HE1u', '2FxoKyUYSq0TLBc', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('tMupwIdH9Lrk5XF', 'OZbI3fRwqcWMKxn', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('TyPBl9rGodbUtNW', 'LhdinCV1I2oQtwU', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('u4WxJcGXCbK36qf', 'sQ8DeEfpxqYwzKC', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('Uvd3y0mtElNaFGW', 'CNa8cDE5QgKpkV2', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('VqLOtmi5ukTvjYZ', '0eyIJsZP9wNfOxM', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('VUNhECKpS45FBYe', 'lnGKmaYE5fSw7xZ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('w7brl1Es3G84Ogm', 'MwS7vk6AE5mbqVI', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('x50MkL7WN6XuRiU', 'ogv6CJcNbYWOins', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('XBa046fIM8uKC1y', 'uScCnmVprhQZeox', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('xj3koCKu69v1d7I', 'la3bXfmeRnNHtMr', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('XlixtY5I3cVamk1', 'nsW0ObhmUl8oYKJ', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('XmPsdB632UpujM9', 'JdTBHiamwIl74gr', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('XUQFnoxSwA6DfuN', 'ZnTs70YtgiquhrV', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('YORCgIlue8vH7ZJ', 'FQXDkBdYUmSWocn', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('Yr2EdlUeg3mCJvy', 'nm4SQ6EcUWlPhOp', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('YsEBGmNQHScuZqr', 'xsdte3Gl1D9nZEU', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('YTfaRZU2PoLw3GJ', 'mG8tqN2b3jZB6oW', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('z5Lcd9gDxamRfXW', 'IC0cFvoDgtj6TzV', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('ZHYmvFP2Ip0wbDu', 'M0vJ58lrIbSPoHV', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png'),
('zu9BeZUVOA1hRaW', 'YqLp39i8rFNg0UO', 'trimline_2d.png', 'mechline_2d.png', 'trimline_3d.png', 'mechline_3d.png');

-- --------------------------------------------------------

--
-- Table structure for table `layout_information`
--

CREATE TABLE `layout_information` (
  `information_id` varchar(200) NOT NULL,
  `layout_id` varchar(200) NOT NULL,
  `url_information` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `layout_information`
--

INSERT INTO `layout_information` (`information_id`, `layout_id`, `url_information`) VALUES
('0eyIJsZP9wNfOxM', 'jXty1lHrD8OeQUM', 'database/asset/layout/jXty1lHrD8OeQUM/'),
('0RNvKLrHSm8uVXO', 'fPgdZNKYhmL5oaq', 'database/asset/layout/fPgdZNKYhmL5oaq/'),
('2FxoKyUYSq0TLBc', 'y2OP3ABw0ED8MK4', 'database/asset/layout/y2OP3ABw0ED8MK4/'),
('4Gc1UdsiS87LBWp', 'fU7DupLSKX4atvP', 'database/asset/layout/fU7DupLSKX4atvP/'),
('4In6VgpvQe5Azi2', 'YHmibqnkAX8MJgv', 'database/asset/layout/YHmibqnkAX8MJgv/'),
('4vxwteWcJZ0Bya3', 'NYCJA7gH13qMUnG', 'database/asset/layout/NYCJA7gH13qMUnG/'),
('6uNU0iC4LYvbpz2', 'rwXJfHDP1YQTZIp', 'database/asset/layout/rwXJfHDP1YQTZIp/'),
('8hnxqGtsNyKjraQ', 'PAG5kubRTgeh0la', 'database/asset/layout/PAG5kubRTgeh0la/'),
('9oRFWUmeG5zcnHg', 'qcpSiLUP4CrFbHB', 'database/asset/layout/qcpSiLUP4CrFbHB/'),
('a6Q4IdfyYB8W1Zr', 'LypHFxRdKSUWb15', 'database/asset/layout/LypHFxRdKSUWb15/'),
('BJoPd5H2batzTIQ', 'bTp5A0f2GONux6F', 'database/asset/layout/bTp5A0f2GONux6F/'),
('BNTUcE4zwgaOrHd', 'g0t1fLV7QYNz5XJ', 'database/asset/layout/g0t1fLV7QYNz5XJ/'),
('c0v8ei4WMsqAP6f', 'c1OmyUHYguN2TRp', 'database/asset/layout/c1OmyUHYguN2TRp/'),
('CNa8cDE5QgKpkV2', 'WfKFsJX43Ej6qPt', 'database/asset/layout/WfKFsJX43Ej6qPt/'),
('DhiQPnH1mUtVgMk', 'L9oS45Q286JpKrl', 'database/asset/layout/L9oS45Q286JpKrl/'),
('Divfzo1VaO4kspI', '6JAY1QCg2FPvysw', 'database/asset/layout/6JAY1QCg2FPvysw/'),
('EeGawDFTxkoMQYv', 'nCWStUG7q2Dj5Iw', 'database/asset/layout/nCWStUG7q2Dj5Iw/'),
('eHTwPCZ31dxbqW9', 'd6FnQb943YqETJC', 'database/asset/layout/d6FnQb943YqETJC/'),
('EjtJAChKZ5O40lW', 'Kq2hk9lFH5IQeMP', 'database/asset/layout/Kq2hk9lFH5IQeMP/'),
('Es9UDNT6p4xWrJq', '4SVyJln9cWqpxsm', 'database/asset/layout/4SVyJln9cWqpxsm/'),
('ezlnv32uWMpUXVD', 'DUWoFfY9aJnmBtA', 'database/asset/layout/DUWoFfY9aJnmBtA/'),
('f6FsdtPXDEk7GOy', 'tUWTlz0SZAd91JN', 'database/asset/layout/tUWTlz0SZAd91JN/'),
('F8PsxM2zeAu4pSG', '5PnDF9mYTGEdZkh', 'database/asset/layout/5PnDF9mYTGEdZkh/'),
('Fk85ji7wsX0MWz4', 'XwrsTeY2Mj8b9iS', 'database/asset/layout/XwrsTeY2Mj8b9iS/'),
('FpwctGh4NSOEbWz', 'm8tvebgn3uLFQAT', 'database/asset/layout/m8tvebgn3uLFQAT/'),
('FQXDkBdYUmSWocn', 'JRHj89ytG02CSLq', 'database/asset/layout/JRHj89ytG02CSLq/'),
('gajZAuznbr5EvGQ', '3Xs6jMd2BtZbrKL', 'database/asset/layout/3Xs6jMd2BtZbrKL/'),
('glUYmNeJvzbTVju', 'gvAfbxCHniFSh6k', 'database/asset/layout/gvAfbxCHniFSh6k/'),
('gvZY0IAQaLrblUJ', 'XLI6z0schMqZlwN', 'database/asset/layout/XLI6z0schMqZlwN/'),
('H7G1DIAczNyM64w', 'zcTZhtOF2XK3YW0', 'database/asset/layout/zcTZhtOF2XK3YW0/'),
('HQ1wSTY9ln2i5rq', 'b3RgjA4moV0s7iH', 'database/asset/layout/b3RgjA4moV0s7iH/'),
('IC0cFvoDgtj6TzV', '1WAk53veErVQLGn', 'database/asset/layout/1WAk53veErVQLGn/'),
('JdTBHiamwIl74gr', 'JhbsX2MBWkKUcRx', 'database/asset/layout/JhbsX2MBWkKUcRx/'),
('jRNo2gGCsiWvbeO', 'jlJM3Y6kSBgmqxs', 'database/asset/layout/jlJM3Y6kSBgmqxs/'),
('jVUtG5M8peS9CRN', 'wkVOv2hpPlDoWT5', 'database/asset/layout/wkVOv2hpPlDoWT5/'),
('krnBsRMG2IEyL6K', 'iGkL8dmT2hjKbMl', 'database/asset/layout/iGkL8dmT2hjKbMl/'),
('la3bXfmeRnNHtMr', 'Pr0SuHOQ78btJsE', 'database/asset/layout/Pr0SuHOQ78btJsE/'),
('LhdinCV1I2oQtwU', '5YdfeEMrXzRkOTH', 'database/asset/layout/5YdfeEMrXzRkOTH/'),
('lnGKmaYE5fSw7xZ', '4GEeut6MJw7hkLI', 'database/asset/layout/4GEeut6MJw7hkLI/'),
('LuB0DMyAcQPF4fI', 'aB1QwkY6HjDluVA', 'database/asset/layout/aB1QwkY6HjDluVA/'),
('M0vJ58lrIbSPoHV', 'VniPmSgwcIRpZed', 'database/asset/layout/VniPmSgwcIRpZed/'),
('mG8tqN2b3jZB6oW', 'PB2JpG8KlgoOqnv', 'database/asset/layout/PB2JpG8KlgoOqnv/'),
('Mlq9Qt0rTXeK2PC', 'KAIHEdtXJCi4e3Y', 'database/asset/layout/KAIHEdtXJCi4e3Y/'),
('MS5jCcr8PGk7QyB', 'aLvQtZ1oIzSGdmb', 'database/asset/layout/aLvQtZ1oIzSGdmb/'),
('MwS7vk6AE5mbqVI', 'ei348mapHNvQxFy', 'database/asset/layout/ei348mapHNvQxFy/'),
('NCBn7vO9U2hI45k', 'eLAEHcWriQa3XKN', 'database/asset/layout/eLAEHcWriQa3XKN/'),
('nm4SQ6EcUWlPhOp', '4yaw52CDG0nbLq1', 'database/asset/layout/4yaw52CDG0nbLq1/'),
('nsW0ObhmUl8oYKJ', 'fsebTvwjCBqd73t', 'database/asset/layout/fsebTvwjCBqd73t/'),
('ogv6CJcNbYWOins', 'ho97VestRPvfOrS', 'database/asset/layout/ho97VestRPvfOrS/'),
('ojVAemZq7FtCvE0', 'dZsqvbA6KicSOwE', 'database/asset/layout/dZsqvbA6KicSOwE/'),
('On20xZmHL1TKjru', 'jlZQsh80pgKkwAD', 'database/asset/layout/jlZQsh80pgKkwAD/'),
('OZbI3fRwqcWMKxn', 'g6KjfPeUZBsk5LG', 'database/asset/layout/g6KjfPeUZBsk5LG/'),
('pFlIM1Po5zGBurW', 'vWxunT1FzURHhVr', 'database/asset/layout/vWxunT1FzURHhVr/'),
('pJnWKfPEVH18sAj', 'ChEcMaeRzTAIkG0', 'database/asset/layout/ChEcMaeRzTAIkG0/'),
('q6fctM2Lah3yNUX', 'Yd1GCkcBIO9E3RQ', 'database/asset/layout/Yd1GCkcBIO9E3RQ/'),
('sDK2UItT9XGaEzZ', 'NG1akrozjXnBwcF', 'database/asset/layout/NG1akrozjXnBwcF/'),
('sQ8DeEfpxqYwzKC', 'bp7TeyD9X2kxrtA', 'database/asset/layout/bp7TeyD9X2kxrtA/'),
('sT7Q4VpxLjzySW3', 'NALuQqwlBo4zdsm', 'database/asset/layout/NALuQqwlBo4zdsm/'),
('UdhG8CKkj94lLui', 'pSHn8s4m29WvOXL', 'database/asset/layout/pSHn8s4m29WvOXL/'),
('uDQdcafLZgj9ERO', '2oEU95Rs0aYdw8u', 'database/asset/layout/2oEU95Rs0aYdw8u/'),
('uRqQzrjKaETeFWJ', 'k4Ao7vwMgq6Gans', 'database/asset/layout/k4Ao7vwMgq6Gans/'),
('uScCnmVprhQZeox', 'jQqLd8UFhKJSwD4', 'database/asset/layout/jQqLd8UFhKJSwD4/'),
('uZxJOD0iIlLd1H3', 'xXuoQ9eha4mkpiD', 'database/asset/layout/xXuoQ9eha4mkpiD/'),
('vBzMyY2WHJhPxiZ', 'zmeLbMAkfKU4ujd', 'database/asset/layout/zmeLbMAkfKU4ujd/'),
('xM7Xs50n6biyDZl', 'YanWOQFD3U9rsqH', 'database/asset/layout/YanWOQFD3U9rsqH/'),
('xsdte3Gl1D9nZEU', 'mGbPI4wUJtWrZnB', 'database/asset/layout/mGbPI4wUJtWrZnB/'),
('xYyJKspSX1OAZ6T', 'gnhYOcbemf8HaFC', 'database/asset/layout/gnhYOcbemf8HaFC/'),
('YeQ5sLCoNuh297q', 'gIKwGLd8Rofkyj5', 'database/asset/layout/gIKwGLd8Rofkyj5/'),
('YqLp39i8rFNg0UO', 'Lg0h1HpqucFIOnt', 'database/asset/layout/Lg0h1HpqucFIOnt/'),
('z2WwsMXtIEfaFbB', 'yszap8EjcVGbi2n', 'database/asset/layout/yszap8EjcVGbi2n/'),
('ZnTs70YtgiquhrV', 'L4oqbXBcfGdPA9Q', 'database/asset/layout/L4oqbXBcfGdPA9Q/');

-- --------------------------------------------------------

--
-- Table structure for table `layout_uploaded`
--

CREATE TABLE `layout_uploaded` (
  `layout_id` varchar(200) NOT NULL,
  `user_id` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL,
  `trimline` varchar(200) NOT NULL,
  `mechline` varchar(200) NOT NULL,
  `final_mechline` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `jph` float NOT NULL,
  `option_layout` varchar(10) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `layout_uploaded`
--

INSERT INTO `layout_uploaded` (`layout_id`, `user_id`, `url`, `trimline`, `mechline`, `final_mechline`, `type`, `jph`, `option_layout`, `date`) VALUES
('1WAk53veErVQLGn', '1', 'database/asset/layout/1WAk53veErVQLGn/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MRA,MFA,MHA,PIH', 6, '2', '2021-01-03'),
('2oEU95Rs0aYdw8u', '1', 'database/asset/layout/2oEU95Rs0aYdw8u/', 'trimline.png', 'mechline.png', '', 'MFA', 1.5, '1', '2021-01-03'),
('3Xs6jMd2BtZbrKL', '1', 'database/asset/layout/3Xs6jMd2BtZbrKL/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'PIH', 6, '2', '2021-01-03'),
('4GEeut6MJw7hkLI', '1', 'database/asset/layout/4GEeut6MJw7hkLI/', 'trimline.png', 'mechline.png', '', 'MHA,PIH', 3, '2', '2021-01-03'),
('4SVyJln9cWqpxsm', '1', 'database/asset/layout/4SVyJln9cWqpxsm/', 'trimline.png', 'mechline.png', '', 'MHA', 3, '2', '2021-01-03'),
('4yaw52CDG0nbLq1', '1', 'database/asset/layout/4yaw52CDG0nbLq1/', 'trimline.png', 'mechline.png', '', 'MFA,MHA', 6, '1', '2021-01-03'),
('5PnDF9mYTGEdZkh', '1', 'database/asset/layout/5PnDF9mYTGEdZkh/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MFA,MHA,PIH', 6, '2', '2021-01-03'),
('5YdfeEMrXzRkOTH', '1', 'database/asset/layout/5YdfeEMrXzRkOTH/', 'trimline.png', 'mechline.png', '', 'PIH', 1.5, '1', '2021-01-03'),
('6JAY1QCg2FPvysw', '1', 'database/asset/layout/6JAY1QCg2FPvysw/', 'trimline.png', 'mechline.png', '', 'MFA,PIH', 6, '1', '2021-01-03'),
('aB1QwkY6HjDluVA', '1', 'database/asset/layout/aB1QwkY6HjDluVA/', 'trimline.png', 'mechline.png', '', 'MFA,MHA', 1.5, '1', '2021-01-03'),
('aLvQtZ1oIzSGdmb', '1', 'database/asset/layout/aLvQtZ1oIzSGdmb/', 'trimline.png', 'mechline.png', '', 'MHA,PIH', 3, '1', '2021-01-03'),
('b3RgjA4moV0s7iH', '1', 'database/asset/layout/b3RgjA4moV0s7iH/', 'trimline.png', 'mechline.png', '', 'MFA', 3, '1', '2021-01-03'),
('bp7TeyD9X2kxrtA', '1', 'database/asset/layout/bp7TeyD9X2kxrtA/', 'trimline.png', 'mechline.png', '', 'MFA,MHA', 3, '1', '2021-01-03'),
('bTp5A0f2GONux6F', '1', 'database/asset/layout/bTp5A0f2GONux6F/', 'trimline.png', 'mechline.png', '', 'MFA', 6, '1', '2021-01-03'),
('c1OmyUHYguN2TRp', '1', 'database/asset/layout/c1OmyUHYguN2TRp/', 'trimline.png', 'mechline.png', '', 'MRA,MFA', 6, '1', '2021-01-03'),
('ChEcMaeRzTAIkG0', '1', 'database/asset/layout/ChEcMaeRzTAIkG0/', 'trimline.png', 'mechline.png', '', 'MRA', 3, '2', '2021-01-03'),
('d6FnQb943YqETJC', '1', 'database/asset/layout/d6FnQb943YqETJC/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MFA', 6, '2', '2021-01-03'),
('DUWoFfY9aJnmBtA', '1', 'database/asset/layout/DUWoFfY9aJnmBtA/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA', 3, '1', '2021-01-03'),
('dZsqvbA6KicSOwE', '1', 'database/asset/layout/dZsqvbA6KicSOwE/', 'trimline.png', 'mechline.png', '', 'PIH', 6, '1', '2021-01-03'),
('ei348mapHNvQxFy', '1', 'database/asset/layout/ei348mapHNvQxFy/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MRA,MFA,MHA', 6, '2', '2021-01-03'),
('eLAEHcWriQa3XKN', '1', 'database/asset/layout/eLAEHcWriQa3XKN/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MRA,MFA,PIH', 6, '2', '2021-01-03'),
('fPgdZNKYhmL5oaq', '1', 'database/asset/layout/fPgdZNKYhmL5oaq/', 'trimline.png', 'mechline.png', '', 'MRA,PIH', 1.5, '1', '2021-01-03'),
('fsebTvwjCBqd73t', '1', 'database/asset/layout/fsebTvwjCBqd73t/', 'trimline.png', 'mechline.png', '', 'MFA,PIH', 3, '2', '2021-01-03'),
('fU7DupLSKX4atvP', '1', 'database/asset/layout/fU7DupLSKX4atvP/', 'trimline.png', 'mechline.png', '', 'MFA', 3, '2', '2021-01-03'),
('g0t1fLV7QYNz5XJ', '1', 'database/asset/layout/g0t1fLV7QYNz5XJ/', 'trimline.png', 'mechline.png', '', 'MRA', 3, '1', '2021-01-03'),
('g6KjfPeUZBsk5LG', '1', 'database/asset/layout/g6KjfPeUZBsk5LG/', 'trimline.png', 'mechline.png', '', 'MRA,MFA', 3, '1', '2021-01-03'),
('gIKwGLd8Rofkyj5', '1', 'database/asset/layout/gIKwGLd8Rofkyj5/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MFA,MHA', 6, '2', '2021-01-03'),
('gnhYOcbemf8HaFC', '1', 'database/asset/layout/gnhYOcbemf8HaFC/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA,PIH', 3, '2', '2021-01-03'),
('gvAfbxCHniFSh6k', '1', 'database/asset/layout/gvAfbxCHniFSh6k/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,PIH', 3, '2', '2021-01-03'),
('iGkL8dmT2hjKbMl', '1', 'database/asset/layout/iGkL8dmT2hjKbMl/', 'trimline.png', 'mechline.png', '', 'MRA,MHA', 6, '1', '2021-01-03'),
('JhbsX2MBWkKUcRx', '1', 'database/asset/layout/JhbsX2MBWkKUcRx/', 'trimline.png', 'mechline.png', '', 'PIH', 3, '2', '2021-01-03'),
('jlJM3Y6kSBgmqxs', '1', 'database/asset/layout/jlJM3Y6kSBgmqxs/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MRA', 6, '2', '2021-01-03'),
('jlZQsh80pgKkwAD', '1', 'database/asset/layout/jlZQsh80pgKkwAD/', 'trimline.png', 'mechline.png', '', 'MFA,MHA,PIH', 1.5, '1', '2021-01-03'),
('jQqLd8UFhKJSwD4', '1', 'database/asset/layout/jQqLd8UFhKJSwD4/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MHA,PIH', 6, '2', '2021-01-03'),
('JRHj89ytG02CSLq', '1', 'database/asset/layout/JRHj89ytG02CSLq/', 'trimline.png', 'mechline.png', '', 'MFA,MHA,PIH', 6, '1', '2021-01-03'),
('jXty1lHrD8OeQUM', '1', 'database/asset/layout/jXty1lHrD8OeQUM/', 'trimline.png', 'mechline.png', '', 'MRA,PIH', 3, '2', '2021-01-03'),
('k4Ao7vwMgq6Gans', '1', 'database/asset/layout/k4Ao7vwMgq6Gans/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MRA,MFA', 6, '2', '2021-01-03'),
('KAIHEdtXJCi4e3Y', '1', 'database/asset/layout/KAIHEdtXJCi4e3Y/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MHA', 6, '2', '2021-01-03'),
('Kq2hk9lFH5IQeMP', '1', 'database/asset/layout/Kq2hk9lFH5IQeMP/', 'trimline.png', 'mechline.png', '', 'MFA,PIH', 1.5, '1', '2021-01-03'),
('L4oqbXBcfGdPA9Q', '1', 'database/asset/layout/L4oqbXBcfGdPA9Q/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA,PIH', 1.5, '1', '2021-01-03'),
('L9oS45Q286JpKrl', '1', 'database/asset/layout/L9oS45Q286JpKrl/', 'trimline.png', 'mechline.png', '', 'MHA', 3, '1', '2021-01-03'),
('Lg0h1HpqucFIOnt', '1', 'database/asset/layout/Lg0h1HpqucFIOnt/', 'trimline.png', 'mechline.png', '', 'MHA,PIH', 6, '1', '2021-01-03'),
('LypHFxRdKSUWb15', '1', 'database/asset/layout/LypHFxRdKSUWb15/', 'trimline.png', 'mechline.png', '', 'MFA,MHA', 3, '2', '2021-01-03'),
('m8tvebgn3uLFQAT', '1', 'database/asset/layout/m8tvebgn3uLFQAT/', 'trimline.png', 'mechline.png', '', 'MFA,MHA,PIH', 3, '2', '2021-01-03'),
('mGbPI4wUJtWrZnB', '1', 'database/asset/layout/mGbPI4wUJtWrZnB/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA', 1.5, '1', '2021-01-03'),
('NALuQqwlBo4zdsm', '1', 'database/asset/layout/NALuQqwlBo4zdsm/', 'trimline.png', 'mechline.png', '', 'MHA', 1.5, '1', '2021-01-03'),
('nCWStUG7q2Dj5Iw', '1', 'database/asset/layout/nCWStUG7q2Dj5Iw/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA', 3, '2', '2021-01-03'),
('NG1akrozjXnBwcF', '1', 'database/asset/layout/NG1akrozjXnBwcF/', 'trimline.png', 'mechline.png', '', 'MHA,PIH', 1.5, '1', '2021-01-03'),
('NYCJA7gH13qMUnG', '1', 'database/asset/layout/NYCJA7gH13qMUnG/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MRA,MHA', 6, '2', '2021-01-03'),
('PAG5kubRTgeh0la', '1', 'database/asset/layout/PAG5kubRTgeh0la/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,PIH', 6, '1', '2021-01-03'),
('PB2JpG8KlgoOqnv', '1', 'database/asset/layout/PB2JpG8KlgoOqnv/', 'trimline.png', 'mechline.png', '', 'MRA', 6, '1', '2021-01-03'),
('Pr0SuHOQ78btJsE', '1', 'database/asset/layout/Pr0SuHOQ78btJsE/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MFA,PIH', 6, '2', '2021-01-03'),
('pSHn8s4m29WvOXL', '1', 'database/asset/layout/pSHn8s4m29WvOXL/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA', 3, '1', '2021-01-03'),
('qcpSiLUP4CrFbHB', '1', 'database/asset/layout/qcpSiLUP4CrFbHB/', 'trimline.png', 'mechline.png', '', 'MHA', 6, '1', '2021-01-03'),
('rwXJfHDP1YQTZIp', '1', 'database/asset/layout/rwXJfHDP1YQTZIp/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA,PIH', 6, '1', '2021-01-03'),
('tUWTlz0SZAd91JN', '1', 'database/asset/layout/tUWTlz0SZAd91JN/', 'trimline.png', 'mechline.png', '', 'MRA', 1.5, '1', '2021-01-03'),
('VniPmSgwcIRpZed', '1', 'database/asset/layout/VniPmSgwcIRpZed/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,MHA,PIH', 3, '1', '2021-01-03'),
('vWxunT1FzURHhVr', '1', 'database/asset/layout/vWxunT1FzURHhVr/', 'trimline.png', 'mechline.png', '', 'MRA,PIH', 3, '1', '2021-01-03'),
('WfKFsJX43Ej6qPt', '1', 'database/asset/layout/WfKFsJX43Ej6qPt/', 'trimline.png', 'mechline.png', '', 'MRA,MHA', 3, '2', '2021-01-03'),
('wkVOv2hpPlDoWT5', '1', 'database/asset/layout/wkVOv2hpPlDoWT5/', 'trimline.png', 'mechline.png', '', 'MRA,MHA', 3, '1', '2021-01-03'),
('XLI6z0schMqZlwN', '1', 'database/asset/layout/XLI6z0schMqZlwN/', 'trimline.png', 'mechline.png', '', 'MRA,MHA', 1.5, '1', '2021-01-03'),
('XwrsTeY2Mj8b9iS', '1', 'database/asset/layout/XwrsTeY2Mj8b9iS/', 'trimline.png', 'mechline.png', '', 'PIH', 3, '1', '2021-01-03'),
('xXuoQ9eha4mkpiD', '1', 'database/asset/layout/xXuoQ9eha4mkpiD/', 'trimline.png', 'mechline.png', 'final_mechline.png', 'MRA,PIH', 6, '2', '2021-01-03'),
('y2OP3ABw0ED8MK4', '1', 'database/asset/layout/y2OP3ABw0ED8MK4/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,PIH', 3, '1', '2021-01-03'),
('YanWOQFD3U9rsqH', '1', 'database/asset/layout/YanWOQFD3U9rsqH/', 'trimline.png', 'mechline.png', '', 'MRA,MFA,PIH', 1.5, '1', '2021-01-03'),
('Yd1GCkcBIO9E3RQ', '1', 'database/asset/layout/Yd1GCkcBIO9E3RQ/', 'trimline.png', 'mechline.png', '', 'MRA,PIH', 6, '1', '2021-01-03'),
('YHmibqnkAX8MJgv', '1', 'database/asset/layout/YHmibqnkAX8MJgv/', 'trimline.png', 'mechline.png', '', 'MRA,MFA', 1.5, '1', '2021-01-03'),
('yszap8EjcVGbi2n', '1', 'database/asset/layout/yszap8EjcVGbi2n/', 'trimline.png', 'mechline.png', '', 'MRA,MFA', 3, '2', '2021-01-03'),
('zcTZhtOF2XK3YW0', '1', 'database/asset/layout/zcTZhtOF2XK3YW0/', 'trimline.png', 'mechline.png', '', 'MFA,MHA,PIH', 3, '1', '2021-01-03'),
('zmeLbMAkfKU4ujd', '1', 'database/asset/layout/zmeLbMAkfKU4ujd/', 'trimline.png', 'mechline.png', '', 'MFA,PIH', 3, '1', '2021-01-03');

-- --------------------------------------------------------

--
-- Table structure for table `line_feeding`
--

CREATE TABLE `line_feeding` (
  `line_feeding_id` varchar(15) NOT NULL,
  `information_id` varchar(15) NOT NULL,
  `line_feeding` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_feeding`
--

INSERT INTO `line_feeding` (`line_feeding_id`, `information_id`, `line_feeding`) VALUES
('1kAL5pgjb93unTa', 'uZxJOD0iIlLd1H3', ''),
('1lRMmnTS6tXypHZ', 'YeQ5sLCoNuh297q', ''),
('3o6qOe2biT8mEnF', 'vBzMyY2WHJhPxiZ', ''),
('3XtG1BsqA4eJEhl', 'sT7Q4VpxLjzySW3', ''),
('4g8pCq2rAXFz9GH', 'sDK2UItT9XGaEzZ', ''),
('4gVwqIXAMC5pzKm', 'q6fctM2Lah3yNUX', ''),
('4qb5xuGHvpMRZnk', 'ZnTs70YtgiquhrV', ''),
('4tFJeiHDu6Bb8ad', 'uScCnmVprhQZeox', ''),
('4WjUwC51rvkdB6L', 'ojVAemZq7FtCvE0', ''),
('5Chg9KwuOJrY7sG', 'jVUtG5M8peS9CRN', ''),
('6ByRfLkwdzlqncr', 'c0v8ei4WMsqAP6f', ''),
('896jkiFV2zUS5Rw', 'Fk85ji7wsX0MWz4', ''),
('8fmBSV2O3ZPvnHy', 'jRNo2gGCsiWvbeO', ''),
('a3JMw4vNlgVSoEx', 'a6Q4IdfyYB8W1Zr', ''),
('aAcqpeUYrzxXQOb', 'la3bXfmeRnNHtMr', ''),
('Arqfin8Z2JQwpzW', 'DhiQPnH1mUtVgMk', ''),
('b8QJ0dxRLTGoXwM', 'UdhG8CKkj94lLui', ''),
('BVpzs6PlSgr4dUi', '0eyIJsZP9wNfOxM', ''),
('d7GjPCWvc4zkqBi', 'IC0cFvoDgtj6TzV', ''),
('dNcuUAIRwvM2XEZ', '4In6VgpvQe5Azi2', ''),
('DyP5wsb6ndvg2al', '0RNvKLrHSm8uVXO', ''),
('DzV4Wjko9Ap7Xqe', 'NCBn7vO9U2hI45k', ''),
('e6tREzflxwUXbnq', '4Gc1UdsiS87LBWp', ''),
('ECv7A9mM4kOnqLT', 'nsW0ObhmUl8oYKJ', ''),
('Ef9rLcNdZxBqUXV', 'Mlq9Qt0rTXeK2PC', ''),
('etsz7kD0BMwgG6I', 'xYyJKspSX1OAZ6T', ''),
('favC4VOmkEwun0t', 'Es9UDNT6p4xWrJq', ''),
('ferzZR4MJH9QmPT', 'CNa8cDE5QgKpkV2', ''),
('ftM85kO9gxh6HEv', 'LhdinCV1I2oQtwU', ''),
('g3lkpFMiZrEnOoV', 'xM7Xs50n6biyDZl', ''),
('G5duifIhbolvQr1', 'nm4SQ6EcUWlPhOp', ''),
('G6WLYXDeZvk3nHT', 'xsdte3Gl1D9nZEU', ''),
('gzk3dOi4BYQwspu', 'sQ8DeEfpxqYwzKC', ''),
('GZzt8Jdy91I5kn0', 'gajZAuznbr5EvGQ', ''),
('INHKFv2heu3Ypz1', 'H7G1DIAczNyM64w', ''),
('j0O7wrqA4Bo6ySi', 'M0vJ58lrIbSPoHV', ''),
('J4RK5EuQqynsm9N', 'BNTUcE4zwgaOrHd', ''),
('JLeWPo5G0vqyVOs', 'MwS7vk6AE5mbqVI', ''),
('JlqpWQswhgiCBGF', 'lnGKmaYE5fSw7xZ', ''),
('Jrcs4qAGztfXYdE', 'f6FsdtPXDEk7GOy', ''),
('jZ7h3kL5Ed26c9K', 'pJnWKfPEVH18sAj', ''),
('Kmx70ZdSFCXzycl', '2FxoKyUYSq0TLBc', ''),
('KtQclz385S72IaZ', 'pFlIM1Po5zGBurW', ''),
('Lg8CfErSxvPFH4o', 'ezlnv32uWMpUXVD', ''),
('mLIHfJO1wA5yThX', 'EeGawDFTxkoMQYv', ''),
('N97PAsGYqi6eWTZ', '6uNU0iC4LYvbpz2', ''),
('njhYIwp7N1kqUa0', 'MS5jCcr8PGk7QyB', ''),
('oIbh2u8n1CyekXi', 'mG8tqN2b3jZB6oW', ''),
('oiXcqrynDZptfk5', 'BJoPd5H2batzTIQ', ''),
('Onc2NgPByhXDUqG', 'OZbI3fRwqcWMKxn', ''),
('pfImrUMx7aYeTSR', 'YqLp39i8rFNg0UO', ''),
('pL8feh1An6FUNH0', 'eHTwPCZ31dxbqW9', ''),
('PtWTkjdogV81vOE', 'uRqQzrjKaETeFWJ', ''),
('PTZHIrd3wLmyvex', 'JdTBHiamwIl74gr', ''),
('QiEAfOJSKVMwGat', 'EjtJAChKZ5O40lW', ''),
('rYALlomX3hsE8vC', 'glUYmNeJvzbTVju', ''),
('sIpx9bEJlQZRrPY', '4vxwteWcJZ0Bya3', ''),
('smtuFGfkl3dInvj', 'FQXDkBdYUmSWocn', ''),
('TkdPbg7a5061sKV', 'F8PsxM2zeAu4pSG', ''),
('U2Pxkbc53ZESzaY', '8hnxqGtsNyKjraQ', ''),
('UWLdwhc7zfgnNCl', 'ogv6CJcNbYWOins', ''),
('UzkNbZoTygGw9nr', 'HQ1wSTY9ln2i5rq', ''),
('v2AU0hkqCGizlcO', 'z2WwsMXtIEfaFbB', ''),
('VTB7HuxfNRYe0iD', 'krnBsRMG2IEyL6K', ''),
('wJngzKp0aEOkXoY', 'FpwctGh4NSOEbWz', ''),
('WKzBkTxGYcSHUDE', 'On20xZmHL1TKjru', ''),
('xTOBshwlEtnmyWQ', 'uDQdcafLZgj9ERO', ''),
('y92XxQzJtGoY5ar', 'gvZY0IAQaLrblUJ', ''),
('yXmfcQoGJ8UhzxA', 'LuB0DMyAcQPF4fI', ''),
('ZCxIWibrLtcqVQa', '9oRFWUmeG5zcnHg', ''),
('ZLGgT3CsSxtAdnh', 'Divfzo1VaO4kspI', '');

-- --------------------------------------------------------

--
-- Table structure for table `qgate_01`
--

CREATE TABLE `qgate_01` (
  `qgate_01_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qgate_01`
--

INSERT INTO `qgate_01` (`qgate_01_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('1PJCXxcv5WRyISa', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/qgate_01/'),
('1WzTbGj6DfiN8cC', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/qgate_01/'),
('2nIs8fRJvklYiCh', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/qgate_01/'),
('2sNaZFm5OwlpBVT', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/qgate_01/'),
('4dVwIa8R0jtcuBZ', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/qgate_01/'),
('5hYqMALcXWgFQZU', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/qgate_01/'),
('6rLkWTmAHKC8aui', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/qgate_01/'),
('7QTnhHCyGcbaSxA', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/qgate_01/'),
('8spTk7nVE6tcaeF', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/qgate_01/'),
('9B5nPuGLgAUvysz', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/qgate_01/'),
('A1zeUd2PT9tgkMQ', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/qgate_01/'),
('AXyvUNFRmhnbW5z', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/qgate_01/'),
('cu19XJIFBrZtj5f', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/qgate_01/'),
('CYbcSZK34H7gOnI', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/qgate_01/'),
('DAyZsweG2UJa5Vu', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/qgate_01/'),
('diX5926ABGNLlT1', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/qgate_01/'),
('DohjYPZzJavtuiR', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/qgate_01/'),
('dq6Wf3ux1t0glG4', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/qgate_01/'),
('DYVBWrbM8pTdF4S', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/qgate_01/'),
('e5yVNf9AB1E2cpY', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/qgate_01/'),
('EacjlnOW1IDPVZ0', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/qgate_01/'),
('EBXkKjSiZGwDHc1', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/qgate_01/'),
('EOV9UvXjHhBuJKx', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/qgate_01/'),
('fbZ4KEwhUrvHsjl', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/qgate_01/'),
('fOjDlsPATHtbd9r', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/qgate_01/'),
('grlKmQwG2bXs89Y', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/qgate_01/'),
('HLa190sVCQANjiy', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/qgate_01/'),
('IclS8FmrnTP0eNt', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/qgate_01/'),
('IDvrwhRfegxXWap', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/qgate_01/'),
('IiXTjwqJo9bSzP6', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/qgate_01/'),
('Ik5XzyGjco3K1wd', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/qgate_01/'),
('iNmJcMvfk3PBUh2', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/qgate_01/'),
('j6mIkRKABpx3soW', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/qgate_01/'),
('Jf4AavREKbILstO', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/qgate_01/'),
('JijBvy10LUbnAMg', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/qgate_01/'),
('joN4uCd8pRhUaey', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/qgate_01/'),
('JVLPUEd3Zxemv6k', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/qgate_01/'),
('jw290TWBtFDfx5V', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/qgate_01/'),
('k1Dy3t8jBzN5qm6', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/qgate_01/'),
('kduA75lVtCn4yRJ', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/qgate_01/'),
('Kg8Z1TazRsLduVh', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/qgate_01/'),
('lH2t9mxnGidKIAR', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/qgate_01/'),
('ljICJrTkHQEsYNb', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/qgate_01/'),
('LqW67tsXwoDi18h', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/qgate_01/'),
('MtTYDgmOSFlKQvG', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/qgate_01/'),
('MvXOm8ZNAViLTka', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/qgate_01/'),
('NUIHJ4AXBkbnaZ5', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/qgate_01/'),
('NVzF2JWhPt0B5Tf', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/qgate_01/'),
('o63ckvghL21V0Tl', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/qgate_01/'),
('OlHYQqkxPNEjVet', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/qgate_01/'),
('p1JXe3RfhKP2FxA', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/qgate_01/'),
('pAoKJ0SFbx4zulZ', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/qgate_01/'),
('PdrJAL3EVqkS0yY', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/qgate_01/'),
('PiodUXnkhv6GwTZ', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/qgate_01/'),
('QT7CNGDHSu9b5RB', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/qgate_01/'),
('qUyFXbKLnSO31Yl', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/qgate_01/'),
('QzM1vdcYK4BauVT', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/qgate_01/'),
('Rcv4857leyaNisC', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/qgate_01/'),
('ReuE79GbFOTH6rc', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/qgate_01/'),
('RuHQzevLOoTISx7', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/qgate_01/'),
('tGUmoAYNVjaE3uh', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/qgate_01/'),
('UrN108mGOEzK6aZ', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/qgate_01/'),
('UYKDE29daVToZgQ', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/qgate_01/'),
('V7J1TRI0bEpZvkW', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/qgate_01/'),
('vJW8N3uHFSTa29L', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/qgate_01/'),
('wA5Q2vWpjz36ThP', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/qgate_01/'),
('yjGedbgqDvNXZEY', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/qgate_01/'),
('YvjskVtZarCPRnT', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/qgate_01/'),
('ZFO9huz0Pn3wWfQ', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/qgate_01/'),
('ZkXYnxu1CGR4Ir2', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/qgate_01/'),
('znl9H1d4yCkQVK5', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/qgate_01/');

-- --------------------------------------------------------

--
-- Table structure for table `qgate_02`
--

CREATE TABLE `qgate_02` (
  `qgate_02_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qgate_02`
--

INSERT INTO `qgate_02` (`qgate_02_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('2tdWp3vDc9jxoiT', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/qgate_02/'),
('3tmag0NVbfndwKs', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/qgate_02/'),
('4yG9qRgi2JXLQPV', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/qgate_02/'),
('6lZV2SYIpLFztmx', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/qgate_02/'),
('7livJYG8nPE20Cp', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/qgate_02/'),
('9o5adJBkIbGDy3c', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/qgate_02/'),
('9Z1OQ7Y4MrnctRg', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/qgate_02/'),
('aAJyHKZ5zdLPhu6', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/qgate_02/'),
('ABnKUjzyJ5IecmP', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/qgate_02/'),
('AIqJkEVNXvh1FWj', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/qgate_02/'),
('AU3zs54Otorcmde', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/qgate_02/'),
('b8IrGhVexQON6tD', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/qgate_02/'),
('bMAnuEyKLzY2CIQ', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/qgate_02/'),
('BPRL6vf8otmh0cE', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/qgate_02/'),
('BrX5ML0uTOY2pgR', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/qgate_02/'),
('C0O9xizlcHJgmtb', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/qgate_02/'),
('CeE0oWgtjlY1bVS', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/qgate_02/'),
('cqbQ4EiAsO3ZYLe', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/qgate_02/'),
('CQMvm8gEa9uAdN1', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/qgate_02/'),
('d7x2WImVEMyqkaZ', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/qgate_02/'),
('DBzCOweo0acj9qU', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/qgate_02/'),
('DpYb49dQvjuGVok', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/qgate_02/'),
('eNJCS4MYVtyfU0W', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/qgate_02/'),
('Euj7D6x3IcgHVMU', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/qgate_02/'),
('FcWAnwZj5XOsbQ3', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/qgate_02/'),
('FIWo0ZHMEtyDacV', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/qgate_02/'),
('Fj1MnCmHr530fUe', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/qgate_02/'),
('g3WRcYhFuO1Emwq', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/qgate_02/'),
('HA5j4vh3LW6aclx', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/qgate_02/'),
('HPY4vRNIM9i6t8l', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/qgate_02/'),
('Ib94vN6J5KYLS3j', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/qgate_02/'),
('Jlgweuam6RBOp1y', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/qgate_02/'),
('KA2yaNlYF3O5ueE', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/qgate_02/'),
('KFSTHYr3DxGndaZ', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/qgate_02/'),
('KYM8WbeTUBpZuP4', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/qgate_02/'),
('MIVsnW8YEwAk6qG', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/qgate_02/'),
('MK9qEO1roFicHDY', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/qgate_02/'),
('modBvNEMhYzuIUq', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/qgate_02/'),
('mrZnYUPpS7CQxKW', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/qgate_02/'),
('nS8k65bgWRDmFpL', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/qgate_02/'),
('nYtLpKfGcJqbB2V', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/qgate_02/'),
('o2pz59XncQeTgyS', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/qgate_02/'),
('POfaeTSCcQkrBUM', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/qgate_02/'),
('qboj7Uvkyu3rwMm', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/qgate_02/'),
('QdoAuWylLRq9EIP', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/qgate_02/'),
('qP1NUldQE827hHZ', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/qgate_02/'),
('rgcw4QyALvoVCDE', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/qgate_02/'),
('Rph8dVqosniNAcB', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/qgate_02/'),
('sIAn9S3qM1FuJvc', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/qgate_02/'),
('Skq345xFbD7vVLM', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/qgate_02/'),
('so4jGVxOpk5Fv39', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/qgate_02/'),
('T6QPWZIX5GfAobg', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/qgate_02/'),
('Tto9d4PKLfFJRVr', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/qgate_02/'),
('U4ZTzGuK6BjIMVr', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/qgate_02/'),
('uREaXNnm0t3bBVQ', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/qgate_02/'),
('V3ceaK7sN5SPgvG', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/qgate_02/'),
('v4WPj6qI37rcnGw', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/qgate_02/'),
('VcBlGmoDtFh7LwS', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/qgate_02/'),
('VjZeHWY47lXOfA9', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/qgate_02/'),
('vK3IOduqENekgwh', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/qgate_02/'),
('wbm5pHTGuziaWco', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/qgate_02/'),
('wMBvfZsmnxKAHgU', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/qgate_02/'),
('Xf51HORWcIdpCSh', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/qgate_02/'),
('xvyNYeZdzf3IBcP', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/qgate_02/'),
('YGZ0QBxOk4EuLaw', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/qgate_02/'),
('YhcGkPFHnqZojKN', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/qgate_02/'),
('yQY8NLHgCbJ9B0q', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/qgate_02/'),
('ytq7mTQ8UkgDhlJ', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/qgate_02/'),
('zAGy6flm52kpaCI', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/qgate_02/'),
('ZeVRSo7WangTBsJ', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/qgate_02/'),
('zgITwsVQYX9ipHS', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/qgate_02/');

-- --------------------------------------------------------

--
-- Table structure for table `sa_panorama`
--

CREATE TABLE `sa_panorama` (
  `sa_panorama_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sa_panorama`
--

INSERT INTO `sa_panorama` (`sa_panorama_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0J7RD6TniXHtzlc', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/sa_panorama/'),
('0M1wO8btVQYc4ir', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/sa_panorama/'),
('1zNotCrkbZs4T7S', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/sa_panorama/'),
('34bgc5iYqtOmHaG', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/sa_panorama/'),
('3yaK8AdX6ULTHfw', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/sa_panorama/'),
('4hMLuOps6PCaFiK', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/sa_panorama/'),
('5ImabZgwjWc4dXh', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/sa_panorama/'),
('62szNPHAdmbBZpI', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/sa_panorama/'),
('6ED5yYfJGFNWZHp', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/sa_panorama/'),
('6r3fP5ewR02gb1T', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/sa_panorama/'),
('7RzhCXtsG6fOonY', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/sa_panorama/'),
('7yxKDi03e2hFLPg', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/sa_panorama/'),
('84xmhSItoTlp2aR', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/sa_panorama/'),
('8EaskQ6JyW5iDnZ', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/sa_panorama/'),
('90nmREwC4HNW7Ku', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/sa_panorama/'),
('9LDk6fPTAmCUjMb', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/sa_panorama/'),
('9zT0y1H63U58Jbi', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/sa_panorama/'),
('Ad1H2xZuig5cT9o', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/sa_panorama/'),
('aiG3kAsBIN2U4Wg', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/sa_panorama/'),
('aq8XGmItKoW3RPN', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/sa_panorama/'),
('aUFd39OmDWLxpet', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/sa_panorama/'),
('b3fHIgBGEXsmDh1', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/sa_panorama/'),
('cowCFOiR0ykmpn2', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/sa_panorama/'),
('cRkvSGYy10jOsLh', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/sa_panorama/'),
('CVJHUFv4nhSKolT', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/sa_panorama/'),
('d20tKLEw1QXuWfr', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/sa_panorama/'),
('dJPUS3czWvq8wLY', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/sa_panorama/'),
('er8EvbwTNRJACmd', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/sa_panorama/'),
('f64w9MehFkPn2zd', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/sa_panorama/'),
('fdAIalRkZNrHLto', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/sa_panorama/'),
('fFzBtnZqA5GlYHO', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/sa_panorama/'),
('FPWcJ173G02oeku', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/sa_panorama/'),
('FZSwA8h53ClOJT0', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/sa_panorama/'),
('g5GM1Kt0Acx6dI8', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/sa_panorama/'),
('geuFQ8cyi2aj41X', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/sa_panorama/'),
('GiFDvTJ0d3cLMEj', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/sa_panorama/'),
('htVI0uizL2qbokm', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/sa_panorama/'),
('iaTKbWqrUI3hMNA', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/sa_panorama/'),
('iM73Wnwo8PeDHkO', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/sa_panorama/'),
('K4odPUJn6WE9RtH', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/sa_panorama/'),
('Kk5XDsBYQA3Mh4j', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/sa_panorama/'),
('l5BWfGAEjq8eUTK', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/sa_panorama/'),
('lBZkWUwK8MzL15v', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/sa_panorama/'),
('LoZO2aXUzxNy4rc', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/sa_panorama/'),
('lpAmcKU35ZOeWfg', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/sa_panorama/'),
('ltn03mFqAsSEDhc', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/sa_panorama/'),
('LYVtwshaMC2QXf0', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/sa_panorama/'),
('MFu39Q54P2NIXzh', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/sa_panorama/'),
('nhY0N4QxaD9LBjM', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/sa_panorama/'),
('NUAbZwFeJkoi3uh', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/sa_panorama/'),
('nuoze47xyMXN2fQ', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/sa_panorama/'),
('oMm8F2AwdchStTO', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/sa_panorama/'),
('PNr4shp6bejWdED', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/sa_panorama/'),
('qy3PdTIGQZLsepV', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/sa_panorama/'),
('QYSRykZ9O1BNqvt', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/sa_panorama/'),
('Srle6JCKigw3T5L', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/sa_panorama/'),
('Tk5w2rocVZUzMA8', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/sa_panorama/'),
('tKgsZW8Xh4PzDvw', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/sa_panorama/'),
('UAxplaNSgQRLi1s', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/sa_panorama/'),
('ViMWaqzs5f10hZe', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/sa_panorama/'),
('vuzmkwAKDjRQhdP', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/sa_panorama/'),
('VWCNuKBXaA94JIF', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/sa_panorama/'),
('WaiF2yAIM9tmKTl', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/sa_panorama/'),
('wcW8njMRvkdHoQu', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/sa_panorama/'),
('wI4ia87sS52mNVD', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/sa_panorama/'),
('wZCbaQ5KlJShO8R', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/sa_panorama/'),
('x0uoPXU7SDvIEkr', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/sa_panorama/'),
('Ya8upUCi6ZgPIQS', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/sa_panorama/'),
('YclhKyDUXiI5guT', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/sa_panorama/'),
('Z3CaBmjcqYAxRIS', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/sa_panorama/'),
('zmUVxnrlAJFQh5G', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/sa_panorama/');

-- --------------------------------------------------------

--
-- Table structure for table `st_00`
--

CREATE TABLE `st_00` (
  `st_00_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_00`
--

INSERT INTO `st_00` (`st_00_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('1OewH8xoChLzc0V', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_00/'),
('4VzrpFNf3Q5onvX', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_00/'),
('4WXNmuJCdhyV1fz', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_00/'),
('5Kr87CZgoPkWqsL', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_00/'),
('72frVRGq1AWZnKM', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_00/'),
('7VORSd6ljBPgmsH', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_00/'),
('8Uqni1swAYmM7Ko', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_00/'),
('9VWoucl1qK3FvYe', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_00/'),
('A4mB9RQbGpKVTDW', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_00/'),
('aBWRJu09C24pKP5', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_00/'),
('acet7AHGFoYfpMQ', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_00/'),
('Al6M1PNSTHp7foQ', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_00/'),
('AyqCaBFjUZQoNxI', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_00/'),
('bTl0RUfHwYsQC2K', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_00/'),
('c6jDnCyfQ2Was1X', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_00/'),
('c7orgQlTNmaU3PL', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_00/'),
('CBEWeh3qZ0ot7iY', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_00/'),
('cHAX1kJmKIELSvd', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_00/'),
('cPYMOftRjam9LD1', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_00/'),
('DfoIVc5xwH3rR26', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_00/'),
('dKqkcP4OG6e5y0R', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_00/'),
('dN3skgyfEC7D1l4', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_00/'),
('dTVH2cUO6aNR81A', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_00/'),
('E5s9jgJLOdweZlT', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_00/'),
('emNBKPytugl2bcJ', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_00/'),
('eMRvZWYLUCAqXxf', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_00/'),
('f3oCgKbEre7u9M2', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_00/'),
('FC1utzG5MqeU8dH', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_00/'),
('FhgIDSrZw7YxGmM', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_00/'),
('GWQpNsxnduOaeMV', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_00/'),
('HDZ9FwPvGzAI1fy', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_00/'),
('hk7xRgs82oWBSdP', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_00/'),
('i2ZaekQR6lfWyUo', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_00/'),
('ixFfVpPOn9hYMs0', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_00/'),
('j8uasdt3EX4SLxA', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_00/'),
('jyVCLRX0D9tIdSk', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_00/'),
('KsV7QmP4lU8EYoH', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_00/'),
('MvUh4aIXoEObTP9', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_00/'),
('N7dCRJbBTnxjS0m', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_00/'),
('OE3A9JbrLQkqWeZ', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_00/'),
('OFoKDEITSvZH6JC', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_00/'),
('ONPAHqhSM2WZIzc', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_00/'),
('pv9xAtyR5hHdwI7', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_00/'),
('q72D08dNFXGaiof', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_00/'),
('QGoF9Ti406y8W3k', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_00/'),
('QiCkNVo8sdSvHcE', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_00/'),
('qvwEdI5myOf2je4', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_00/'),
('rdjNlBRMfmYCPxb', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_00/'),
('rq9NfZK8muPbH0i', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_00/'),
('RQCOWt1PIXMdSxs', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_00/'),
('rxGztB3wEm5UJlf', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_00/'),
('s7aFo3NqOhRYIBU', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_00/'),
('sA49QS5ktpN0fHv', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_00/'),
('SfuRmDyaKnVBiQ7', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_00/'),
('sJV6wj51DrT0efQ', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_00/'),
('tPu7jeET1xpi8SD', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_00/'),
('uGsiFP9QxDZTHWU', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_00/'),
('UnSuTYC1dyFvlhZ', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_00/'),
('v0ntjyOYKBaLm51', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_00/'),
('V8YN9b2MnhRPDa1', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_00/'),
('vJkQmaO13S8NzgM', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_00/'),
('wPh9ulf8cFT6UKr', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_00/'),
('wPkdIyOunTlhVge', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_00/'),
('WriDSOI8ylwmMFa', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_00/'),
('XnWpVLfH1xDy94S', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_00/'),
('Y2grq6NvRiGdoej', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_00/'),
('YG1nRLjdUFEI0a7', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_00/'),
('YitAuSMxV2qyKHg', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_00/'),
('YLAQZP10CXRN6Sa', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_00/'),
('YwmHUlgMJWnshxX', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_00/'),
('ztrTPeDRhi3yWdp', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_00/');

-- --------------------------------------------------------

--
-- Table structure for table `st_01`
--

CREATE TABLE `st_01` (
  `st_01_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_01`
--

INSERT INTO `st_01` (`st_01_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0TzJyQVCWU8Gijh', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_01/'),
('19vwScjhpLsNVRO', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_01/'),
('2HTtQacwPq1KLCS', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_01/'),
('2NWElAiskFJKfaL', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_01/'),
('3omB2rICcGizj6L', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_01/'),
('4cOkZWj2XJfEIz8', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_01/'),
('5VZAEjx6iIc2RKk', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_01/'),
('6jgutDeByHdP5UT', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_01/'),
('6Q4WGswo5Ihqi0S', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_01/'),
('6Rs4VFhSZJMGrxa', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_01/'),
('7vmqxC1O5pgRiHA', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_01/'),
('80xUMfzX5s34nEo', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_01/'),
('8WCyZPrkLjOQida', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_01/'),
('a7qBXoHsrFhTKwk', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_01/'),
('D5izoehC9XdqWSm', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_01/'),
('DSfAL241VIcyErx', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_01/'),
('dvlJBxwy9Yj0ufg', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_01/'),
('flheWQxag2MDtNu', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_01/'),
('flJ3xGPgkBRAMu6', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_01/'),
('G75UjwHyME3Dr1I', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_01/'),
('Ge6LfPnU37Xrojl', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_01/'),
('gqhI7Bx0SLEUG1p', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_01/'),
('HQyRLjmq8evprKz', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_01/'),
('I1WjGSMVz5Fu9io', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_01/'),
('iOGnDFK41CVRIPg', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_01/'),
('ISlKpZ3wVRvU5Fq', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_01/'),
('iU4KzfxTJ2w5uae', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_01/'),
('jagO2vBS41Vhucf', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_01/'),
('JGXDht4AklW3iZS', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_01/'),
('JHbk1rULSayDsXc', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_01/'),
('jIvmlJZiM3kAqVr', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_01/'),
('jZdb26FzlhHiBqn', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_01/'),
('kgoqXAVKOp2Mmlw', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_01/'),
('KRfuNx5F2iz4lnq', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_01/'),
('KtbNJMLZog29ieU', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_01/'),
('ktWZBd4KogUiNvX', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_01/'),
('kY2HTv4PDJKVz3h', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_01/'),
('lCJVGfatcDpvQR0', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_01/'),
('lktIqCE0sUZMKWF', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_01/'),
('lw2jDfBbRqkiF7x', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_01/'),
('lwuE9LJKOACg2UW', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_01/'),
('mdlxAJV765LbPIZ', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_01/'),
('mfs2vVAH3g4Qwyz', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_01/'),
('mXBxzTvdNinFWKG', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_01/'),
('NCXe9mtUn2J5lBd', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_01/'),
('Nrh2l1AUnbPdEcC', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_01/'),
('O4wresLSPxdj3Cu', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_01/'),
('oE297LSKbNeV0CG', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_01/'),
('OmEMFANQVDaUSiC', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_01/'),
('OWvKZbarYqEUdwe', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_01/'),
('p02CQnkTzP3HeXV', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_01/'),
('pfHdQ67wUIlFLsN', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_01/'),
('Pl1u4UELIm5zpQf', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_01/'),
('PsdNYv7rjQV984p', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_01/'),
('QnANy8z1a7c9kIq', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_01/'),
('qr3BeYolzCOsT7w', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_01/'),
('Qv5iGf7EIp1qz8j', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_01/'),
('QWCYSnisTcxr93f', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_01/'),
('s835DbGO496yRJB', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_01/'),
('seAEODGcUSFunWw', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_01/'),
('SjEyIU3amw9AXZr', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_01/'),
('u6sSj4hxkydlbiQ', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_01/'),
('uHwNVPUWXaeKFRj', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_01/'),
('UtuKJqxk3z0V5In', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_01/'),
('vRnwV0pHWsZGJOh', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_01/'),
('XlMkB2y6WiROArz', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_01/'),
('ygZEIaYvcH5p4Cl', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_01/'),
('zc3hUgteqwSWKub', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_01/'),
('zDBtZlUJmXRGQc0', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_01/'),
('zRGMxni8b9K1DCm', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_01/'),
('ZvKjgo213HM9YP8', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_01/');

-- --------------------------------------------------------

--
-- Table structure for table `st_02`
--

CREATE TABLE `st_02` (
  `st_02_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_02`
--

INSERT INTO `st_02` (`st_02_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('14PDEVR7WUYz8FH', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_02/'),
('4M6dRFwV3PS0goL', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_02/'),
('6GNcaD7OVdxu9AW', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_02/'),
('6icFzXK02pmtlZO', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_02/'),
('6w0BVeKhHD4ZfSy', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_02/'),
('7J1C6lykbe0IYaR', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_02/'),
('9tQYeMbNjh6IJn4', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_02/'),
('A3j2pMBT1v4wbKa', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_02/'),
('aMZXsv7YVTwhLnf', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_02/'),
('AWRHTb4moDfKxeM', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_02/'),
('bWxLSVgl8ve3u7q', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_02/'),
('bZ1zjLipdrJVEw0', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_02/'),
('C1fJB4hV9K6GLxZ', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_02/'),
('C3mzYZ4F1Nk6ngj', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_02/'),
('dP74f2g08MVrFRw', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_02/'),
('elx1oOvf9sCD7VA', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_02/'),
('FDrGcT1YhyjvJ0A', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_02/'),
('fzP6BwId9g05ciQ', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_02/'),
('g2CBczFQ6ID5XmK', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_02/'),
('g6MOE7Ju9s3KlwU', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_02/'),
('GaeO2ZFiAcfs6vy', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_02/'),
('hfSuXpWAYesxwa9', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_02/'),
('IwyQjFz6KYh0grq', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_02/'),
('iXAoxybpHs1k6nu', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_02/'),
('iYqh6Dvae0CuI2S', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_02/'),
('J3SCgKZy2BLUOvq', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_02/'),
('jaE5iNDkLXwrHOS', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_02/'),
('JI7djvHamMTCSi8', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_02/'),
('jKzOl1mLg5ZAehv', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_02/'),
('jmCG2ZIa5cp3WD8', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_02/'),
('k4N3V5w92YH7iox', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_02/'),
('kB4SNKFuqOVJ0IL', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_02/'),
('kM9XRA3qEWJfmxu', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_02/'),
('KsI1C8q5nhUzBwF', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_02/'),
('L9cUZXzj5uaDRGh', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_02/'),
('lnhtrSpIXzfx3LE', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_02/'),
('m02P41XIRFZgGOz', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_02/'),
('M3XCSNn2Uu1aJkl', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_02/'),
('MNDl4V1dCbHkg86', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_02/'),
('mNj9BUcn5gQkD7v', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_02/'),
('MVDKJRY0GIwei5H', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_02/'),
('OBzdRXV7kxgbcqP', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_02/'),
('PapJylxCocYMFg5', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_02/'),
('Q8zcR2l3WyCU0MV', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_02/'),
('qlTJw4UAgNaM71O', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_02/'),
('qmYLTHE2DOKjAhQ', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_02/'),
('Qs1ebuX0nYJkHIC', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_02/'),
('qz0fdsMKR2vXxLQ', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_02/'),
('r6ot1X2DlFJGTBI', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_02/'),
('RcCp6oqrMzg07Ij', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_02/'),
('RX8V6HwuBvDMzqt', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_02/'),
('tu13sXWbAeDZRm2', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_02/'),
('tYnq1HBc8jOSlzs', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_02/'),
('uP5CdkRnfDz6r3E', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_02/'),
('urPdX0WJC5TsyE8', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_02/'),
('WeaoHIuR1KzLv0f', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_02/'),
('WTPc57vVmtN0lKO', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_02/'),
('wyoaAh4lJCbfO0P', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_02/'),
('wYSZgHmtiEGsdPR', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_02/'),
('wz19uW7kj4L2FVH', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_02/'),
('x6er3uIWvHLSPfw', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_02/'),
('XaewqFg4oAYdMzn', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_02/'),
('XiHrG45naF3JpyU', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_02/'),
('YbLU9VOAhxEGBvj', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_02/'),
('Yp3Z6NzUPot5wxi', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_02/'),
('yS3YR4LPnEa2hbA', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_02/'),
('YWCKXah4ntRkDgm', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_02/'),
('z5qpPiBHkyFK0s4', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_02/'),
('zaFN4huVXTI2p8f', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_02/'),
('zaQMuTY81tVESd9', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_02/'),
('Zx1cK28lsONCvgz', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_02/');

-- --------------------------------------------------------

--
-- Table structure for table `st_03`
--

CREATE TABLE `st_03` (
  `st_03_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_03`
--

INSERT INTO `st_03` (`st_03_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('1jqo8JQAwLIWgFn', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_03/'),
('50X9YI7vHsSrzhi', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_03/'),
('6xhNYgq1A5DG9if', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_03/'),
('7BvV30LQuUAP94J', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_03/'),
('7tKvicYgB3a8m1X', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_03/'),
('8g0cL5supQ1JIek', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_03/'),
('8RUMmhbjIOrBGSK', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_03/'),
('A3tyPr5EMxieqSn', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_03/'),
('aCwTgGFO6YDqUzM', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_03/'),
('AM7tShJjIcl6KDx', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_03/'),
('b6SVJZWHlPwNihM', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_03/'),
('Bz1glsHnQmk7rte', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_03/'),
('CvAiqUrFwTjgOfB', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_03/'),
('CxrbokEhSeg7OjD', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_03/'),
('dfmXlHFhY5I63RV', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_03/'),
('DjFT7ydrSWEaNo1', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_03/'),
('ElDFLwR19UY3v8b', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_03/'),
('ep9oIJEyZNhjCrB', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_03/'),
('F2S7GQwMcZiEvXg', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_03/'),
('Ff0bXG9axLkRmdQ', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_03/'),
('FYgQ0WyBqh2nvxc', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_03/'),
('G1InB5XO3MjQTs7', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_03/'),
('HPJ8x9fisov0AOp', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_03/'),
('hryi81geTcwNq7o', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_03/'),
('hySKtCJOg7csevH', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_03/'),
('IaM148Pnl3x6Ec9', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_03/'),
('ICrbKRFn7PisNYX', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_03/'),
('IHzkPKJSh2aVTRb', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_03/'),
('iYjsZS3cymWhfKP', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_03/'),
('J1qSFPfOGD4NjUZ', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_03/'),
('JEuIN6qTDgmdVAb', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_03/'),
('JhQfS35eP7OmivF', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_03/'),
('JwF4rT8cZHB2VtR', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_03/'),
('LhYT9Ogz75u48m3', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_03/'),
('LkAXirnzdoMbw7Y', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_03/'),
('lRSzysjZd8vrmCP', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_03/'),
('m03EikDFBTYglt8', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_03/'),
('MRSBd0O8mqJeAoK', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_03/'),
('nAhZC7QxMpEkXsV', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_03/'),
('NgBUG5AHFPfRuWT', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_03/'),
('o9YFNSmr3CVBpsh', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_03/'),
('P1UeRoLGwkVxfms', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_03/'),
('PMiXmJwYEUNRDAZ', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_03/'),
('q3Ejbce0NQliwaT', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_03/'),
('QDvapLFCi1dMzW8', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_03/'),
('qQTAEDHkOWwBZa0', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_03/'),
('RQYAwITkxGMslJF', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_03/'),
('Rszl0uIJhcafg84', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_03/'),
('SClBrhnkOuqcZ84', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_03/'),
('sDHQOgnLze2hTB7', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_03/'),
('shEJrYO7k9c3DbL', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_03/'),
('T2v8w6qOzaiCIDA', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_03/'),
('tmRY1OivDFhA4H3', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_03/'),
('tv6OEodCZpAq9J3', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_03/'),
('UdkifCzNs0XEWBa', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_03/'),
('uQOKZje0FEy5MCB', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_03/'),
('UqrGWeyi80PkKFL', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_03/'),
('Vj1QpTgL8yXCbzm', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_03/'),
('VUyNXYmo9HWfjdG', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_03/'),
('we6kfg8CqXzBWxM', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_03/'),
('WsHBg5mFrGiTVdb', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_03/'),
('xnVstdcS4HZ5Dbf', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_03/'),
('YiGO9xbQEj7FySf', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_03/'),
('yjxUXb2WR7Cozan', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_03/'),
('yVHxdY65AhqFezt', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_03/'),
('z5tFoHk9VrhZCnO', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_03/'),
('zGgywMeQA2kStiV', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_03/'),
('zlONwMBFAjhUoev', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_03/'),
('zRhLm5FsNHKgkfc', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_03/'),
('ZuKBGcyJa2vR061', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_03/'),
('zZMwuf17HODb6q3', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_03/');

-- --------------------------------------------------------

--
-- Table structure for table `st_04`
--

CREATE TABLE `st_04` (
  `st_04_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_04`
--

INSERT INTO `st_04` (`st_04_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0OmJwZe9AiRNq5Y', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_04/'),
('0VuUs6HdqSMDYjt', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_04/'),
('1N3F8ORlVdQy6oz', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_04/'),
('1tCIhNfXPkFVAye', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_04/'),
('25mpnNVBDzvJl8i', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_04/'),
('2SEJBRPn8MAsXzf', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_04/'),
('39CJ6N0t4gnd28V', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_04/'),
('3vxGEn8YuW9PTj7', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_04/'),
('4gZeE7PJdfUtpwH', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_04/'),
('5hofyKBJjuT1WvX', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_04/'),
('5OzwQEL6rdWY2NR', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_04/'),
('6AxfdGMwuDYQPrb', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_04/'),
('6cBCEpjQTgalVw9', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_04/'),
('6RzpN3dmlKA09gu', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_04/'),
('7PLNqRlW8AygTc0', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_04/'),
('7XNLYEolpcGqsb8', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_04/'),
('81gfkUxjDn4t57B', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_04/'),
('AC2caIYWFbQgMwv', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_04/'),
('AluPGmDhXx8yi7e', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_04/'),
('APQYzegd8VTlcns', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_04/'),
('b2StEiro5LYQRd7', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_04/'),
('B4IatdS715URz2G', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_04/'),
('bPgdfrElcxz2YLQ', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_04/'),
('c1ijlsw96at25EW', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_04/'),
('cZ06UKX1PYwfRqW', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_04/'),
('DCQ6fiOXoRaps1l', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_04/'),
('DxqNTuzd5eOfa4m', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_04/'),
('epjfxXIWz9L2GM0', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_04/'),
('Fd5ycsvZt2uHTbR', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_04/'),
('Fey1CWZ2EPm5dpO', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_04/'),
('gGwTDYmCq6JXRLf', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_04/'),
('gp6ScHyhjOEUMi2', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_04/'),
('guGEC1hymvZ3K4N', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_04/'),
('HIc7PrEZAV1goWj', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_04/'),
('hZxjnvQNmgKzfW8', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_04/'),
('ikhKnzdc0LQeNo5', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_04/'),
('ILWjmsM9SRgtu17', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_04/'),
('iOPqb0sJ3rnQSUt', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_04/'),
('ljvwCz8VkxIST4H', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_04/'),
('mSeN7uqXc5aTF2I', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_04/'),
('nf5rFtw4sUSNuZO', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_04/'),
('NFGe4bHIqCKSAaU', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_04/'),
('nz6IGyeEq7U3d0x', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_04/'),
('o9ntW5sOb6a8KXc', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_04/'),
('onJlp5a1AGe2qgY', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_04/'),
('QDkjrfxtbJiEnwS', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_04/'),
('raesfMbQETH04Vq', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_04/'),
('rCBDkeHluSQV1Af', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_04/'),
('rmzhyjSAIFYqvKP', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_04/'),
('sCIKuxWmeSpDldZ', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_04/'),
('sYNp3i4jJhUBVR6', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_04/'),
('TYvbDCS1KxL4VRX', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_04/'),
('u7rMNWs6ZHGoKE0', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_04/'),
('UPv39d6y7auFTOX', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_04/'),
('V0e3ECSpwQD9gOz', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_04/'),
('VApjvc9L5okD8eH', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_04/'),
('VDrug9Z8xplStN3', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_04/'),
('VedhNBwM5lx1TkL', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_04/'),
('VN6YAEd3l8eHvaR', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_04/'),
('vSW9HMLsjqB0ZJm', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_04/'),
('Wdso5bwPe9Tm01E', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_04/'),
('xrqPNmuMTZ9zWcB', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_04/'),
('xTqRVnJM9tykdIP', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_04/'),
('XV2pqdRUnWxBvus', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_04/'),
('yctQXOjumGMJadF', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_04/'),
('yHeD6W2FfEUlhzT', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_04/'),
('YI84exlgCZpytoQ', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_04/'),
('yUpxVYHikEKOCN8', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_04/'),
('yxhFSOGq9Rpjscf', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_04/'),
('ZkLcsiVDwNvjhCI', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_04/'),
('ZyRn02KvtPGizE5', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_04/');

-- --------------------------------------------------------

--
-- Table structure for table `st_05`
--

CREATE TABLE `st_05` (
  `st_05_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_05`
--

INSERT INTO `st_05` (`st_05_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0ngGXUW594ao8Jy', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_05/'),
('0xsRBgKHySpU7Pd', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_05/'),
('1O5p9j7C0vQzRTg', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_05/'),
('2DUHa86MizunBCq', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_05/'),
('5JNQ8kujtZAElOP', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_05/'),
('6cD9mG3hk5MnNx0', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_05/'),
('73A4VtzRBd0HTui', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_05/'),
('8Us6Al9I1JDNr2W', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_05/'),
('9nEb1KlPCtAY3RV', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_05/'),
('aPyxJROeZGuVswM', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_05/'),
('beqihya5gvWVRwY', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_05/'),
('BtikdhxzmTDR0vp', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_05/'),
('Cr5UEglyOB6TRKn', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_05/'),
('dNIFMX6hAElrvY0', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_05/'),
('edPhODmqly8745a', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_05/'),
('G8prWhbwJAlK27j', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_05/'),
('gqbj1Bo8yZfFRGl', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_05/'),
('HkhoJnwpNabPmyE', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_05/'),
('HM2FiOTrzq6JNR8', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_05/'),
('HRygUWvrb1a0qOM', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_05/'),
('Id1CQWw5fyBcZ6D', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_05/'),
('ik0TcPBV5q8pQoj', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_05/'),
('IkQAZahFjPCosVR', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_05/'),
('iL4ZT2VWYb5SD19', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_05/'),
('jDnwOsC34LF70PY', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_05/'),
('Jlw2rk08my5cWpe', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_05/'),
('kj71FqfYTWZSCei', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_05/'),
('Kp7OgLUdVC0E3BQ', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_05/'),
('kwsdcXfZiIoYveu', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_05/'),
('mjbYGveKNcT2nL5', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_05/'),
('MLoqQT6rijDHwNJ', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_05/'),
('MzKUZ56aTD9Eoli', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_05/'),
('N6HjY8ZQoalBt0W', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_05/'),
('NgZmuA2dOP8pc6k', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_05/'),
('nHm4f0ZsBXu69V7', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_05/'),
('oqkNFEj4y0CSsrM', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_05/'),
('oTk24ZA8leGyhrF', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_05/'),
('PCy4RmKkDiXQ9Ml', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_05/'),
('pkvwrEgVtGenb8X', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_05/'),
('Pnv20kDe3FlQLuK', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_05/'),
('PpfrJk0Do8tFTRg', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_05/'),
('pTQGqly56N1h9Jx', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_05/'),
('Puv9qEZs40Mfx5m', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_05/'),
('pzhIu9Dnj7yFte3', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_05/'),
('QcPqlOg6sx91Bkw', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_05/'),
('Qmit72sdDfS54LI', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_05/'),
('r1xoGhBjUCK8HbY', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_05/'),
('R9VZ7nmP3baXCov', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_05/'),
('RrvZEHyj6VxuwT1', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_05/'),
('sgXiLVHjfkhyrIu', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_05/'),
('SlAG7jpz38Wrs0m', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_05/'),
('STW47Muz2GkYjtg', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_05/'),
('szbJ7mk3F9hYdQf', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_05/'),
('T83OsKSkQLCfU4B', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_05/'),
('tkCXuPNSrAc025M', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_05/'),
('tXVrFTIumznDeyl', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_05/'),
('u5r36m7fvGMzyiE', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_05/'),
('UBwTpZR4YyCAQoN', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_05/'),
('uClxvs2KLXNJM9H', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_05/'),
('UIuY8V0OWvNRpy4', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_05/'),
('UQiPnrV1dfx5vyN', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_05/'),
('VldzHwnE6ygTvFO', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_05/'),
('WemMQ15Agk0bs6H', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_05/'),
('WK5gwzFZahPoH0v', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_05/'),
('xH6UY3RwXmh2urQ', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_05/'),
('xXZ4SQK75PgVrwU', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_05/'),
('Y8Aebj0vCOrXPqF', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_05/'),
('yQb4nxCkMZjRvWD', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_05/'),
('zAJbYLCHNPmT2gE', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_05/'),
('ZBWPxYgzcHGnCaV', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_05/'),
('zjVX2Lsy4A9BGED', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_05/');

-- --------------------------------------------------------

--
-- Table structure for table `st_06`
--

CREATE TABLE `st_06` (
  `st_06_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_06`
--

INSERT INTO `st_06` (`st_06_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0Js4intLVz8OheP', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_06/'),
('1asvPebIzx4iD80', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_06/'),
('3tirqwkEgQhL9BK', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_06/'),
('61zpSNa35ByqgOF', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_06/'),
('7mgJ0tWHZiCXIux', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_06/'),
('8ETBdNmG6SfOkVU', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_06/'),
('8FZxwOzRJsgBvyE', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_06/'),
('9FjkO7QasK8RxyS', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_06/'),
('ah8EKyP2B6JtpDm', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_06/'),
('BY4IKL6HxsaPhbS', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_06/'),
('bYksCo8iH2ehAJT', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_06/'),
('C7EbkVWILOXYfQy', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_06/'),
('dcS92XlibC1mqge', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_06/'),
('DGmZFgE78XoTHhL', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_06/'),
('dVh3zeOyul8DCxF', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_06/'),
('eCqiK7hQ0FovJMV', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_06/'),
('EJu6pvRScthGjYM', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_06/'),
('EQyZd7Tqz6VavpU', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_06/'),
('F8WNKTw7uIQAOLg', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_06/'),
('FCbMr1o63NAyQuj', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_06/'),
('fogSP9t4BFbVO7E', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_06/'),
('FznPugaQ8sEDyj6', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_06/'),
('gu5SjUwqHXdtZLA', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_06/'),
('gzGUpKt9Ye5JE8y', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_06/'),
('h6K9D4GUnFJsuf3', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_06/'),
('h8olbJdLQgDUAsv', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_06/'),
('H9U0lyGAgsfVMmP', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_06/'),
('HjTkWBYZLlUhRuE', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_06/'),
('hQOdoSwrKjUJq1E', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_06/'),
('ILqpmEf36Mukx7z', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_06/'),
('ImLvXdniM6RrjH8', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_06/'),
('iQWo74JltOLa8hV', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_06/'),
('iYnuPcIxbaQ4rgF', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_06/'),
('j8NcOTK13draGP2', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_06/'),
('JZ8UFqx2catjXyW', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_06/'),
('KBDtSCX8W72ulYR', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_06/'),
('l5vOhuzXoTEiWQk', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_06/'),
('LCVd5I7wxuUgcD8', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_06/'),
('ljK6uB8kZ4JdF3R', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_06/'),
('mg1BdXzOlnoycDs', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_06/'),
('mr2z9FbVx6Ge4a0', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_06/'),
('NFKUEuiTdIwQm9l', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_06/'),
('nhLmD0zgfESybpx', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_06/'),
('nKTOeUHwqMxIJNj', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_06/'),
('nyCKM514TliYkQR', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_06/'),
('NyQGb7zpLkZ2HlO', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_06/'),
('oaPFDAHR8LuKUS5', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_06/'),
('P7GFdTcuYe5QSA6', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_06/'),
('PEnbwXuUGdSW9RJ', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_06/'),
('PfimQbIFC9ZYd5N', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_06/'),
('q3c1ShON6dtmT9z', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_06/'),
('QiN1lsrxYSg24jK', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_06/'),
('qyWXsKQkmRdb8CY', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_06/'),
('Rg1ISwNpyfGzh8x', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_06/'),
('rKnDY2ewfJz9jOR', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_06/'),
('rq47zJEewFH85lA', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_06/'),
('Sd0IHarX5ohwpqu', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_06/'),
('Sjkd97bVDhixEMT', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_06/'),
('srazWNHi8JbTgIc', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_06/'),
('St7sXCI30UW9TlB', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_06/'),
('TEN9ZpUo36PCRxg', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_06/'),
('TYe3zcbWsOw0xoq', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_06/'),
('UEICWQc0rjnh1KO', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_06/'),
('uLaz5jlhxd4sDJ9', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_06/'),
('uOjndciJfRDwAMy', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_06/'),
('VRuZ8b12jPBYv3f', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_06/'),
('vUz39Rtkedya5XS', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_06/'),
('wLtWKuVcfIzHgq7', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_06/'),
('WQdYP6zfF5p7j9w', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_06/'),
('xK4V6Rq5SI93Ol2', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_06/'),
('zw7DalIyn6AGvo2', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_06/');

-- --------------------------------------------------------

--
-- Table structure for table `st_07`
--

CREATE TABLE `st_07` (
  `st_07_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_07`
--

INSERT INTO `st_07` (`st_07_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('01scLmDUliCY7Xj', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_07/'),
('0djwgJOvmKNh8VA', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_07/'),
('1znsM76lVkw5yrI', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_07/'),
('2xXVIcWKgGzd0CH', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_07/'),
('3P2jnsu4XEgDiAl', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_07/'),
('4AcxaoFREbe5LXY', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_07/'),
('4IdPsFNgTAe7tLR', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_07/'),
('5BLydhZQHVDiuft', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_07/'),
('67SxhPteoTum39i', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_07/'),
('6OVGXfnI2BLtEk1', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_07/'),
('87wkqUierLSanbv', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_07/'),
('8QyuvRbIUNzgxBC', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_07/'),
('a2sC59ZWbASN7TD', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_07/'),
('aCqNxWhyPo30UZk', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_07/'),
('ajvyoswDSAEMCqX', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_07/'),
('ATuU1COxoNs8WI9', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_07/'),
('b2vQhSX7HnMolrd', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_07/'),
('B5w2ozlL4RDKbq9', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_07/'),
('BgcPYTxOZXvJ79I', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_07/'),
('BKY6g3o2GheHU8Z', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_07/'),
('Cc5nafJ6msAtuVr', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_07/'),
('cwqL6e1vZIPzpsh', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_07/'),
('d0nEjNw18qXbVof', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_07/'),
('DfLpTd4v8GzUNEX', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_07/'),
('dFmE8ikU7ItsCo3', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_07/'),
('drkEhMYxegcaHZC', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_07/'),
('fTQIpni6ZtxqM4B', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_07/'),
('g1pyUQfhToJcj9e', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_07/'),
('gbv0a4mHejXidNK', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_07/'),
('GEsFLfc2WModOS4', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_07/'),
('giYoplKQbME6H5t', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_07/'),
('gLcDqAopFvHr8Qa', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_07/'),
('hPIk9ERGDBqxiOL', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_07/'),
('IBA9uU0r6NSsap8', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_07/'),
('igSZkIt710YNyBc', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_07/'),
('it9PFxRe3vmkBZj', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_07/'),
('Iuq4Ps5QDHlcVJO', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_07/'),
('JyCYPQnr9AqedWc', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_07/'),
('kc06MrJeAXvE4ZH', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_07/'),
('Ke3IaGwhVs2CWOv', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_07/'),
('L04Qjszu9kT6NVd', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_07/'),
('LGtUXzDruOcqP9K', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_07/'),
('mP0Q6hARuS1MZHE', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_07/'),
('nMDN2T8q4QbSjrf', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_07/'),
('OA9LJv04ez6jkrm', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_07/'),
('OdrXUj7b9g5Q2oB', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_07/'),
('ou9s0DzymXL7Okd', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_07/'),
('oUEOjm7B3SPrFdz', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_07/'),
('oWgnHFyb704B5UL', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_07/'),
('pcSj1WyXK0QRbzv', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_07/'),
('pijPWqdLubDC06k', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_07/'),
('piL785Uv3rCgbyH', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_07/'),
('RG3NhrFq87OXLak', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_07/'),
('sdPopf8aCQHvYbu', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_07/'),
('ShRqYFE05r2IKsf', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_07/'),
('SOzf6PRNnirQJqY', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_07/'),
('tib6OgqYyIachUJ', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_07/'),
('Tv3SyOYFpmnwDAV', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_07/'),
('WDvG3mT2cNjop6q', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_07/'),
('wYJXNSOeZxo0KUb', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_07/'),
('WZJUgyXIDOPTFaH', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_07/'),
('XbhPa478OfM5Vdu', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_07/'),
('xifaEbqQeAL5DlG', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_07/'),
('XMAnQxrKPa810bS', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_07/'),
('yb9NiYeXx1AWofq', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_07/'),
('YJ328ZamDwfnkT5', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_07/'),
('yKJlMLaGgu5RqWY', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_07/'),
('zp7Qa6xfYUMqITn', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_07/'),
('zW7dsFuQaiCwcDJ', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_07/'),
('zwUOE6Q0IdXv8f2', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_07/'),
('zxCcYQPjNpfnUiW', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_07/');

-- --------------------------------------------------------

--
-- Table structure for table `st_08`
--

CREATE TABLE `st_08` (
  `st_08_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_08`
--

INSERT INTO `st_08` (`st_08_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0hmAtfLcq5S7ZUI', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_08/'),
('2d9izrk50MGc1bP', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_08/'),
('38aSlIDdnZ57wJB', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_08/'),
('3VeBDYf1GJZKLlv', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_08/'),
('6lmryZf1FQRN5ab', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_08/'),
('6VYF1PIAD3OXpfk', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_08/'),
('6XP3W2VEpQOTuYt', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_08/'),
('8FmXk9MUu4gAscQ', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_08/'),
('8lzaT9NbF4D2O1B', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_08/'),
('9BUbnjDprMEvwdz', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_08/'),
('9KiFLCI1NADpVHs', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_08/'),
('9yRULd2FkNQxA3W', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_08/'),
('a702YciUfvmeW4R', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_08/'),
('ajzEDuFlX39sbCI', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_08/'),
('awuOIWzKYQTVMC8', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_08/'),
('BMH3jJ9Zevg5Lmf', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_08/'),
('BVJeT7WQujUf365', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_08/'),
('CDbPMoOIv0ST21V', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_08/'),
('CTXBuM2y56NlOjR', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_08/'),
('CzGkxA1UFY7Lfe8', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_08/'),
('dMVWwkUmbTPst5j', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_08/'),
('DzeRyMGxrXpqJCm', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_08/'),
('E1STmqMiJfDP2YX', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_08/'),
('E75gtchqFnL9iC3', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_08/'),
('eBmjhHUEqkL5Zf4', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_08/'),
('EvDK8U4CfBS6gZI', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_08/'),
('F65TOwqlJaWNxpr', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_08/'),
('fUT36gNMFRJEmLu', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_08/'),
('gDhs2M6B4EI1cbz', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_08/'),
('h59A6msDWFqVtb0', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_08/'),
('hGmgVydKB5bWcYw', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_08/'),
('iGgjHILRX8mY4Db', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_08/'),
('ihGWzbwRf7O9VZP', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_08/'),
('IjJNnQ8EoDixk0e', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_08/'),
('il1abg5qDBWNnVT', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_08/'),
('iLrZy84P6d03TXW', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_08/'),
('iSZ48aJQ20smrLq', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_08/'),
('K2baOBRHT5r3mZj', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_08/'),
('kcHveasqxV8wmhB', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_08/'),
('lOhF8JYmXjwd5SP', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_08/'),
('LytBE2x6KvNuPoC', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_08/'),
('MU3nsYFvQdCVPie', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_08/'),
('Mvp42zEXc635nlZ', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_08/'),
('nCcYJ58ylkXQIE4', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_08/'),
('oJ0jBbFelc5Kfw9', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_08/'),
('oPEz4W3diGc60Mx', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_08/'),
('PEJaTSRHgLZ5rof', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_08/'),
('pqONm8ShngHJykz', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_08/'),
('QfesvqgkotIc0Jh', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_08/'),
('QmgUjAya5EbYDeC', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_08/'),
('qZ4mwvMoAF0kbi7', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_08/'),
('ShyDNJAo4Hp7FXm', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_08/'),
('sNHqhfwzlUZgEdG', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_08/'),
('T0chiRAX6GJMYyt', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_08/'),
('tGgsVqJ9KYdWC0p', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_08/'),
('tNe1CjQyzDVZH7u', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_08/'),
('uVRItf9bScsTq3x', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_08/'),
('VYJrEAxBlL14MRS', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_08/'),
('Wef4dkUoKYRE5Cj', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_08/'),
('WhwvTead48QoREl', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_08/'),
('wVzkh9p6Ug3Y7FP', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_08/'),
('wZFyAU2IifCm9aB', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_08/'),
('xdypP9UtLgkKWCT', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_08/'),
('Xeg05ixdVyAIBmT', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_08/'),
('XPoYMyezR3Nw09V', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_08/'),
('xt3DpKdU8VYs71N', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_08/'),
('YbJAWrD5u3vsQoI', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_08/'),
('yHhtmNxFkE6f5cW', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_08/'),
('YKIOuf1lMsN3Gz9', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_08/'),
('YV8mzjxONBgWueX', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_08/'),
('Zshx4B6e3SWVRMw', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_08/');

-- --------------------------------------------------------

--
-- Table structure for table `st_09`
--

CREATE TABLE `st_09` (
  `st_09_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_09`
--

INSERT INTO `st_09` (`st_09_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0cszU9FSH4M1PTg', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_09/'),
('0DgOp8kX4WCMtyQ', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_09/'),
('0q6WwPiMuDF3xLB', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_09/'),
('17RaHS92PB8gfti', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_09/'),
('1m5dzVF0Mk2uBn9', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_09/'),
('1r3fQuJlyCYdeAZ', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_09/'),
('302zsnxKB1qHyQu', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_09/'),
('3rwO1fhDtelMuJG', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_09/'),
('3ucTgW1OLNMsenh', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_09/'),
('3xS5ClhbmFD7kzf', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_09/'),
('5MvxFwQPcYAhbJ8', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_09/'),
('6zDWUXRBM25vjea', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_09/'),
('afWxO0AbiXE8vmV', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_09/'),
('AokHVGx6Pc2RhlD', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_09/'),
('AqGUHlfyXIi4MoC', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_09/'),
('AzMlZDTcIVRL3Qe', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_09/'),
('bPT4Ow8ythEaGx6', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_09/'),
('by8CtokgeqGYPDW', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_09/'),
('CGHFkwMSmTczegu', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_09/'),
('cGoIN0Z18Uryjz6', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_09/'),
('ChjXZF8gVzUx43A', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_09/'),
('clOERFXgIdhn5qD', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_09/'),
('CLXSOyYhKGrtueB', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_09/'),
('dD3Lg2ZchzTny9A', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_09/'),
('DGkFvCgexWBa0K2', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_09/'),
('e7A3jVUkznXuSLv', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_09/'),
('EIuVfmBPAc4QJ3t', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_09/'),
('EUufNLr7HbGp8hJ', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_09/'),
('G8QZFXeh3TnKlRc', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_09/'),
('gqKDcjEyroF7SXi', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_09/'),
('hCtIk5vZFx1TycK', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_09/'),
('ilAjKRDtJv3maPz', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_09/'),
('IqcSdzJmQb51AeE', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_09/'),
('j6l0cCEMA7LqG3X', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_09/'),
('jBrKsnabxRdz7vt', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_09/'),
('jRJtW5gbNOuE8wQ', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_09/'),
('jrNTFDbeVC08d6k', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_09/'),
('jW7E2nOP8lUAXZL', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_09/'),
('jz6408rcQOghVUN', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_09/'),
('kLtIwBRMip1Fln8', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_09/'),
('kQL5nPKBr4zHeE6', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_09/'),
('l9HFVp3jtNysqJW', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_09/'),
('LbuD6dBPMQx7jI5', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_09/'),
('LgJOb7oz94V8yqI', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_09/'),
('lzaR9dVEhumJK0x', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_09/'),
('mnaKgWDpbI5RcqE', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_09/'),
('NLIZ7fv3RW9DzFq', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_09/'),
('OJDjxSgosPzEWFy', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_09/'),
('olHkb2Xt8n3zjLM', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_09/'),
('OUoNGA2BTyIzDSZ', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_09/'),
('PRvYHdrSbI3652D', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_09/'),
('PZVETW1F4Ju2Rec', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_09/'),
('r4iqUkSnVDewKCd', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_09/'),
('RvE078KPrMDVkqp', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_09/'),
('sBZoQue9tFbmwN5', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_09/'),
('t03ly9jrekTcCaX', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_09/'),
('tjMQs9kAHKiOJLg', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_09/'),
('Trpv1CLwoJIxAGW', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_09/'),
('TSzExXP4v2g0fHp', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_09/'),
('U0V4WX8neDJ3doO', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_09/'),
('UT7l8zVr2Mg5fbn', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_09/'),
('UtoPY0Mn58GSDCf', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_09/'),
('UuOvP61cnzxQ3iV', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_09/'),
('VukZ6mTjEXbvrqx', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_09/'),
('WslgjRyCud4FYLJ', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_09/'),
('Y3pdSfrP8mh5g6k', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_09/'),
('YgPw9AoNMuEFQkZ', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_09/'),
('yR5l6VMcofQjxU9', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_09/'),
('YVg9yxnTWpJ25oF', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_09/'),
('z3YNCsme1wAqb0j', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_09/'),
('zUP0fR6WZyQN21m', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_09/');

-- --------------------------------------------------------

--
-- Table structure for table `st_11`
--

CREATE TABLE `st_11` (
  `st_11_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_11`
--

INSERT INTO `st_11` (`st_11_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0I3pracghHCfTVd', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_11/'),
('0IjTKOBipLVd8RU', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_11/'),
('0XWqsAvUVrwO5oz', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_11/'),
('197KFU6kfMHjuTe', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_11/'),
('1NJyVkaSt2x3n46', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_11/'),
('1PrsqVlmZAXpJSc', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_11/'),
('1wk2lVK3NcaE9Ct', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_11/'),
('3tNFyrAcKvRCnOw', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_11/'),
('3vW1MkJBZwQrUHs', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_11/'),
('3Xge7xQCPAJNOYz', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_11/'),
('6xOkmiAW1drF9YE', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_11/'),
('6y2ldkZCIt5TKAE', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_11/'),
('7HA6KrFniO8C5ME', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_11/'),
('8rfkjZElTix24AW', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_11/'),
('8s6tBpvFalkn1Gw', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_11/'),
('92Cn1EKFyrhmZtb', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_11/'),
('AF26CUmZLwnPTdJ', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_11/'),
('bDBkYIzOgQAdsy6', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_11/'),
('bDQOKWrkfwnGSiJ', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_11/'),
('C7M96aXhuyEVTpI', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_11/'),
('CPF9TmnZEct6xLA', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_11/'),
('CWy172tLSjBb0Nk', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_11/'),
('d5HBevU4rcufpnt', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_11/'),
('E7jTRBSVfmerXFN', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_11/'),
('Eb9Dm7F3YfiQOXA', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_11/'),
('EyOn1wVLYXkd2SZ', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_11/'),
('FdS6QxgM4tNUPA9', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_11/'),
('FViAqdfyvsrl7UE', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_11/'),
('gC3lb7cn9GZeyh0', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_11/'),
('gfYX5qo0EV9AZ1l', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_11/'),
('h2RuQfZcxr50FXV', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_11/'),
('IHcrCqOtvly4k7b', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_11/'),
('IPsjfh3li4yTYqL', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_11/'),
('Ix3WFYwaEOJT8Pe', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_11/'),
('JB2HetZKxLNfcqV', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_11/'),
('jG6WPTNZEnoSeKQ', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_11/'),
('k0bRGPOuV7WhxDo', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_11/'),
('kavYr5u08pnweQ2', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_11/'),
('kbYcPQvKnXsZiJ8', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_11/'),
('kTmN9rVORPv8jM5', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_11/'),
('kxrN72qv9LVQeK4', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_11/'),
('l6BQOG8TiFk75H2', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_11/'),
('ler3WONZQ4ymqwu', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_11/'),
('lxAsoDVU93YqRrd', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_11/'),
('LXdViGHjJtpFf2Q', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_11/'),
('MxfSjheNBpc9nHF', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_11/'),
('n2rA5yQTC4IPJMe', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_11/'),
('Noz0CeLvJHMtx8K', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_11/'),
('pWvylYbuFUOqgX5', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_11/'),
('QfXT72Ing6qjzUR', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_11/'),
('QPys2JK7IBzhTOn', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_11/'),
('S5uNr2X3fMm1pqk', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_11/'),
('s9FyUq8ICHawr5b', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_11/'),
('sIV7bLqaOTUySBg', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_11/'),
('sZNJyMpd8h0zQtO', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_11/'),
('TdQPIBmiMLWKlCV', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_11/'),
('TfZeB0na6IQWOgk', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_11/'),
('tQ4q8vf10RZHEiu', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_11/'),
('tvRXegOGhWE97aN', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_11/'),
('U1cpe6AZy087RMl', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_11/'),
('U75zcJGFlHrZPQN', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_11/'),
('U94t27OZ6AkVGrL', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_11/'),
('uNeVDlWnCJPvfEs', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_11/'),
('UpAcNVSdsRYngIM', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_11/'),
('v6Xm3nSMgtATwNb', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_11/'),
('VFA1ZCoQ46T8a9S', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_11/'),
('vOB7PzbSXqstd9Z', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_11/'),
('WE1qbFB9sAcxrn6', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_11/'),
('wGKARHOLu2mkfgM', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_11/'),
('wZVBO0pWxzEc2nC', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_11/'),
('yqVrcL2mnvi87xN', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_11/');

-- --------------------------------------------------------

--
-- Table structure for table `st_12`
--

CREATE TABLE `st_12` (
  `st_12_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_12`
--

INSERT INTO `st_12` (`st_12_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0I96YAvXiCHUKLz', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_12/'),
('1cTmV4wF3aCZU6y', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_12/'),
('2rhnDO7qXQsTHlW', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_12/'),
('57JQ9ZTjLHxhfGK', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_12/'),
('5EFwgPmDVfBGyCJ', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_12/'),
('7GdZ23IxU6BXeDw', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_12/'),
('7pRKLIV4winAfDO', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_12/'),
('8elKZ5TJUh4m3oR', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_12/'),
('8OW3xKoQu2CBIXq', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_12/'),
('94gXNiIUbdteRxK', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_12/'),
('9F1Vb2jndSmBCZQ', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_12/'),
('9zCobftBiY3dgLc', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_12/'),
('auFiC145NqYtJrU', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_12/'),
('bcHndrRtTl5gO61', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_12/'),
('BK0DcCt7rzsifFO', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_12/'),
('CQ3f7BgtAInWpOY', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_12/'),
('cwCLEdRuVKFrDbH', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_12/'),
('DFi1TYado6cCZOx', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_12/'),
('E1dAiKXck2lMw0H', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_12/'),
('e8sGtX9pv6d3lxZ', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_12/'),
('fedx2N13FS6Vmsi', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_12/'),
('fVrWlmQeKqwMDs1', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_12/'),
('giAR4BldTzFNXEP', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_12/'),
('gLsrx4YbJuOqye9', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_12/'),
('h82cjJEACLolsBW', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_12/'),
('HUsMRQAGkTc8JYC', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_12/'),
('HWNMLb041dSDFXn', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_12/'),
('HWO4oKbkdJDnvqQ', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_12/'),
('I5SdWk2CHta1MFN', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_12/'),
('i6IvNbWSGhAfZj3', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_12/'),
('iOGswmDvIF2eWTQ', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_12/'),
('jWetI3Ug5pGsH4q', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_12/'),
('KjbBihlHuI06P5M', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_12/'),
('LF6Om4wQ0VsydrN', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_12/'),
('LFogAhaWBkXcUyQ', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_12/'),
('LQuRAEU26MWx5dh', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_12/'),
('MRXaD3puI0idCzn', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_12/'),
('MsBlmy2PbtroAvI', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_12/'),
('mWU0RZc1alDCNpE', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_12/'),
('nAKUWQ469LrIuVw', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_12/'),
('nB3MOpW0t7NybUH', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_12/'),
('nlXjUVYdMr4tF5W', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_12/'),
('nrPwaOsgDK3ujE1', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_12/'),
('oP28LNETiXJ7G3y', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_12/'),
('oYlfs2QM6RUyGjB', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_12/'),
('pdGI1E9NUncR4aQ', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_12/'),
('Q403BkreNZXDtKg', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_12/'),
('QatgoKAsrjMhki7', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_12/'),
('QaYcHfAtFsohX3T', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_12/'),
('qldF6bBY25wuoyj', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_12/'),
('Rh5GOSp6ozcsvZQ', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_12/'),
('s4fTZMQIhm2FRNv', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_12/'),
('sSqoX9wfvLK3Qgk', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_12/'),
('SzIkrfPEqX7aJHA', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_12/'),
('SZTGWFA6Y9v4aEO', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_12/'),
('tliOB9xYZvWbpVo', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_12/'),
('ts7ioOv6Frz93MA', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_12/'),
('UPya8iNRnoswkxF', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_12/'),
('vYPQrdZXS39n2Mp', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_12/'),
('WLd3DgC1sSmpXOc', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_12/'),
('xC5HO8g7VTSnw1m', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_12/'),
('xfRbzAH7ijMmr0D', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_12/'),
('XmBOdyFcKgrxhe6', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_12/'),
('XWMJI0lCvTa18i6', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_12/'),
('y36NSd5AKRIXo7B', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_12/'),
('y3o8zDUSxQZObdr', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_12/'),
('Y5ZcqpiMJ8EkfA6', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_12/'),
('yUO6TdEMsfCQ0Sb', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_12/'),
('ZgtQq7Rd03bzoUH', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_12/'),
('zKbQVIiSvdRG2Ly', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_12/'),
('ZXCzMunKrPwabDh', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_12/');

-- --------------------------------------------------------

--
-- Table structure for table `st_13`
--

CREATE TABLE `st_13` (
  `st_13_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_13`
--

INSERT INTO `st_13` (`st_13_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0fWOLerRaNF6DXw', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_13/'),
('2p56HroQBMY9q1a', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_13/'),
('3H6gGxt5EPAOVUT', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_13/'),
('3U2gHjGnEXAFkc5', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_13/'),
('5wIzkgFEG8Opsb3', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_13/'),
('7l26eiIK8AwDpO0', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_13/'),
('8G3UtidJxYNH59L', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_13/'),
('8KwpeTbMZCJyPfi', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_13/'),
('8vEaPN6ZWrxJjMG', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_13/'),
('91jiNBPuFVdgcfJ', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_13/'),
('94j0fi7SzDQva2M', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_13/'),
('95iN2gsPOZ4z1aT', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_13/'),
('A0sMaLBlkxe2IQ5', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_13/'),
('AP3ZjN7GbXKkB1R', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_13/'),
('bLjvUS6rXkMdWBl', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_13/'),
('c2NOid8754SKCFx', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_13/'),
('d2EArS5qX9PUlHh', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_13/'),
('D6pJl4INXwPdO0M', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_13/'),
('DbHnCeBmVr69YG1', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_13/'),
('ec3GWibydVkAB9w', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_13/'),
('EKGMOt8A4b6yNS9', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_13/'),
('F8pOoMZTXwCnKc2', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_13/'),
('gBLctQYkoXS9lrV', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_13/'),
('ghUMu74o0ymK32B', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_13/'),
('gm8v6XLFPlBjEV7', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_13/'),
('GQDY4r2vmNxRuMP', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_13/'),
('h5dzMDj9uIbix0J', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_13/'),
('HxgUh1Wr2Xv6BI3', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_13/'),
('I0trohdL81GNzE9', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_13/'),
('i0yF3P69barScN5', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_13/'),
('iagENMJfKwUdYbL', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_13/'),
('iIFH3Jo1szxfXGY', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_13/'),
('iR91ZXjlKCAsLDd', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_13/'),
('JoQ4q96gArdiwsM', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_13/'),
('kbAma2C30Dnh6J7', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_13/'),
('KIjoAOb6xNimfPG', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_13/'),
('kSCYdQzJ05wALUt', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_13/'),
('L0VJluoe3TxvsHt', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_13/'),
('LwOUaEqNXpFhSId', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_13/'),
('mnI2ijNHYxGD73O', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_13/'),
('mQEG3Bd4MeCDuYU', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_13/'),
('MQg54VjeJfSmnEN', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_13/'),
('nMbvlSQ39EdBHtD', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_13/'),
('nXuQ3SitdN4jO5M', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_13/'),
('o1np5R0kmDeL7Kd', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_13/'),
('ovlsRdFgZpj1AVc', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_13/'),
('oWVbkhBOTg7lfpn', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_13/'),
('OZFe7W3spVPInxj', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_13/'),
('pBQPF7wu40I1yCx', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_13/'),
('pCM6qduiY7fbO21', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_13/'),
('PKZxvURGs84b7yF', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_13/'),
('Qt9HbOCsvDk4dK5', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_13/'),
('R3PE0AbxOzWIYph', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_13/'),
('R7BzyEq6W80tjnC', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_13/'),
('RBgANkx7XhjIDi0', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_13/'),
('RGUl1APBVHr3J5u', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_13/'),
('rj61IiwKaxgRdSf', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_13/'),
('sGn2UbzBIik1Tol', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_13/'),
('T5Mj6fdJ31kCKWy', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_13/'),
('tnMzC309PSGXhA5', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_13/'),
('v0kK4oBJhNWpFAI', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_13/'),
('V25sWTNucDHymbC', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_13/'),
('VmjLzCv2eEqMGPJ', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_13/'),
('wDTfm3VSZ0JP9Ka', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_13/'),
('X0MSshf62RtHB9V', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_13/'),
('XQsxSA3kNUujDCF', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_13/'),
('yqdQLEaGFHgspcf', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_13/'),
('yugXHA851PledZj', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_13/'),
('yWSKE9AcI1bzd6H', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_13/'),
('yxpz4wie1Iho3QZ', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_13/'),
('Z0cIB9dzlq7rFX1', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_13/');

-- --------------------------------------------------------

--
-- Table structure for table `st_14`
--

CREATE TABLE `st_14` (
  `st_14_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_14`
--

INSERT INTO `st_14` (`st_14_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('24gvyOLpzc90MUn', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_14/'),
('2dAX5yafl3Y7tri', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_14/'),
('2REPa5JYNBlLMxw', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_14/'),
('3SsTcOGe1PQnVCE', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_14/'),
('4pKf3htO9eqlYRN', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_14/'),
('4zF0d1cGygivjRD', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_14/'),
('5j4kqDJIScZtLeb', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_14/'),
('5lBFEIQceKxpyzN', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_14/'),
('5sGQDZ8gkJibUqh', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_14/'),
('6fDG1qLlJ2wFI0R', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_14/'),
('7S1yc69VdYqznrt', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_14/'),
('98Vw6qUkemdXil1', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_14/'),
('9BsUe67bI4yiEgV', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_14/'),
('9xhCEnH2wvTiMzr', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_14/'),
('a764jsot1Cgibnl', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_14/'),
('ajBO0b5N4Xgsw3R', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_14/'),
('AkzruUJbhKXvBmn', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_14/'),
('dE3C6QwSghHrRfZ', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_14/'),
('dg6SW2sKaVQkHAw', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_14/'),
('DiACY3Xzbouh0Lj', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_14/'),
('DIPO3Lxrvm4woG2', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_14/'),
('DkqE3SohIfVQ5J6', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_14/'),
('EaW8MNl0LDT39hF', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_14/'),
('EKtYqih97b5Q3uO', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_14/'),
('ez16udN4HCBSZp0', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_14/'),
('fAswB2W5eP9MSVv', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_14/'),
('GnJhMwCHL7xaFeI', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_14/'),
('go5JNtMR3sQj67T', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_14/'),
('HJTk3tmq6eFuiGK', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_14/'),
('HZvdeyO8gtwlu2c', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_14/'),
('IluQwCJUGKnvxgf', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_14/'),
('iYUgVBnfSXmDqQ8', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_14/'),
('Jan0jTMCZFX1Gy4', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_14/'),
('jlbOirUMfhtk7CW', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_14/'),
('JR97wK6rWNjoZBA', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_14/'),
('kQMmYJUIVdXKaqW', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_14/'),
('laoPYAm5ztT4iQs', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_14/'),
('lBgraHbiq89xnhZ', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_14/'),
('lfGB7zw3hSrcovD', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_14/'),
('lRxLr2iwcTj7CyH', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_14/'),
('LT740BrPK5GuQXR', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_14/'),
('lvOQyz9XhreKc2B', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_14/'),
('m53cbFzvNIexU0y', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_14/'),
('MNFBIPHqOahE6Qi', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_14/'),
('NEdLrVJH7qM38AT', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_14/'),
('o3nU5M82OR0h6jt', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_14/'),
('oRxKNfwBQ8EtpqZ', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_14/'),
('OyqcKL9uZDAxzBm', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_14/'),
('P5FZothkBHV4vg6', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_14/'),
('PGybKfdS0W7qekg', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_14/'),
('PJo3CqcaK7humxL', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_14/'),
('PVkW6nhcful1BJr', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_14/'),
('PZBMpybWAoRienE', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_14/'),
('Q0Zul1S5RqU9hVA', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_14/'),
('Q4LsOtChvRcElMI', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_14/'),
('Qc6aEThGBi3DlXw', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_14/'),
('Qowp2vEf80uUGqR', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_14/'),
('rBkfKcj6hM4TQ1x', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_14/'),
('SbcL9iv0fY1hIBG', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_14/'),
('sfNoK9XiyGquhvT', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_14/'),
('T38ZrSpsWYcqlgu', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_14/'),
('TSZdBer7KjwVAUb', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_14/'),
('VF6eTJvgCNzWdmA', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_14/'),
('vjI3F0bup6K5V2A', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_14/'),
('vMXNi2JfT7B1pIP', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_14/'),
('W5TsqNidEu6kfcG', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_14/'),
('W6fJwHlXONPIca5', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_14/'),
('wugd3yRqlLCeOAv', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_14/'),
('XDjqfSAvMVCwplZ', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_14/'),
('yRHkJ8fapXznmSQ', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_14/'),
('zs9Hgko2Mtd7quQ', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_14/');

-- --------------------------------------------------------

--
-- Table structure for table `st_15`
--

CREATE TABLE `st_15` (
  `st_15_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_15`
--

INSERT INTO `st_15` (`st_15_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('1OjwtV37SqenByz', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_15/'),
('1vdkHmXBSwTtgjY', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_15/'),
('3K18PkiBHbRnwmp', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_15/'),
('6D2lLfvj38kIaPT', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_15/'),
('7hZHPi5B298CSJK', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_15/'),
('7UdswIV4gkinj2N', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_15/'),
('8aso2ydbXclMx61', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_15/'),
('8ov2tzD6RAmMOlr', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_15/'),
('9gAVobwPUkTNOrE', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_15/'),
('9I2XGMxNYd6W41B', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_15/'),
('9ovM8TfOglCZHz4', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_15/'),
('9qWjB7wYtOpaVh1', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_15/'),
('bg9jv7EsSHZhQi6', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_15/'),
('C7sFp6J81dLbkXz', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_15/'),
('CBz9H1iu76lD5GZ', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_15/'),
('cLk0n9AIl86ts1e', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_15/'),
('cslXIzg0QheHE5C', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_15/'),
('DLj3Y5QnKiObpgG', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_15/'),
('E0N4reb8u5TwfL7', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_15/'),
('eTbGtJmOFfc2a8A', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_15/'),
('eyDHITg6KuaLbFs', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_15/'),
('fmqVhbLXoDIgPM6', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_15/'),
('Fui2th84JpDI9Ca', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_15/'),
('GE5WyLRf1ekQlb2', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_15/'),
('gJ6r13yRKNHC5vW', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_15/'),
('gyGRphN68lavYUb', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_15/'),
('gyiaU5fXw1BOvz9', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_15/'),
('HdVD41FoA2q9UzY', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_15/'),
('hqpFTOdbXW4zuUw', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_15/'),
('ImaPiQxkEMyOjph', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_15/'),
('j3CNSqdwI7etrFJ', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_15/'),
('jOm4WIV2ph7dXuU', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_15/'),
('kmw94IFDN5uvtUp', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_15/'),
('laCs8RWrPqF6HOI', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_15/'),
('lQmUk1n4cruAeKx', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_15/'),
('m6xF9UXwqv5d27b', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_15/'),
('MwnGLbdhSy9N4QE', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_15/'),
('N1zrX6xKO9cwZqf', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_15/'),
('nBPk72jpf9HTDxc', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_15/'),
('NwU7CRtJn6Mesb0', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_15/'),
('oe1mNE2TbXxyf46', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_15/'),
('oIhNsiJwyD2UEH3', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_15/'),
('pfS9xl5mAyobY2e', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_15/'),
('PpzDnbvfmL0t7KA', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_15/'),
('QNWlEJVb07tpGuZ', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_15/'),
('QVS6CpihbqNOF8s', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_15/'),
('scQvfz1Mh8mZB59', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_15/'),
('Sznt8mexiUXEJba', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_15/'),
('tH3Ooiyxqc8VhvQ', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_15/'),
('tME4jT15ZlPaufQ', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_15/'),
('twXQngazfv9MCe2', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_15/'),
('u1zIDSMlVHyNTGC', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_15/'),
('UVbIqPylZLmXOtE', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_15/'),
('V1PJ9gTkqIBuAjW', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_15/'),
('v9fW5NPHwXh078r', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_15/'),
('VSIZ98Wip4dkm0j', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_15/'),
('W4Pr95xlRhBZGOt', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_15/'),
('waWZzyIUpfL5EdT', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_15/'),
('WfkZ97yUplv1ue8', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_15/'),
('wlCG8gcNzZoMjJr', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_15/'),
('X8YWHu4pCfEv7Vb', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_15/'),
('Xn8p5q1olVLPMbN', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_15/'),
('XVCm7P4JNtZGTfw', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_15/'),
('xVmtNJOcFTKLdDB', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_15/'),
('xVrY4zj57I1XyNB', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_15/'),
('yAR4gpcJnFVo5ZH', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_15/'),
('Z0xWiLuegv6bP2B', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_15/'),
('ZadMWFmorhflBCX', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_15/'),
('ZotF3zndA0GxkRm', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_15/'),
('ZQAGJxHfngu4FNv', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_15/'),
('ZSHdN0ToyKMmtpJ', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_15/');

-- --------------------------------------------------------

--
-- Table structure for table `st_16`
--

CREATE TABLE `st_16` (
  `st_16_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_16`
--

INSERT INTO `st_16` (`st_16_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('1BjrKSA2M4hb0Cy', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_16/'),
('1zqaw9kgnXN83Ms', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_16/'),
('3ByGVsdWxuLimq4', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_16/'),
('3xoY2nD0hgqJXHd', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_16/'),
('5Ik6LurvpWDlgCb', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_16/'),
('5VwCUD6oEg9N2a1', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_16/'),
('5zYP1lAfajuyhd8', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_16/'),
('61rtzxUe9aiKmW3', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_16/'),
('6mKIdgpAoTqGXBw', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_16/'),
('70Q3dwR9YjIfZuJ', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_16/'),
('8RgStAM50behrQw', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_16/'),
('9DEymLtT6Bqpwf4', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_16/'),
('9TPJIYwDhBXyMKk', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_16/'),
('avAW7gs60Z5qCUp', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_16/'),
('Bb8k6MCTzDRUSFl', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_16/'),
('BWb1kFms0NMDhlr', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_16/'),
('c6b3MGHERw1Tqmf', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_16/'),
('CkfdlbTLgtP8wOq', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_16/'),
('cpi3meSJrt7zuOo', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_16/'),
('CwZXd1LjhUPyx0E', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_16/'),
('e2VjHdUoK0crny1', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_16/'),
('G1Fyl4qr7MgLASf', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_16/'),
('gSpLtU6zEZF1Ojh', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_16/'),
('hqaY1f8XsKipAg6', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_16/'),
('HWykf3lJNqDo4wn', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_16/'),
('hZ6PEJT52ojMaRu', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_16/'),
('IQLvtDuFw2cgK97', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_16/'),
('Ir8JhAqBVmOoTNe', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_16/'),
('jFHRU51GTPrZgzA', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_16/'),
('Kgt0CZrmeWGUxfq', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_16/'),
('KPisJhmxCwUdA1H', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_16/'),
('l12uO3fnkv9KQXy', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_16/'),
('l9OriqaPt5H4hzD', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_16/'),
('lMJmd5rVuWeAfOC', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_16/'),
('LPgiWHzmKtI9n6b', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_16/'),
('lSvXzbM7HphatR1', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_16/'),
('lUBQ4OMeVYAIf3H', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_16/'),
('NQA5JjcZdGwgmra', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_16/'),
('o2rgLlytCQqkfJX', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_16/'),
('O7HG5CnBFf4Rcdx', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_16/'),
('O8P4yqtHDiS2kMV', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_16/'),
('OAZvLMDtqbB1cSx', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_16/'),
('oN7YBmKjyltP1We', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_16/'),
('Ooh8Flr0dup4csQ', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_16/'),
('Pr8b3iMeWfpgjXd', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_16/'),
('QDbtcSUV08LlG9d', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_16/'),
('qDJLZwI2WYUjTF7', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_16/'),
('Rmk9irq42v6xdAH', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_16/'),
('SByn61t539wfQTp', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_16/'),
('sQldB6jznw1k04A', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_16/'),
('sz9XpOritLFZ8ke', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_16/'),
('T5FgcOEr6R4XJl2', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_16/'),
('TPayxYlb0vioj2u', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_16/'),
('tq7rPoKaFf6OhUm', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_16/'),
('UAjErSNRfpwOzqW', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_16/'),
('ucXigLhM8N3jmpk', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_16/'),
('UYhT8JQKfB4mXZC', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_16/'),
('V6KfMCdZjolmsY4', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_16/'),
('vTA3ZwlzUx4M76a', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_16/'),
('w5Q1kzWaYgLOecD', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_16/'),
('WOH7dNwJv1t0DIB', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_16/'),
('wqzbSkpCFVdDXEo', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_16/'),
('xPjQENCda7KYIAn', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_16/'),
('xqPSLeb3cjK28WC', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_16/'),
('XugaCYzwITJltvb', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_16/'),
('xwismOt20RDzfcN', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_16/'),
('YVgKbLJp1UZrDPA', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_16/'),
('YvTaolc7380qniw', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_16/'),
('Zd1sX2wbf9RHvhk', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_16/'),
('zLpiP7e0CkRGBjO', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_16/'),
('zpvd8HPNkriBnV2', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_16/');

-- --------------------------------------------------------

--
-- Table structure for table `st_17`
--

CREATE TABLE `st_17` (
  `st_17_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_17`
--

INSERT INTO `st_17` (`st_17_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0QHvKd5tFPgaICb', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_17/'),
('1myjQKzDLOvlHus', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_17/'),
('1qUxaNRIWtQPzdK', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_17/'),
('2QawxIfH9Ct0OsA', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_17/'),
('3VU8peTh4GxW5iz', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_17/'),
('4k2R6HC5DQ1u3vX', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_17/'),
('60rb9L3cTtxlUas', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_17/'),
('6YzU4yFBvRJobAx', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_17/'),
('8Y4NnFLvl9fKwcC', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_17/'),
('AgtHuCEUOzQp27c', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_17/'),
('Avklb2Ne3YWBnKq', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_17/'),
('awcxjUqfF9KukGA', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_17/'),
('AXOb1F4rGMs0jTf', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_17/'),
('BAL3vMRbjSdO0tm', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_17/'),
('BFIyw6zW0h4ZGpU', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_17/'),
('BWamkvZx1fzYGTV', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_17/'),
('c21dAe3YoH8r64v', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_17/'),
('CbazZhFON1YJELD', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_17/'),
('cusFBSnPLdKgQ1A', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_17/'),
('DF9OCroghQIEW7p', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_17/'),
('DI9GximHyW6pjZf', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_17/'),
('DLPQJr12m7uh4bg', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_17/'),
('dr15g8XwxlN32YG', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_17/'),
('e90b4mW6KlCZjDc', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_17/'),
('Ei4qLUGjlSYOCpR', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_17/'),
('EwqgkGfc0AdZe2I', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_17/'),
('Fy9toKAcv5wsVhU', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_17/'),
('gponuVWy40T13Is', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_17/'),
('gwELGyIQ9ACl34e', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_17/'),
('h2UsPaYVuwvc8Mt', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_17/'),
('h3ZfkTSWp5vaAVB', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_17/'),
('JSDnfkvM4ZG9H2Q', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_17/'),
('jWSYhlr61TLZRCo', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_17/'),
('k0lNLwdX1Cgqp4K', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_17/'),
('k86P5YyjRQMuncD', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_17/'),
('K8QuwqpRSMTGy9U', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_17/'),
('kuFCbg6zYrW0ETh', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_17/'),
('L6lTfdICOFSRPKE', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_17/'),
('m2PSje7HL6h51BU', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_17/'),
('MLzuB4ox1CWkXbF', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_17/'),
('NGHfVKWE3O15eRs', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_17/'),
('opDlf3SXPNyj2EL', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_17/'),
('p3OJA2Bhed8XDWf', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_17/'),
('qaRpDnThjxHWLAS', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_17/'),
('qGz4L5CiVb30fKs', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_17/'),
('QH9oKcmTw5XsZGU', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_17/'),
('QHufY63gqDUTJ0l', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_17/'),
('qzvyRZL9QFPkcM5', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_17/'),
('Rip1mOzb5oh4XuP', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_17/'),
('RToUV9e75jiQHPF', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_17/'),
('rxDWNfs9M04hETV', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_17/'),
('RZ3OKHLPVDA1jp7', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_17/'),
('Scplr03TNUbe7n8', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_17/'),
('SgO7WZohz4nCl0X', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_17/'),
('srRpw3h6jbgqU0J', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_17/'),
('SwACVxHoInmhWkK', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_17/'),
('THrbe7oEZnywOWt', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_17/'),
('u6foePm0dB83GI4', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_17/'),
('u6hQjyeOsWVG3ND', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_17/'),
('uaOymKVv9zHLUl4', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_17/'),
('uBnDAXRJ9Ys4Hve', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_17/'),
('vLHgc0se9ko8TFK', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_17/'),
('vu9iGogsXT0Ae18', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_17/'),
('WG7n65amiydlJKF', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_17/'),
('x9ykGIJaXbRjDBL', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_17/'),
('XGS2pguV8Ir0TC5', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_17/'),
('yoAbkmjQXdIgBSL', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_17/'),
('YsXTR6KPo9NUvAw', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_17/'),
('ZAPFd0DolpbaneR', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_17/'),
('ZCE3TqSsB1j9L6n', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_17/'),
('zN2oUPiCvVRtG4D', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_17/');

-- --------------------------------------------------------

--
-- Table structure for table `st_18`
--

CREATE TABLE `st_18` (
  `st_18_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_18`
--

INSERT INTO `st_18` (`st_18_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('0Engu3QqvmsMcIL', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_18/'),
('0sLrZwl19AYFy3Q', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_18/'),
('1E2ZiPyzOYR3Dqw', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_18/'),
('1QH5OUD0gXzmVE4', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_18/'),
('1UyB8K5er6CZ7Wf', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_18/'),
('3iA7HdmxYDVtcwG', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_18/'),
('3PGxBDCAJmz1oaX', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_18/'),
('5MPCeOsmxZzGIi6', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_18/'),
('5q6VQkEWlG7iBb9', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_18/'),
('7fGTpsZaIokhYjP', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_18/'),
('8ae0smPDrLIcbJo', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_18/'),
('8Q6bpJkZN0rXMBO', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_18/'),
('8qfweTSMWJihBP6', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_18/'),
('8qLGZUWpmFvjYBJ', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_18/'),
('9SZsopCq8v3RDHM', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_18/'),
('AOL1hI3KmcDqn6X', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_18/'),
('B5LY0cAJrkGfZ4d', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_18/'),
('BZdlsV9vuLnmhow', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_18/'),
('c9U7A43NvQk6PDV', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_18/'),
('cCZhXilBAEIqKeT', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_18/'),
('CHDWAPJ483cfkr0', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_18/'),
('cM6g4hVBI5AO7Ti', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_18/'),
('CW1Kztpk6Tdf4hQ', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_18/'),
('D9KVtfqOvknMTQL', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_18/'),
('e1AY0O4Qo673iW5', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_18/'),
('ENJdChy3m8PfXGg', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_18/'),
('ENQjhBmZt5CTwLY', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_18/'),
('EVzOxAe0lw6oJRU', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_18/'),
('fjTB3XsrxumVIkt', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_18/'),
('g5vGl6JOHEs2Vfh', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_18/'),
('GeoqDBwQWAdzHTV', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_18/'),
('gO1PlLcfUivMqx4', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_18/'),
('JAnHoayfTlx80CZ', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_18/'),
('JmaL5wAXfIbFcQq', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_18/'),
('jRrpafd7XC932hA', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_18/'),
('K4W2UON1Gu5Xe6m', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_18/'),
('kpTn6lYHdULFQPI', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_18/'),
('kQ9eRd6r8BvPMOU', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_18/'),
('Kv8159IFZc4Dax2', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_18/'),
('LCvTla0ihfP5tXA', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_18/'),
('mbvDHrBgktPhNLn', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_18/'),
('mcHPqFiCp7IVlN3', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_18/'),
('MSbYIgB28dcsnZA', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_18/'),
('MXeFAjnftzJUWIs', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_18/'),
('OHGAZBJc9Kb43SV', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_18/'),
('p0OYuH463okhGcU', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_18/'),
('PaOhIB3bMn9Tsil', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_18/'),
('PdGaZS70nHxyB2v', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_18/'),
('ptPac6efjqwFEH4', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_18/'),
('puwjXG4TLoOqPQU', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_18/'),
('PZ9sT4zhHVluwKE', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_18/'),
('qHsY7RmlkpoeLtQ', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_18/'),
('QI6TGwYepB2AoFC', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_18/'),
('QpS1rodxqH7hDb2', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_18/'),
('Qu2jXBU019NRkfT', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_18/'),
('rgu7e3GJIR92X0z', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_18/'),
('s729fTowab0Yzgl', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_18/'),
('stf7liLqnW6PHK1', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_18/'),
('TeFkdj2iLC1EoRf', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_18/'),
('Tn6128A9Smg0Ocu', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_18/'),
('TQ9t2Go4xaBJPc8', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_18/'),
('UHubi2L38AYxa40', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_18/'),
('unwmW8kj5gB23sI', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_18/'),
('UwIob7gVcOmSCYn', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_18/'),
('vZtKA6dDuMBJbgs', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_18/'),
('XLxYg1q5TBjIRba', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_18/'),
('YuNXPx5eV0ZgGJ7', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_18/'),
('z1VaA69WS8qFknI', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_18/'),
('ZJXzMiA4RgF6nvB', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_18/'),
('Zqv10lDWE58T2st', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_18/'),
('ZyiegnEWt32B6k0', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_18/');

-- --------------------------------------------------------

--
-- Table structure for table `st_19`
--

CREATE TABLE `st_19` (
  `st_19_id` varchar(200) NOT NULL,
  `information_id` varchar(200) NOT NULL,
  `2d_drawing` varchar(200) NOT NULL,
  `3d_drawing` varchar(200) NOT NULL,
  `equipment_matrix` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `st_19`
--

INSERT INTO `st_19` (`st_19_id`, `information_id`, `2d_drawing`, `3d_drawing`, `equipment_matrix`, `url`) VALUES
('03Zp4GmU1KdviXs', 'H7G1DIAczNyM64w', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zcTZhtOF2XK3YW0/information/st_19/'),
('1pOwDBnVdJr9T3S', '0RNvKLrHSm8uVXO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fPgdZNKYhmL5oaq/information/st_19/'),
('1xdqcwF3AepDE60', 'ZnTs70YtgiquhrV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L4oqbXBcfGdPA9Q/information/st_19/'),
('2BxyajNkpHYDrL3', 'JdTBHiamwIl74gr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JhbsX2MBWkKUcRx/information/st_19/'),
('39HhUTel0s87BNR', 'EeGawDFTxkoMQYv', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/nCWStUG7q2Dj5Iw/information/st_19/'),
('3jZKSba5QXN6AM2', 'ogv6CJcNbYWOins', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ho97VestRPvfOrS/information/st_19/'),
('3Ql2ONMPkYuDEm9', 'On20xZmHL1TKjru', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlZQsh80pgKkwAD/information/st_19/'),
('6Vvb7P2t9qZhxXp', 'NCBn7vO9U2hI45k', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/eLAEHcWriQa3XKN/information/st_19/'),
('8DczQ9RTiXmOx4V', 'MS5jCcr8PGk7QyB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aLvQtZ1oIzSGdmb/information/st_19/'),
('8g3AHm7GjLW9t1z', 'gajZAuznbr5EvGQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/3Xs6jMd2BtZbrKL/information/st_19/'),
('8HMOzIQUw4Nkv3B', 'nsW0ObhmUl8oYKJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fsebTvwjCBqd73t/information/st_19/'),
('8IcziKhwFvEyXUa', 'IC0cFvoDgtj6TzV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/1WAk53veErVQLGn/information/st_19/'),
('8IpTQ4NWCu1odRz', 'c0v8ei4WMsqAP6f', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/c1OmyUHYguN2TRp/information/st_19/'),
('8xJmBtszpHQNLuy', 'OZbI3fRwqcWMKxn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g6KjfPeUZBsk5LG/information/st_19/'),
('94SIgFboALXyR8v', 'CNa8cDE5QgKpkV2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/WfKFsJX43Ej6qPt/information/st_19/'),
('9IsFWO230QcCHMu', '6uNU0iC4LYvbpz2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/rwXJfHDP1YQTZIp/information/st_19/'),
('A2pMx8y1wKzGNcg', 'uRqQzrjKaETeFWJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/k4Ao7vwMgq6Gans/information/st_19/'),
('ahIkSnovDcKQ6EB', 'mG8tqN2b3jZB6oW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PB2JpG8KlgoOqnv/information/st_19/'),
('AHTRnMXz8lvaSGy', 'Es9UDNT6p4xWrJq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4SVyJln9cWqpxsm/information/st_19/'),
('b9fHkOpsjC7tTEe', 'xYyJKspSX1OAZ6T', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gnhYOcbemf8HaFC/information/st_19/'),
('BGIwbmF42uHTaJ1', 'glUYmNeJvzbTVju', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gvAfbxCHniFSh6k/information/st_19/'),
('c08M9fUBKtjCzwD', 'HQ1wSTY9ln2i5rq', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/b3RgjA4moV0s7iH/information/st_19/'),
('Cqv1wKfTuZk2zXO', 'EjtJAChKZ5O40lW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Kq2hk9lFH5IQeMP/information/st_19/'),
('DFxuImqdRknfSNr', '8hnxqGtsNyKjraQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/PAG5kubRTgeh0la/information/st_19/'),
('dNnrqmB4jz6lYeF', 'LhdinCV1I2oQtwU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5YdfeEMrXzRkOTH/information/st_19/'),
('DSwWhYVCKURzimf', 'pFlIM1Po5zGBurW', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/vWxunT1FzURHhVr/information/st_19/'),
('e38I2dKqoivaFUY', 'z2WwsMXtIEfaFbB', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/yszap8EjcVGbi2n/information/st_19/'),
('eFLIqMpVnRa32wi', 'BNTUcE4zwgaOrHd', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/g0t1fLV7QYNz5XJ/information/st_19/'),
('eKDjgQ9Tq2ynck1', 'F8PsxM2zeAu4pSG', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/5PnDF9mYTGEdZkh/information/st_19/'),
('FbBZTz4udopc6xr', 'krnBsRMG2IEyL6K', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/iGkL8dmT2hjKbMl/information/st_19/'),
('g5A7h9RxntbdV8m', 'sQ8DeEfpxqYwzKC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bp7TeyD9X2kxrtA/information/st_19/'),
('gE61jhJiGBLW9Kx', '4Gc1UdsiS87LBWp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/fU7DupLSKX4atvP/information/st_19/'),
('gOYidZm6TDozx2k', 'f6FsdtPXDEk7GOy', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/tUWTlz0SZAd91JN/information/st_19/'),
('htCsZV2JlE5fm91', 'pJnWKfPEVH18sAj', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ChEcMaeRzTAIkG0/information/st_19/'),
('Ij5X0wDTuUvezJF', 'Mlq9Qt0rTXeK2PC', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/KAIHEdtXJCi4e3Y/information/st_19/'),
('IO96VNms1fKWML8', 'M0vJ58lrIbSPoHV', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/VniPmSgwcIRpZed/information/st_19/'),
('iUwCegau718hlAd', 'BJoPd5H2batzTIQ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/bTp5A0f2GONux6F/information/st_19/'),
('IWKyNZS0ro5Y7lq', '2FxoKyUYSq0TLBc', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/y2OP3ABw0ED8MK4/information/st_19/'),
('K0eiSWywVjMmt1f', 'nm4SQ6EcUWlPhOp', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4yaw52CDG0nbLq1/information/st_19/'),
('KFvoXLVgiNI5Ex8', 'ojVAemZq7FtCvE0', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/dZsqvbA6KicSOwE/information/st_19/'),
('Kgw86DXxWV7GRHm', 'gvZY0IAQaLrblUJ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XLI6z0schMqZlwN/information/st_19/'),
('kTu7OjZp2qy5BGS', 'q6fctM2Lah3yNUX', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Yd1GCkcBIO9E3RQ/information/st_19/'),
('l9CFgTBHwM84OPD', 'Divfzo1VaO4kspI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/6JAY1QCg2FPvysw/information/st_19/'),
('MlfDuCkJ80Ft3Yj', 'FpwctGh4NSOEbWz', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/m8tvebgn3uLFQAT/information/st_19/'),
('Nfc9nLopJETxz4j', 'xsdte3Gl1D9nZEU', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/mGbPI4wUJtWrZnB/information/st_19/'),
('o5cFzgXWVlA67QS', 'vBzMyY2WHJhPxiZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/zmeLbMAkfKU4ujd/information/st_19/'),
('oA0gkLFrjY68aVI', '0eyIJsZP9wNfOxM', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jXty1lHrD8OeQUM/information/st_19/'),
('OZjR6rWta2F1fEn', 'Fk85ji7wsX0MWz4', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/XwrsTeY2Mj8b9iS/information/st_19/'),
('pxlS1k3O9eiWjMs', 'uDQdcafLZgj9ERO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/2oEU95Rs0aYdw8u/information/st_19/'),
('Q9tjeO5yu8AYPKH', 'xM7Xs50n6biyDZl', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YanWOQFD3U9rsqH/information/st_19/'),
('Qf7VMkp1ZxjnCHR', 'uScCnmVprhQZeox', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jQqLd8UFhKJSwD4/information/st_19/'),
('qvgUFur4MeWPLtb', 'UdhG8CKkj94lLui', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/pSHn8s4m29WvOXL/information/st_19/'),
('REWS3wPodIqb6tz', 'MwS7vk6AE5mbqVI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/ei348mapHNvQxFy/information/st_19/'),
('Rhzaw3VgPnYT5Uc', 'FQXDkBdYUmSWocn', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/JRHj89ytG02CSLq/information/st_19/'),
('rSTM4Ka5PE07gWI', 'ezlnv32uWMpUXVD', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/DUWoFfY9aJnmBtA/information/st_19/'),
('S0Wvfbs8g3PRLjO', 'uZxJOD0iIlLd1H3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/xXuoQ9eha4mkpiD/information/st_19/'),
('tK3YI87Qj9hixbf', 'YeQ5sLCoNuh297q', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/gIKwGLd8Rofkyj5/information/st_19/'),
('uGMhTOEAZsRqYDl', 'jVUtG5M8peS9CRN', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/wkVOv2hpPlDoWT5/information/st_19/'),
('UnaCF0jcMWYT3eh', 'LuB0DMyAcQPF4fI', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/aB1QwkY6HjDluVA/information/st_19/'),
('vLRCYAubBI3xsWX', 'sT7Q4VpxLjzySW3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NALuQqwlBo4zdsm/information/st_19/'),
('VTUOdYBq5sowxgN', '4In6VgpvQe5Azi2', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/YHmibqnkAX8MJgv/information/st_19/'),
('wSjvbc9m0yTJfMr', 'eHTwPCZ31dxbqW9', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/d6FnQb943YqETJC/information/st_19/'),
('XCSgoFWqs6YyGRp', '4vxwteWcJZ0Bya3', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NYCJA7gH13qMUnG/information/st_19/'),
('xuHOaq6Y7lCkymG', 'lnGKmaYE5fSw7xZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/4GEeut6MJw7hkLI/information/st_19/'),
('xZJAOb3kg07a6Pl', '9oRFWUmeG5zcnHg', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/qcpSiLUP4CrFbHB/information/st_19/'),
('y0hCpqtPcFo5aRw', 'sDK2UItT9XGaEzZ', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/NG1akrozjXnBwcF/information/st_19/'),
('YgaOcXFHLdey3lM', 'YqLp39i8rFNg0UO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Lg0h1HpqucFIOnt/information/st_19/'),
('Yham9g3JyeuS1ow', 'DhiQPnH1mUtVgMk', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/L9oS45Q286JpKrl/information/st_19/'),
('zKxh62fAo3YvaLO', 'jRNo2gGCsiWvbeO', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/jlJM3Y6kSBgmqxs/information/st_19/'),
('zq6TR3Mi1a8Eotn', 'a6Q4IdfyYB8W1Zr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/LypHFxRdKSUWb15/information/st_19/'),
('Zy8bw7zi5DNh2YO', 'la3bXfmeRnNHtMr', '2d_drawing.png', '3d_drawing.png', 'equipment_matrix.png', 'database/asset/layout/Pr0SuHOQ78btJsE/information/st_19/');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` varchar(100) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `date_created` date NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`, `date_created`, `level`) VALUES
('1', 'admin', 'gajahrizki@gmail.com', 'gajahajag11', '2020-12-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_account_settings`
--

CREATE TABLE `user_account_settings` (
  `user_id` varchar(200) NOT NULL,
  `layout_posted` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `layout_concept`
--
ALTER TABLE `layout_concept`
  ADD PRIMARY KEY (`layout_concept_id`);

--
-- Indexes for table `layout_information`
--
ALTER TABLE `layout_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `layout_uploaded`
--
ALTER TABLE `layout_uploaded`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `line_feeding`
--
ALTER TABLE `line_feeding`
  ADD PRIMARY KEY (`line_feeding_id`);

--
-- Indexes for table `qgate_01`
--
ALTER TABLE `qgate_01`
  ADD PRIMARY KEY (`qgate_01_id`);

--
-- Indexes for table `qgate_02`
--
ALTER TABLE `qgate_02`
  ADD PRIMARY KEY (`qgate_02_id`);

--
-- Indexes for table `sa_panorama`
--
ALTER TABLE `sa_panorama`
  ADD PRIMARY KEY (`sa_panorama_id`);

--
-- Indexes for table `st_00`
--
ALTER TABLE `st_00`
  ADD PRIMARY KEY (`st_00_id`);

--
-- Indexes for table `st_01`
--
ALTER TABLE `st_01`
  ADD PRIMARY KEY (`st_01_id`);

--
-- Indexes for table `st_02`
--
ALTER TABLE `st_02`
  ADD PRIMARY KEY (`st_02_id`);

--
-- Indexes for table `st_03`
--
ALTER TABLE `st_03`
  ADD PRIMARY KEY (`st_03_id`);

--
-- Indexes for table `st_04`
--
ALTER TABLE `st_04`
  ADD PRIMARY KEY (`st_04_id`);

--
-- Indexes for table `st_05`
--
ALTER TABLE `st_05`
  ADD PRIMARY KEY (`st_05_id`);

--
-- Indexes for table `st_06`
--
ALTER TABLE `st_06`
  ADD PRIMARY KEY (`st_06_id`);

--
-- Indexes for table `st_07`
--
ALTER TABLE `st_07`
  ADD PRIMARY KEY (`st_07_id`);

--
-- Indexes for table `st_08`
--
ALTER TABLE `st_08`
  ADD PRIMARY KEY (`st_08_id`);

--
-- Indexes for table `st_09`
--
ALTER TABLE `st_09`
  ADD PRIMARY KEY (`st_09_id`);

--
-- Indexes for table `st_11`
--
ALTER TABLE `st_11`
  ADD PRIMARY KEY (`st_11_id`);

--
-- Indexes for table `st_12`
--
ALTER TABLE `st_12`
  ADD PRIMARY KEY (`st_12_id`);

--
-- Indexes for table `st_13`
--
ALTER TABLE `st_13`
  ADD PRIMARY KEY (`st_13_id`);

--
-- Indexes for table `st_14`
--
ALTER TABLE `st_14`
  ADD PRIMARY KEY (`st_14_id`);

--
-- Indexes for table `st_15`
--
ALTER TABLE `st_15`
  ADD PRIMARY KEY (`st_15_id`);

--
-- Indexes for table `st_16`
--
ALTER TABLE `st_16`
  ADD PRIMARY KEY (`st_16_id`);

--
-- Indexes for table `st_17`
--
ALTER TABLE `st_17`
  ADD PRIMARY KEY (`st_17_id`);

--
-- Indexes for table `st_18`
--
ALTER TABLE `st_18`
  ADD PRIMARY KEY (`st_18_id`);

--
-- Indexes for table `st_19`
--
ALTER TABLE `st_19`
  ADD PRIMARY KEY (`st_19_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
