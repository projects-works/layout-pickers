<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authenticate extends CI_Model {


	function auth($username, $password)
    {
        $result = "";
        $this->db->where('username', $username);
        if ($this->db->where('password', $password))
        {
            $query = $this->db->get('user');		
            if ($query->num_rows()>0)
            {
                foreach ($query->result() as $row) {
                    $sess = array (	'username' 	        => $row->username,
                                    'password' 	        => $row->password,
                                    'user_id' 	        => $row->user_id,
                                    'level' 	        => $row->level);
                }			
                $this->session->get_userdata($sess);
                $this->session->set_userdata('user_id', $sess['user_id']);
                $this->session->set_userdata('username', $sess['username']);			
                $this->session->set_userdata('level', $sess['level']);            
                $result = "authenticated";                        
                        
            }else{
                $result = "please check your username or password.";
                
            }
        }else
        {
            $result = "please check your username or password.";
        }
		
  
        return $result;
		
    } 
}
