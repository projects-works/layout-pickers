<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database_method_model extends CI_Model {


	public function index()
	{
		$this->load->view('login/activity_login');
    }    

    public function createaccount($tablename, $data)
	{
		$this->db->select('email','password');
		$this->db->from($tablename);
		$this->db->where('email',$data['email']);
		$this->db->limit(1);
		$query =$this->db->get();
		if($query->num_rows()==0){
			$tambah = $this->db->insert($tablename,$data);
        	return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function create_layout($table_name, $data)
    {
        $tambah = $this->db->insert($table_name, $data);
    }
    
    public function uploadInfromationLayout($table_name, $data){
        $tambah = $this->db->insert($table_name, $data);
    }

    public function upload_layout($title, $image)
    {
        $result = FALSE;
        $data = array(
            'title'     => $title,
            'file_name' => $image
        );
        if ($this->db->insert('db_names', $data))
        {
            $result = TRUE;
        }
        return $result;
    }

    public function cekuserexist($email, $username)
	{
		$this->db->where('username', $username);
		$this->db->where('email', $email);
		$query = $this->db->get('user');
		if ($query->num_rows()>0)		
		{
			return TRUE;
		}else
		{
			return FALSE;
		}
    }
    
    public function ceklayoutexist($type, $jph, $option)
	{
		$this->db->where('type', $type);
        $this->db->where('jph', $jph);
        $this->db->where('option_layout', $option);
		$query = $this->db->get('layout_uploaded');
		if ($query->num_rows()>0)		
		{
			return TRUE;
		}else
		{
			return FALSE;
		}
    }

	function fetch_data($type, $jph)
    {
        $this->db->select("*");
        $this->db->from("layout_uploaded");
        if($type != '')
        {
            $this->db->like('type', $type);
            $this->db->like('jph', $jph);
        }
        $this->db->order_by('type');
        return $this->db->get();
    }

    function fetch_data2($station, $info_id)
    {
        $this->db->select("*");
        $this->db->from($station);
        if($info_id != '')
        {
            $this->db->like('information_id', $info_id);            
        }
        $this->db->order_by('information_id');
        return $this->db->get();
    }

	public function getLayout($table_name){
        $this->db->select('*');
		$this->db->from($table_name);
		$query =$this->db->order_by('layout_id')->get();
		return $query;
    }

    public function getUserdata($table_name){
        $this->db->select('*');
		$this->db->from($table_name);
		$query =$this->db->order_by('user_id')->get();
		return $query;
    }

    public function updateaccount($table_name, $data, $user_id)
    {
        $this->db->from($table_name);
        $this->db->where('user_id', $user_id);
        $this->db->update($table_name, $data);
    }

	public function getLayoutInfo($table_name){
        $this->db->select('*');
		$this->db->from($table_name);
		$query =$this->db->order_by('information_id')->get();
		return $query;
    }

	public function getLayout2($table_name){
        $this->db->select('*');
		$this->db->from($table_name);
        $query =$this->db->order_by('type');
        $query =$this->db->order_by('jph');
        $query =$this->db->order_by('option_layout');
        $query =$query->get();
		return $query;
    }
    public function upload_image_layout($table_name, $data, $layoutid){        
		$this->db->from($table_name);
        $this->db->where('layout_id', $layoutid);
        $this->db->update($table_name, $data);
    }
    public function upload_information($table_name, $data, $info_id){        
		$this->db->from($table_name);
        $this->db->where('information_id', $info_id);
        $this->db->update($table_name, $data);
    }
}
