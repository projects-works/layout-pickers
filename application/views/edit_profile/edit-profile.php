<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Create User</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <input type="number" class="non-visible" id="index-menu-library" value="2"> 

        <div class="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
                        

            <div class="body-new" id="body-new">
                <div class="first-section-center">                                                   
                <div class="space-vertical"></div>                    
                    <div class="general-library-body">
                        <div class="seccond-section-center">
                        <div class="space-vertical"></div>
                        <button onclick="goBack()"class="font-style-montserrat-semi-bold back-button pointer">
                            <span class="font-size-medium">&#10094;</span> 
                            Back
                        </button>
                            <div class="third-section-content">
                                <div class="midle-content">
                                    <div class="outer">                                        
                                        <h2 class="font-style-montserrat-semi-bold no-margin" style="text-align:center;">Edit Profile</h2>
                                        <div class="space-vertical"></div>
                                        <?php 
                                            foreach($userData->result_array() as $data){ 
                                                if($data['user_id'] == $this->session->userdata('user_id')){
                                        ?>                                        
                                        <div class="form-create-user-body">
                                            <div class="form-box-1">
                                                <input type="text" class="form-input-3 fields 
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Username" title="8 words maximum"
                                                id="username" autocomplete="on" required
                                                oninput="count_username()" value="<?php echo $data['username'] ?>">
                                            </div>
                                            <div class="form-box-1">
                                                <input type="email" class="form-input-3 fields
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Email" 
                                                id="email" autocomplete="on" required
                                                value="<?php echo $data['email'] ?>">                                    
                                            </div>
                                            <div class="form-box-1">
                                                <input type="password" class="form-input-3 fields
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Password" 
                                                id="password" autocomplete="on" required
                                                oninput="">                                    
                                            </div>
                                            <div class="form-box-1">
                                                <input type="password" class="form-input-3 fields
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Re-Password" 
                                                id="re-password" autocomplete="on" required
                                                oninput="check_password()">                                    
                                            </div>
                                            <div class="form-box-1">
                                                <input disabled type="text" class="form-input-3 fields 
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Username" title="8 words maximum"
                                                id="username" autocomplete="on" required
                                                oninput="count_username()" value="<?php if($data['level'] == 1){ echo "Admin";}else{ echo "User";}?>">
                                            </div>
                                            <div class="space-vertical"></div>
                                            <div class="form-box-1">
                                                <button class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer" onclick="create_user()">
                                                Update</button>
                                            </div>
                                        </div>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script src="<?php echo base_url();?>asset/create-user.js"></script>
<script>
    var saving;
    $(document).ready(function()
    {
        var id = <?php echo $this->session->userdata('user_id'); ?>;
        saving =  function(username, email, password, level)
                {
                    $.ajax({                            
                        url     : "<?php echo site_url('database_method_controller/update_user_info') ?>",
                        method  : "POST",
                        data    : {username:username, email:email, password:password, level:level, user_id:id},
                        success : function(data)
                        {                  
                            if(data)
                            {                                
                                alert('Success to update user');                                
                            }else
                            {
                                alert('failed to update user, please check all the fields.');
                            }
                        }
                    })
                    
                }
    });
</script>