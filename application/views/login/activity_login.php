<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Login</title>
    </head>
    <body>    

    <div>
        <div class="midle-content">
            <div style="padding-top:14%;">
                <div class="form-box-login">
                    <div class="flex-all">
                        <div>
                            <div class="form-box-top-caption">
                                <h2 class="h-element font-style-montserrat-semi-bold">
                                    Layout <br>
                                    Pickers
                                </h2>
                            </div>
                            <div style="height:70px"></div>
                            <div style="bottom:0px; position:relative;">
                                <a href="<?php echo site_url('send_email') ?>" class="font-style-montserrat-light font-size-small">
                                    <span style="color:black;">Forgot password?</span>   click here
                                </a>
                            </div>
                        </div>

                        <div class="vertical-line"></div>
                        
                        <div>
                            <div class="form-box-top-caption">
                                <h2 class="h-element font-style-montserrat-semi-bold">Login</h2>
                            </div>
                            <form action="">
                                <div class="form-box-1">
                                    <input type="text" class="form-input-1 
                                    font-style-montserrat-light font-size-small" placeholder="Username" 
                                    id="username" required>
                                </div>
                                <div class="form-box-1">
                                    <input type="password" class="form-input-1 
                                    font-style-montserrat-light font-size-small" placeholder="Password" 
                                    id="password" autocomplete="on" required>
                                </div>
                                <div class="form-box-button-1">
                                    <button type="submit" class="pointer form-button-1 
                                    font-style-montserrat-semi-bold font-size-small" id="login-button"> 
                                        Signin 
                                    </submit>
                                </div>
                            </form>
                        </div>
                    </div>                    
                    
                </div>                
            </div>
        </div>
    </div>
    
    </body>
</html>
<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script>
    $(document).ready(function()
    {

        $("#login-button").on('click', function()
        {
            var uname = $("#username").val();
            var upass = $("#password").val();
            if (uname != "" && upass != "")
            {
                login(uname,upass);
            }else{
                alert("please fill out the form.");
            }
        });

        function customAlert(msg,duration){
            var styler  = document.createElement("div");
            styler.setAttribute("style", "border:solid 5px red; width: auto; top: 5%; "+
                                "left:40%; background-color: #444; color:silver");
            styler.innerHTML= "<h1>" +msg+"</h1>";
            setTimeout(function()
            {
                styler.parentNode.removeChild(styler);
            }, duration);
            document.body.appendChild(styler);

        }

        function login(uname,upass)
        {
            $.ajax({
                type: "POST",
                url : "<?php echo site_url('login/auth') ?>",
                data: {uname:uname, upass:upass},
                success: function(data)
                {                                         
                    if(data == "authenticated")
                    {
                        alert(data);
                        <?php 
                            if ($this->session->userdata('username') != "")
                            {                                
                                redirect("activity/home");
                                parent.window.location.reload();     
                            }
                        ?>  
                                         
                    }else{
                        alert(data);
                    }                 
                }
            })

        }
    });
</script>