<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Forgot Account</title>
    </head>
    <body>    

    <div>
        <div class="midle-content">
            <div style="padding-top:14%;">
                <div class="form-box-login">
                    <div class="flex-all">
                        <div>
                            <div class="form-box-top-caption">
                                <h2 class="h-element font-style-montserrat-semi-bold">
                                    Layout <br>
                                    Pickers
                                </h2>
                            </div>
                            <div style="height:70px"></div>
                            <div style="bottom:0px; position:relative;">
                                <a href="<?php echo site_url('login') ?> " class="font-style-montserrat-light font-size-small">
                                    <span style="color:black;">Login now!</span>   click here
                                </a>
                            </div>
                        </div>

                        <div class="vertical-line"></div>
                        
                        <div>
                            <div class="form-box-top-caption">
                                <h2 class="h-element font-style-montserrat-semi-bold">Recover email</h2>
                            </div>
                            <form action="<?php echo site_url('login') ?>">
                                <div class="form-box-1">
                                    <input type="text" class="form-input-1 
                                    font-style-montserrat-light font-size-small" placeholder="Email" 
                                    id="email" required>
                                </div>
                                <div class="form-box-button-1">
                                    <button type="submit" class="pointer form-button-1 
                                    font-style-montserrat-semi-bold font-size-small" id="send-button"> 
                                        Request
                                    </submit>
                                </div>
                            </form>
                        </div>
                    </div>                    
                    
                </div>                
            </div>
        </div>
    </div>
    
    </body>
</html>
<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script>
    $(document).ready(function()
    {

        $("#send-button").on('click', function()
        {
            var email = $("#email").val();            
            if (email != "")
            {
                recoveremail(email);
            }else{
                alert("please fill out the form.");
            }
        });

        function recoveremail(emailuser)
        {
            $.ajax({
                type: "POST",
                url : "<?php echo site_url('send_email/sendemail') ?>",
                data: {emailuser:emailuser},
                success: function(data)
                {                                         
                    
                    alert(data);
                    // parent.window.location.reload();                        
                                                                      
                }
            })

        }
    });
</script>