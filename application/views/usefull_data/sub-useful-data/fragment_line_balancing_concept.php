<div class="custom-section-center">
    <div class="body-corousel-1">
        <!-- ------------------------  EOL listup ------------------------ -->
        <div class="corouselbody_linebalancing fade-in-1">
            <div class="">
                <img src="<?php echo base_url()?>asset\component\image_component\useful_data\line balancing\linbalancing1.png" 
                alt="" class="image-info ">
            </div> 
        </div>
        <div class="corouselbody_linebalancing fade-in-1">
            <div class="">
            <img src="<?php echo base_url()?>asset\component\image_component\useful_data\line balancing\linbalancing2.png" 
                alt="" class="image-info">
            </div> 
        </div>
        <div class="corouselbody_linebalancing fade-in-1">
            <div class="">
            <img src="<?php echo base_url()?>asset\component\image_component\useful_data\line balancing\linbalancing3.png" 
                alt="" class="image-info">
            </div> 
        </div>

        <div class="space-vertical"></div>
        <div style="padding:16px;">
            <div class="border-horizontal-1"></div>
        </div>                                            
        <div class="midle-content">
            <div class="flex-all">
                <div class="rounded rounded_linebalancing pointer fade-in-1" onclick="corouselCurrentslide(1, 1)"></div>
                <div class="space-vertical"></div>
                <div class="rounded rounded_linebalancing pointer fade-in-1" onclick="corouselCurrentslide(2, 1)"></div>
                <div class="space-vertical"></div>
                <div class="rounded rounded_linebalancing pointer fade-in-1" onclick="corouselCurrentslide(3, 1)"></div>                                                
            </div> 
        </div>                                                                                       
    </div>                                        
</div>