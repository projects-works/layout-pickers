<div class="custom-section-center">
    <div class="body-corousel-1">
        <!-- ------------------------  EOL listup ------------------------ -->
        <div class="corouselbody_sop fade-in-1">
            <div class="">
                <img src="<?php echo base_url()?>asset\component\image_component\useful_data\sop\sop1.png" 
                alt="" class="image-info ">
            </div> 
        </div>
        <div class="corouselbody_sop fade-in-1">
            <div class="">
            <img src="<?php echo base_url()?>asset\component\image_component\useful_data\sop\sop2.png" 
                alt="" class="image-info">
            </div> 
        </div>
        <div class="corouselbody_sop fade-in-1">
            <div class="">
            <img src="<?php echo base_url()?>asset\component\image_component\useful_data\sop\sop3.png" 
                alt="" class="image-info">
            </div> 
        </div>

        <div class="space-vertical"></div>
        <div style="padding:16px;">
            <div class="border-horizontal-1"></div>
        </div>                                            
        <div class="midle-content">
            <div class="flex-all">
                <div class="rounded rounded_sop pointer fade-in-1" onclick="corouselCurrentslide(1, 3)"></div>
                <div class="space-vertical"></div>
                <div class="rounded rounded_sop pointer fade-in-1" onclick="corouselCurrentslide(2, 3)"></div>
                <div class="space-vertical"></div>
                <div class="rounded rounded_sop pointer fade-in-1" onclick="corouselCurrentslide(3, 3)"></div>                                                
            </div> 
        </div>                                                                                       
    </div>                                        
</div>