<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Useful Data</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="3">         

        <div class="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
                    

            <div class="body-new" id="body-new">
                <div class="first-section-center">                                   

                    <?php include('useful_data_navbar.php') ?>
                    
                    <div class="space-vertical"></div>

                    <div class="custom-body-content">
                        <div class="seccond-section-center">
                            <div class="corouselbody_usefuldata fade-in-1">
                                <?php include('sub-useful-data/fragment_line_supply_concept.php') ?>
                            </div>
                            <div class="corouselbody_usefuldata fade-in-1">
                                <?php include('sub-useful-data/fragment_line_balancing_concept.php') ?>
                            </div>
                            <div class="corouselbody_usefuldata fade-in-1">
                                <?php include('sub-useful-data/fragment_station_readiness_concept.php') ?>
                            </div>
                            <div class="corouselbody_usefuldata fade-in-1">
                                <?php include('sub-useful-data/fragment_sop_concept.php') ?>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>      
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url("asset/test.js") ?>"></script>
<script src="<?php echo base_url("asset/navigation-useful-data.js") ?>"></script>