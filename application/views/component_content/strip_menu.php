<div>    
    <div class="body-strip-menu">
        <div class="triple-strip-menu">
            <div class="right ">
                <div id="dropdown-button-menu" class="pointer" onmouseover="triplestrip_on()" 
                onmouseout="triplestrip_off()" onclick="showdropdown()">
                    <div class="strip-menu"></div>
                    <div class="strip-menu"></div>
                    <div class="strip-menu"></div>
                </div>
                
            </div>
            <div style="clear:right"></div>
        </div>        
    </div>    
</div>

<div class="content-menu dropdown-content-menu" id="dropdown-content-menu">
    <div class="menu-content">
        <div class="midle-content menu-content">
            <div class="arrow-up menu-content"></div>
        </div>
        
        <div class="body-menu menu-content">
            <div class="box-menu menu-content">
                <img src="" alt="">
            </div>
            <div class="box-menu-username menu-content">
                <span class="font-style-montserrat-semi-bold 
                    font-size-small menu-content" style="color:teal;"><?php echo $this->session->userdata('username')?></span>
            </div>
            <div class="box-menu menu-content">
                <a href="<?php echo site_url('activity/edit_profile');?>">
                    <button class="button-menu font-style-montserrat-semi-bold 
                        font-size-small pointer">
                        Edit Profile
                    </button>
                </a>                
            </div>
            <div class="box-menu menu-content">
                <button class="button-menu font-style-montserrat-semi-bold 
                    font-size-small pointer" onclick="openmodalsignout()">
                    Logout
                </button>
            </div>
        </div>        
    </div>    
</div>
<?php include(BASE_URL ."modal_fragment_signout.php") ?>
<script src="<?php echo base_url("asset/strip-menu.js") ?>"></script>