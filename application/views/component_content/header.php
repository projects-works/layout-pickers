<?php if ($this->session->userdata('username')== '') 
		{	
            $this->session->sess_destroy;
            echo "<script> alert('during innactivity, you're logout'); </script>";
			redirect('login');
        }
    ?> 
<div class="main-header">
    <div>
        <div>
            <div>
                <div>

                    <div class="outer-header">
                        <div class="first-section-center">
                            <div class="left">
                                <div class="flex">
                                    <div class="icon-header">
                                        <div class="flex-all">
                                            <div>
                                                <img src="<?php echo base_url("asset/component/mercy_logo/mercy_logo.png") ?>" 
                                                class="mercy-logo" alt="">
                                            </div>
                                            <div>
                                                <div><span class="font-style-montserrat-light font-size-small no-margin">layout Pickers</span></div>
                                                <span class="font-style-mercy font-size-small no-margin">Mercedes Benz</span>    
                                            </div>  
                                        </div>                      
                                    </div>
                                    <div>
                                        <div class="flex">
                                            <a href="<?php echo site_url('activity/home') ?>">
                                                <div class="button-menu-box">
                                                    <button class="font-style-montserrat-semi-bold 
                                                    font-size-medium pointer menu-button navbar">
                                                        Home
                                                    </button>
                                                </div>
                                            </a>
                                            
                                            <a href="<?php echo site_url('activity/layout') ?>">
                                            <div class="button-menu-box">
                                                    <button class="font-style-montserrat-semi-bold 
                                                    font-size-medium pointer menu-button navbar">
                                                        Layout
                                                    </button>
                                                </div>
                                            </a>
                                            
                                            <a href="<?php echo site_url('activity/library') ?>">
                                                <div class="button-menu-box">
                                                    <button class="font-style-montserrat-semi-bold 
                                                    font-size-medium pointer menu-button navbar">
                                                        Library
                                                    </button>
                                                </div>
                                            </a>
                                            
                                            <a href="<?php echo site_url('activity/usefull_data') ?>">
                                                <div class="button-menu-box">
                                                    <button class="font-style-montserrat-semi-bold 
                                                    font-size-medium pointer menu-button navbar">
                                                        Useful Data
                                                    </button>
                                                </div>
                                            </a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>     
                            <div class="right">
                                <!-- <img src="<?php echo base_url("asset/component/icon/menu.png") ?>" alt=""> -->
                                <?php include(BASE_URL ."strip_menu.php") ?>
                                
                            </div>       
                            <div style="clear:both;"></div>            
                        </div>  
                    </div> 

                    
                </div>
            </div>
        </div>
    </div>
         
</div>
<script src="<?php echo base_url("asset/navigation.js") ?>"></script>