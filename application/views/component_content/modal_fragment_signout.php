<div class="modal-signout" id="modal-signout">
    <div class="body-modal-transparent">
        <div class="midle-content">
            <div class="body-modal-signout">
                <div>
                    <p class="font-style-montserrat-semi-bold 
                    font-size-medium" style="color:black; text-align:center;">Are you sure?</p>
                </div>
                <div class="space-horizontal"></div>
                <div class="flex-all">
                    <div class="signout-box-button">
                        <a href="<?php echo site_url("login/logout")?>">
                            <button class="signout-button pointer" onmouseover="darker()" onmouseout="lightner()">Signout</button>
                        </a>
                    </div>
                    <div class="signout-box-button">
                        <button class="cancel-signout-button pointer" onclick="close_modal_signout()">Cancel</button>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>

