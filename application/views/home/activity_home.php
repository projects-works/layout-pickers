<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Home</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="0"> 
        <?php 
            define("BASE_URL", "application/views/component_content/");
            include(BASE_URL ."header.php"); 
        ?>
        

        <!-- <a href="<?php echo site_url("login/logout")?>"> <h2>home</h2></a> -->

        <div class="body-new">
            <div class="first-section-center">
                <div class="custom-body-content">                    
                    <div class="title-home">
                        <span class="font-style-montserrat-bold">
                            Layout Pickers
                        </span>
                    </div>          
                    <div class="midle-content">
                        <div class="after-title">
                            <span class="font-style-montserrat-light after-title-words">
                                Layout Pickers helps teams start, design and plan for a better manufacturing.
                            </span>
                        </div>
                    </div>   

                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    
                    <div>
                        <div>
                            <div class="midle-content">
                                <div>
                                    <div class="flex">
                                        <div>
                                            <button class="font-style-montserrat-semi-bold home-navbar">Layout</button>                                            
                                        </div>
                                        <div>
                                            <button class="font-style-montserrat-semi-bold home-navbar">Library</button>                                            
                                        </div>
                                        <div>
                                            <button class="font-style-montserrat-semi-bold home-navbar">Useful Data</button>                                            
                                        </div>
                                    </div>                            
                                </div>
                            </div>
                        </div>                        
                    </div> 

                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>

                    <div>
                        <div>
                            <div>
                                <div class="flex-all">
                                    <div style="width:100%">
                                    
                                    </div>
                                    <div style="width:100%">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>        
        
    </body>
</html>