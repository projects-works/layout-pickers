<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Layout/Main Line</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="1"> 
        <input type="number" class="non-visible" id="index-menu-layout" value="0"> 

        <div class="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
            

            <!-- <a href="<?php echo site_url("login/logout")?>"> <h2>home</h2></a> -->

            <div class="body-new" id="body-new">
                <div class="first-section-center">
                    <?php include('layout_navbar.php') ?>                
                    <div class="space-horizontal"></div>

                    <!-- ----------------------- searching layout ----------------------- -->
                    <div>
                        <div class="flex-all">
                            <div class="searchlayout">
                                <div class="seccond-section-center">
                                    <div class="flex">
                                        <div class="flex-all">
                                            <div class="form-box-1">                                
                                                <select id="model-dropdown" title="you can insert upto 3 models" 
                                                class="form-input-2 pointer
                                                font-style-montserrat-light font-size-small custom-select-input">
                                                    <?php include('list_dropdown_model.php') ?>
                                                </select>
                                            </div>
                                            <div class="space-vertical"></div>                            
                                            <div class="form-box-1">
                                                <input type="number" class="form-input-2 
                                                font-style-montserrat-light font-size-small" placeholder="Annual Volume" 
                                                id="capacity" autocomplete="on" required
                                                title="capacity can't be less than 0 & more than 15000">
                                            </div>
                                        </div>
                                        <div class="flex-all">
                                            <div class="space-vertical"></div>
                                            <div class="form-box-1">
                                                <button class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer" 
                                                id="search_layout" onclick="search()">
                                                Add</button>
                                            </div>
                                            <div class="space-vertical"></div>
                                            <div class="form-box-1">
                                                <button class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer" onclick="window.location.reload();">
                                                Reset</button>
                                            </div>
                                        </div>
                                    </div>                                                                                                             
                                    <div class="space-vertical"></div>
                                   
                                    <div class="flex-all">
                                        <input type="text" id="searchLayoutwithType" class="non-visible">
                                        <input type="number" id="searchLayoutwithJPH" class="non-visible">
                                    </div>
                                    <div class="body-dom-array-search">
                                        <div class="flex-all">
                                            <div class="model-dom-array-search flex-all" id="array-model">                                            
                                                <!-- hasil array model -->
                                            </div>
                                            <div class="border-vertical-1"></div>
                                            <div class="capacity-dom-array-search">
                                                <div class="midle-content">
                                                    <span class="font-style-montserrat-semi-bold
                                                        font-size-medium" id="amount">
                                                        0
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                    <div class="space-vertical"></div>                                                                                
                                </div>                               
                            </div>
                            <div class="searchlayout">
                                <div class="midle-content">
                                    <div>
                                        <table class="font-style-montserrat-light
                                            font-size-small">
                                            <tr>
                                                <td>Assumption</td>
                                            </tr>
                                            <tr>
                                                <td>Cycletime/Station</td>
                                                <td></td>
                                                <td>=</td>                                                
                                                <td id="cycletime">0</td>                                                
                                                <td>Minutes</td>
                                            </tr>
                                            <tr>
                                                <td>Working Hour/Day</td>
                                                <td></td>
                                                <td>=</td>                                                
                                                <td>8</td>                                                
                                                <td>Hour/Day</td>
                                            </tr>
                                            <tr>
                                                <td>Cycletime/Station</td>
                                                <td></td>
                                                <td>=</td>                                                
                                                <td  id="outputunit">0</td>                                                
                                                <td>Units/Shift</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   

                    <div id="result">
                        <!-- <?php include('layout-line/1.5.php') ?> -->
                        
                    </div>
                       

                </div>
            </div>      
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url();?>asset/search-layout.js"></script>
<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script>
    var load;
    $(document).ready(function()
    {

        load =  function(type, jph)
                {
                    $.ajax({                            
                        url     : "<?php echo site_url('ajax_search_method/fetch') ?>",
                        method  : "POST",
                        data    : {type:type, jph:jph},
                        success : function(data)
                        {
                            $('#result').html(data);
                        }
                    })
                    
                }
    });
</script>