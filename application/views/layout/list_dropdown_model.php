<option value="x" selected="true" disabled>Select model</option>
<option value="x" class="fontFamDropdown" disabled>--MRA-------------</option>
<option value="C Class">C Class</option>
<option value="C Class Coupe">C Class Coupe</option>
<option value="E Class">E Class</option>
<option value="E Class Coupe"> E Class Coupe</option>
<option value="S Class">S Class</option>
<option value="S Class Maybach">S Class Maybach</option>
<option value="GLC">GLC</option>
<option value="GLC Coupe">GLC Coupe</option>
<option value="CLS">CLS</option>
<option value="x" class="fontFamDropdown" disabled>--MFA-------------</option>
<option value="A Class">A Class</option>
<option value="B Class">B Class</option>
<option value="GLA">GLA</option>
<option value="CLA">CLA</option>
<option value="GLB">GLB</option>
<option value="x" class="fontFamDropdown" disabled>--MHA-------------</option>
<option value="GLE">GLE</option>
<option value="GLE Coupe">GLE Coupe</option>
<option value="GLS">GLS</option>
<option value="GLS Maybach">GLS Maybach</option>
<option value="x" class="fontFamDropdown" disabled>--PIH&BEV---------</option>
<option value="C Class H">C Class</option>
<option value="C Class Coupe H">C Class Coupe</option>
<option value="E Class H">E Class</option>
<option value="E Class Coupe H"> E Class Coupe</option>
<option value="S Class H">S Class</option>
<option value="S Class Maybach H">S Class Maybach</option>
<option value="GLC H">GLC</option>
<option value="GLC Coupe H">GLC Coupe</option>
<option value="CLS H">CLS</option>
<!-- <option value="x" class="fontFamDropdown" disabled>--BEV--------------</option>
<option value="x" disabled>-</option> -->
