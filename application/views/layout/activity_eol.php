<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Layout/End of Line</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="1"> 
        <input type="number" class="non-visible" id="index-menu-layout" value="1"> 

        <div class="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
            

            <!-- <a href="<?php echo site_url("login/logout")?>"> <h2>home</h2></a> -->

            <div class="body-new">
                <div class="first-section-center">
                    <?php include('layout_navbar.php') ?>
                    <div class="space-horizontal"></div>
                    <div class="custom-body-content">
                        <div class="seccond-section-center">
                            <div class="midle-content">
                                <div class="flex-all">
                                    <div class="form-box-1">
                                        <button  class="form-button-3 font-style-montserrat-semi-bold
                                            font-size-small pointer corouselListButton" onclick="corouselCurrentslide(1)">
                                            1.5 & 3 Jobs/hour
                                        </button>
                                    </div>
                                    <div class="space-vertical"></div>
                                    <div class="form-box-1">
                                        <button  class="form-button-3 font-style-montserrat-semi-bold
                                            font-size-small pointer corouselListButton" onclick="corouselCurrentslide(2)">
                                            6 Jobs/hour [Durr]
                                        </button>
                                    </div>
                                    <div class="space-vertical"></div>
                                    <div class="form-box-1">
                                        <button  class="form-button-3 font-style-montserrat-semi-bold
                                            font-size-small pointer corouselListButton" onclick="corouselCurrentslide(3)">
                                            6 Jobs/hour [Hunter]
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- ------------------------ content ------------------------ -->

                            <div class="seccond-section-center">
                                <div class="seccond-section-center">
                                    <div class="custom-section-center">
                                        <div class="body-corousel-1">
                                            <!-- ------------------------  EOL listup ------------------------ -->
                                            <div class="corouselbody fade-in-1">
                                                <div class="">
                                                    <img src="<?php echo base_url()?>asset\component\image_component\EOL\EOL1.5&3.png" 
                                                    alt="" class="eol_image_corousel ">
                                                </div> 
                                            </div>
                                            <div class="corouselbody fade-in-1">
                                                <div class="">
                                                    <img src="<?php echo base_url()?>asset\component\image_component\EOL\EOL6v2.png" 
                                                    alt="" class="eol_image_corousel">
                                                </div> 
                                            </div>
                                            <div class="corouselbody fade-in-1">
                                                <div class="">
                                                    <img src="<?php echo base_url()?>asset\component\image_component\EOL\EOL6v1.png" 
                                                    alt="" class="eol_image_corousel">
                                                </div> 
                                            </div>

                                            <div class="space-vertical"></div>
                                            <div style="padding:16px;">
                                                <div class="border-horizontal-1"></div>
                                            </div>                                            
                                            <div class="midle-content">
                                                <div class="flex-all">
                                                    <div class="rounded pointer fade-in-1" onclick="corouselCurrentslide(1)"></div>
                                                    <div class="space-vertical"></div>
                                                    <div class="rounded pointer fade-in-1" onclick="corouselCurrentslide(2)"></div>
                                                    <div class="space-vertical"></div>
                                                    <div class="rounded pointer fade-in-1" onclick="corouselCurrentslide(3)"></div>                                                
                                                </div> 
                                            </div>                                                                                       
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          
        
    </body>
</html>
<script src="<?php echo base_url("asset/corousel.js") ?>"></script>