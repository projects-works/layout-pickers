<div class="flex-all" >
    <a href="<?php echo site_url('activity/mainline') ?>"
    class="button-layout-menu first-child-radius font-style-montserrat-semi-bold 
                    font-size-medium pointer navbar-layout">
        <div>
            Main Line
        </div>      
    </a> 
    <a href="<?php echo site_url('activity/eol') ?>"
    class="button-layout-menu last-child-radius font-style-montserrat-semi-bold 
                    font-size-medium pointer navbar-layout">
        <div>
            End of Line
        </div>
    </a>
</div>
<script src="<?php echo base_url("asset/navigation-layout.js") ?>"></script>