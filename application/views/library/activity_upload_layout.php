<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Upload Layout</title>        
    </head>
    <body>       
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <input type="number" class="non-visible" id="index-menu-library" value="0"> 

        <div class="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
                        

            <div class="body-new" id="body-new">
                <div class="first-section-center">       

                    <?php include('library_navbar.php'); ?>

                    <div class="space-vertical"></div>

                    <div class="flex-all">

                        <div class="upload-layout-form-body">
                            <div class="third-section-center">
                                <div class="form-box-1"> 
                                    <div class="flex" style="width:100%;">
                                        <div style="width:100%;">
                                            <select name="" id="type-dropdown" title="you can insert upto 3 types"
                                            class="form-input-3 pointer form-create
                                            font-style-montserrat-light font-size-small custom-select-input">
                                                <?php include('sub-upload-layout/list_dropdown_type.php') ?>
                                            </select>
                                        </div>
                                        <div class="space-vertical"></div>
                                        <div style="width:20%;">
                                            <button  onclick="add()" class="rounded-button pointer"> 
                                                &#10141; 
                                            </button>
                                        </div>
                                    </div>                                                                   
                                </div>                                                           
                                <div class="form-box-1">
                                    <input type="number" oninput="convertToJPH()" class="form-input-3 
                                    font-style-montserrat-light font-size-small custom-select-input  form-create" 
                                    placeholder="Annual Volume"  title="capacity must greater than 0"
                                    id="capacityLayout" autocomplete="on" required>                                                                 
                                </div>

                                <?php echo form_open_multipart('database_method_controller/add_new_layout'); ?>

                                <div class="non-visible">
                                    <input type="text" id="inputFormType" name="type" class="non-visible">
                                    <input type="number" id="inputFormCapacity" name="jph" step=".01" class="non-visible">
                                </div>       
                                <div class="form-box-1">                                
                                    <select name="option" id="selectoption" class="form-input-3 pointer  form-create
                                    font-style-montserrat-light font-size-small custom-select-input"
                                    onclick="checklayout()">
                                        <option value="1">Option 1</option>
                                        <option value="2" disabled>Option 2</option>
                                    </select>
                                </div>
                                <div class="form-box-1">
                                    <button class="form-button-2 font-style-montserrat-semi-bold  form-create
                                    font-size-small pointer" onclick="get_data()">
                                    Create</button>
                                </div>

                                <?php echo form_close();?>  
                            </div>                        
                        </div>

                        <div class="space-vertical"></div>

                        <div class="general-library-body">                            
                            <div class="seccond-section-center">
                                <div>
                                    <div class="space-vertical"></div>  
                                    <div class="flex">
                                        <div>
                                            <h3 class="font-style-montserrat-semi-bold no-margin">Example : </h3>
                                        </div>
                                        <div class="space-vertical"></div>
                                        <div>
                                            <div id="flexType" class="flex"></div>
                                        </div>
                                    </div>                                    

                                    <div class="space-vertical"></div>

                                    <!-- <marquee behavior="" direction="" scrollamount="30">
                                        <div class="loading"></div>                                                                               
                                    </marquee> -->
                                    <div class="non-visible fade-in-1" id="option1">
                                        <?php include('sub-upload-layout/layout-example/1-trim_1-mech.php'); ?>
                                    </div>
                                    <div class="non-visible fade-in-1" id="option2">
                                        <?php include('sub-upload-layout/layout-example/2-trim_1-mech.php'); ?>
                                    </div>
                                    <div class="non-visible fade-in-1" id="option3">
                                        <?php include('sub-upload-layout/layout-example/2-trim_2-mech.php'); ?>
                                    </div>
                                    <div class="non-visible fade-in-1" id="option4">
                                        <?php include('sub-upload-layout/layout-example/2-trim_2-mech_1-finalmech.php'); ?>
                                    </div>
                                </div>
                                <div class="space-vertical"></div>
                                <div class="space-vertical"></div>
                            </div>
                        </div>                   
                    </div>
                </div>
            </div>      
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url('asset/add-layout.js'); ?>"></script>