<div onclick="location.href ='<?php echo base_url();?>index.php/activity/information_manage_layout?id=<?php echo $data['layout_id'] ?>'">
    <div class="searched-layout-box pointer">
        <div class="seccond-section-center">
            <div>
                <div>
                    <div class="space-horizontal"></div>
                    <div class="left">
                        <div class="flex-all">
                            <div>
                                <h4 class="font-style-montserrat-semi-bold no-margin"><?php echo $data['type']; ?></h4>
                            </div>
                            <div class="vertical-line"></div>
                            <div>
                                <h4 class="font-style-montserrat-bold no-margin"> <strong><?php echo $data['jph']; ?> JPH</strong></h4>
                            </div>
                            <div class="vertical-line"></div>
                        <div>
                            <h4 class="font-style-montserrat-bold no-margin"> <strong>Version 1 </strong></h4>
                        </div>
                        </div>
                    </div>
                    <div class="right">
                        <div class="flex-all">
                            <div class="flex-all">
                                <div class="box-info-layout-trimline"></div>
                                <span class="font-style-montserrat-semi-bold font-size-small">Trimline</span>
                            </div>
                            <div class="vertical-line"></div>
                            <div class="flex-all">
                                <div class="box-info-layout-mechline"></div>
                                <span class="font-style-montserrat-semi-bold font-size-small">Mechline</span>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="space-horizontal"></div>
                    <div>
                        <div class="flex">
                            <div class="layout-box">
                                <div class="layout-box-layout layout-box-trimline">
                                    <img src="<?php echo base_url($data['url'] .$data['trimline']) ?>" class="layout-box">
                                    <!-- <h2 class="font-style-montserrat-bold">TRIMLINE</h2> -->
                                </div>
                                <!-- <div class="space-horizontal"></div> -->
                                <div class="layout-box-layout layout-box-trimline">
                                    <img src="<?php echo base_url($data['url'] .$data['trimline']) ?>" class="layout-box">
                                    <!-- <h2 class="font-style-montserrat-bold">TRIMLINE</h2> -->
                                </div>
                            </div>
                            
                            <div class="space-vertical"></div>
                            <div class="layout-box">
                                <div class="layout-box-layout layout-box-mechline">
                                    <img src="<?php echo base_url($data['url'] .$data['mechline']) ?>" class="layout-box">
                                    <!-- <h2 class="font-style-montserrat-bold">MECHLINE</h2> -->
                                </div>
                                <!-- <div class="space-horizontal"></div> -->
                                <div class="layout-box-layout layout-box-mechline">
                                    <img src="<?php echo base_url($data['url'] .$data['mechline']) ?>" class="layout-box">
                                    <!-- <h2 class="font-style-montserrat-bold">MECHLINE</h2> -->
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  