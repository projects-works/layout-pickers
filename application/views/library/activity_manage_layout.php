<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Manage Layout</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <input type="number" class="non-visible" id="index-menu-library" value="1"> 

        <div class="new-body" id="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
            

            <!-- <a href="<?php echo site_url("login/logout")?>"> <h2>home</h2></a> -->

            <div class="body-new" id="body-new">
                <div class="first-section-center">                                        
                    <?php include('library_navbar.php'); ?>

                    <div class="vertical-space"></div>
                    
                    <button class="scroll-to-top pointer" id="scroll-to-top" onclick="scrollToTop()">&#10141;</button>

                    <?php include('sub-manage-layout/frament_listiew_manage_layout.php'); ?>
                    
                </div>
            </div> 
            
            
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url('asset/scroll-view.js'); ?>"></script>