<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Upload Layout</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <input type="number" class="non-visible" id="index-menu-library" value="0"> 

        <div class="new-body">                        
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>            

            <div class="body-new" id="body-new">
                <div class="first-section-center">       
                <?php 
                    $layout_id = $_GET['id_layout']; 
                    $info_id = $_GET['id_info']; 
                    
                ?>
                
                    <div class="general-library-body">
                        <div class="seccond-section-center">
                        <div class="space-vertical"></div>
                        <div class="space-vertical"></div>

                            <h3 class="font-style-montserrat-semi-bold no-margin">Upload drawing for Standard Station.</h3>
                            <!-- <div class="space-vertical"></div> -->
                            <div class="line-horzontal-3"></div>

                            <input type="text" class="non-visible" id="info_id" value="<?php echo $info_id;?>">

                            <select class="select-input font-style-montserrat-semi-bold font-size-small pointer" style="atext-align: center;" id="selected_station" onclick="station_check()">                                                  
                                <?php  include('list_dropdown_station.php') ?>
                            </select>

                            <table id="form">
                                <tr>
                                    <?php echo form_open_multipart('database_method_controller/add_information_station'); ?>
                                    <td>
                                        <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                                    </td>
                                    <td>
                                        <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">
                                        <input type="text"  class="non-visible" name="layout_name" value="2d_drawing">
                                        <input type="text"  class="non-visible" name="field_name" id="field_name1" >
                                        <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                    </td>
                                    <td>
                                        <input type="file" name="file" id="" class="st-input">
                                    </td>                               
                                    <td>                                        
                                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                        font-size-small pointer st-input" type="submit"> 
                                            Upload 
                                        </button>
                                    </td>
                                    <td>
                                        <div id="result1"></div>
                                    </td>
                                    <?php echo form_close();?>
                                </tr>
                                <tr><div class="space-vertical"></div></tr>
                                <tr>
                                    <?php echo form_open_multipart('database_method_controller/add_information_station'); ?>
                                    <td>
                                        <span class="font-style-montserrat-semi-bold font-size-medium">3D Drawing</span>
                                    </td>
                                    <td>
                                        <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">
                                        <input type="text"  class="non-visible" name="layout_name" value="3d_drawing">
                                        <input type="text"  class="non-visible" name="field_name" id="field_name2" >
                                        <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                    </td>
                                    <td>
                                        <input type="file" name="file" id="" class="st-input">
                                    </td>
                                    <td>
                                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                        font-size-small pointer st-input" type="submit"> 
                                            Upload 
                                        </button>    
                                    </td>
                                    <td>
                                        <div id="result2"></div>
                                    </td>
                                    <?php echo form_close();?>
                                </tr>
                                <tr><div class="space-vertical"></div></tr>
                                <tr>
                                    <?php echo form_open_multipart('database_method_controller/add_information_station'); ?>
                                    <td>
                                        <span class="font-style-montserrat-semi-bold font-size-medium">Equipment Matrix</span>
                                    </td>
                                    <td>
                                        <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">
                                        <input type="text"  class="non-visible" name="layout_name" value="equipment_matrix">
                                        <input type="text"  class="non-visible" name="field_name" id="field_name3" >
                                        <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                    </td>
                                    <td>
                                        <input type="file" name="file" id="" class="st-input">
                                    </td>
                                    <td>
                                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                        font-size-small pointer st-input" type="submit"> 
                                            Upload 
                                        </button>    
                                    </td>
                                    <td>
                                        <div id="result3"></div>
                                    </td>
                                    <?php echo form_close();?>
                                </tr>
                            </table>        
                            <div class="right">
                                <div class="flex-all">
                                <a href="<?php echo base_url()."index.php/activity/fragment_upload_layout_2?id_layout=".$layout_id."&id_info=".$info_id?>">
                                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                                font-size-small pointer"> 
                                                Back 
                                            </button>
                                        </a>
                                    <div class="space-vertical"></div>
                                    <div>
                                        <a href="<?php echo base_url()."index.php/activity/library?id_layout=".$layout_id."&id_info=".$info_id?>">
                                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                    font-size-small pointer"> 
                                            Finish 
                                        </button>
                                        </a>
                                    </div>
                                </div>                               
                            </div>
                            <div class="right">
                               
                            </div>
                            <div style="clear:right;"></div>
                        <div class="space-vertical"></div>
                        <div class="space-vertical"></div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>      
        </div>          
        
    </body>
<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script>

    var loadstation1;
    $(document).ready(function()
    {
        // loadstation();
        var field1 = "2d";
        var field2 = "3d";
        var field3 = "em";
        loadstation1 =   function(station,info_id)
                        {
                            $.ajax({                            
                                url     : "<?php echo site_url('ajax_search_method/fragment_upload_layout_3') ?>",
                                method  : "POST",
                                data    : {station:station, info_id:info_id, field:field1},
                                success : function(data)
                                {
                                    $('#result1').html(data);
                                }
                            })

                            $.ajax({                            
                                url     : "<?php echo site_url('ajax_search_method/fragment_upload_layout_3') ?>",
                                method  : "POST",
                                data    : {station:station, info_id:info_id, field:field2},
                                success : function(data)
                                {
                                    $('#result2').html(data);
                                }
                            })

                            $.ajax({                            
                                url     : "<?php echo site_url('ajax_search_method/fragment_upload_layout_3') ?>",
                                method  : "POST",
                                data    : {station:station, info_id:info_id, field:field3},
                                success : function(data)
                                {
                                    $('#result3').html(data);
                                }
                            })
                        }
    });
    
   
</script>
<script src="<?php echo base_url('asset/fragment-upload-3.js'); ?>"></script>
</html>

