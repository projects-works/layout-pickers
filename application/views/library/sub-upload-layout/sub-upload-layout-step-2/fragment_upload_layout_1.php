<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Upload Layout</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <input type="number" class="non-visible" id="index-menu-library" value="0"> 

        <div class="new-body">                        
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>            

            <div class="body-new" id="body-new">
                <div class="first-section-center">       
                <?php 
                    $layout_id = $_GET['id_layout']; 
                    $info_id = $_GET['id_info']; 
                    foreach($layout->result_array() as $data){
                        if($data['layout_id'] == $layout_id){
                ?>
                <input class="non-visible" type="text" id="jph" value="<?php echo $data['jph'] ?>">
                <input class="non-visible" type="text" id="option" value="<?php echo $data['option_layout'] ?>">
                    <div class="general-library-body">
                        <div class="seccond-section-center">
                        <div class="space-vertical"></div>
                        <div class="space-vertical"></div>

                            <h3 class="font-style-montserrat-semi-bold no-margin">Upload image for example layout.</h3>
                            <!-- <div class="space-vertical"></div> -->
                            <div class="line-horzontal-3"></div>
                            
                                <table>
                                    <tr>
                                    <?php echo form_open_multipart('database_method_controller/add_image_layout'); ?>
                                        <td>
                                            <span class="font-style-montserrat-semi-bold font-size-medium">Trimline</span>
                                        </td>
                                        <td>
                                            <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">                        
                                            <input type="text"  class="non-visible" name="layout_name" value="trimline">
                                            <input type="text"  class="non-visible" name="field_name" value="layout_uploaded">
                                            <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                        </td>
                                        <td>
                                            <input type="file" name="file" id="trimline">
                                        </td>                               
                                        <td>
                                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                    font-size-small pointer" type="submit"> 
                                                Upload 
                                            </button>
                                        </td>
                                        <td>
                                        <?php if($data['trimline'] == ""){?>
                                                                                 
                                        <?php }else{ ?>                                            
                                            <span style="font-size: 1.5em; color:green;"> &#10004; </span>
                                        <?php } ?> 
                                        </td>
                                        <?php echo form_close();?>  
                                    </tr>
                                    <tr><div class="space-vertical"></div></tr>
                                    <tr>
                                        <?php echo form_open_multipart('database_method_controller/add_image_layout'); ?>
                                        <td>
                                            <span class="font-style-montserrat-semi-bold font-size-medium">Mechline</span>
                                        </td>
                                        <td>
                                            <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">
                                            <input type="text"  class="non-visible" name="layout_name" value="mechline">
                                            <input type="text"  class="non-visible" name="field_name" value="layout_uploaded">
                                            <input type="text"  class="non-visible" name="layout_id" value="<?php echo $_GET['id_layout']; ?>">
                                        </td>
                                        <td>
                                            <input type="file" name="file" id="">
                                        </td>
                                        <td>
                                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                    font-size-small pointer" type="submit"> 
                                                Upload 
                                            </button>    
                                        </td>
                                        <td>
                                            <?php if($data['mechline'] == ""){?>
                                                                                 
                                            <?php }else{ ?>                                            
                                                <span style="font-size: 1.5em; color:green;"> &#10004; </span>
                                            <?php } ?> 
                                        </td>
                                        <?php echo form_close();?>  
                                    </tr>
                                    <tr><div class="space-vertical"></div></tr>
                                    <tr>
                                        <?php echo form_open_multipart('database_method_controller/add_new_layout'); ?>
                                        <td>
                                            <span class="f-mech font-style-montserrat-semi-bold font-size-medium">F. Mechline</span>
                                        </td>
                                        <td>
                                            <input type="text"  class="non-visible f-mech" name="info_id" value="<?php echo $_GET['id_info']; ?>">
                                            <input type="text"  class="non-visible f-mech" name="layout_name" value="final_mechline">
                                            <input type="text"  class="non-visible f-mech" name="field_name" value="layout_uploaded">
                                            <input type="text"  class="non-visible f-mech" name="layout_id" value="<?php echo $_GET['id_layout']; ?>">
                                        </td>
                                        <td>
                                            <input type="file" name="file" id="" class="f-mech">
                                        </td>
                                        <td>                                        
                                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                    font-size-small pointer f-mech" type="submit"> 
                                                Upload 
                                            </button>    
                                        </td>
                                        <?php echo form_close();?>  
                                    </tr>
                                </table>                
                        
                            <div class="right">
                                <a href="<?php echo base_url()."index.php/activity/fragment_upload_layout_2?id_layout=".$layout_id."&id_info=".$info_id?>">
                                    <button  onclick="window.location.replace()" class="form-button-2 font-style-montserrat-semi-bold
                                                    font-size-small pointer"> 
                                        Next 
                                    </button>
                                </a>                                
                            </div>
                            <div style="clear:right;"></div>
                        <div class="space-vertical"></div>
                        <div class="space-vertical"></div>
                        </div>
                    </div>
                        <?php }} ?>
                </div>
            </div>      
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url('asset/fragment-upload-1.js'); ?>"></script>