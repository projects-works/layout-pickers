<div class="line-horzontal-1"></div>
<div class="line-horzontal-2"></div>
<div class="line-horzontal-3"></div>
<div class="line-horzontal-4"></div>
<div class="general-library-body">
    <div class="seccond-section-center">
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>

        <h3 class="font-style-montserrat-semi-bold no-margin">Upload image layout 3D and 2D.</h3>
        <!-- <div class="space-vertical"></div> -->
        <div class="line-horzontal-3"></div>
        <div class="flex">
            <div style="width:100%;">
                <table>
                    <tr>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">Trimline</span>
                        </td>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">2D</span>
                        </td>
                        <td>
                            <input type="file" name="" id="">
                        </td>                               
                        <td>
                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                            font-size-small pointer"> 
                                Upload 
                            </button>
                        </td>
                    </tr>
                    <tr><div class="space-vertical"></div></tr>
                    <tr>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">Trimline</span>
                        </td>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">3D</span>
                        </td>
                        <td>
                            <input type="file" name="" id="">
                        </td>
                        <td>
                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                            font-size-small pointer"> 
                                Upload 
                            </button>    
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:100%;">
                <table>
                    <tr>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">Mechline</span>
                        </td>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">2D</span>
                        </td>
                        <td>
                            <input type="file" name="" id="">
                        </td>                               
                        <td>
                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                            font-size-small pointer"> 
                                Upload 
                            </button>
                        </td>
                    </tr>
                    <tr><div class="space-vertical"></div></tr>
                    <tr>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">Mechline</span>
                        </td>
                        <td>
                            <span class="font-style-montserrat-semi-bold font-size-medium">3D</span>
                        </td>
                        <td>
                            <input type="file" name="" id="">
                        </td>
                        <td>
                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                            font-size-small pointer"> 
                                Upload 
                            </button>    
                        </td>
                    </tr>
                </table>
            </div>
        </div>                
        
        <div class="right">
            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                            font-size-small pointer"> 
                Next 
            </button>
        </div>
        <div style="clear:right;"></div>
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>
    </div>
</div>