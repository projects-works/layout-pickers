<div class="line-horzontal-1"></div>
<div class="line-horzontal-2"></div>
<div class="line-horzontal-3"></div>
<div class="line-horzontal-4"></div>
<div class="general-library-body">
    <div class="seccond-section-center">
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>

        <h3 class="font-style-montserrat-semi-bold no-margin">Upload image for example layout.</h3>
        <!-- <div class="space-vertical"></div> -->
        <div class="line-horzontal-3"></div>
        
            <table>
                <tr>
                    <form action="" id="submit-layout">
                    <td>
                        <span class="font-style-montserrat-semi-bold font-size-medium">Trimline</span>
                    </td>
                    <td></td>
                    <td>
                        <input type="file" name="trimline" id="trimline">
                    </td>                               
                    <td>
                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                font-size-small pointer" type="submit"> 
                            Upload 
                        </button>
                    </td>
                    </form>
                </tr>
                <tr><div class="space-vertical"></div></tr>
                <tr>
                    <td>
                        <span class="font-style-montserrat-semi-bold font-size-medium">Mechline</span>
                    </td>
                    <td></td>
                    <td>
                        <input type="file" name="" id="">
                    </td>
                    <td>
                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                        font-size-small pointer"> 
                            Upload 
                        </button>    
                    </td>
                </tr>
                <tr><div class="space-vertical"></div></tr>
                <tr>
                    <td>
                        <span class="f-mech font-style-montserrat-semi-bold font-size-medium">F. Mechline</span>
                    </td>
                    <td></td>
                    <td>
                        <input type="file" name="" id="" class="f-mech">
                    </td>
                    <td>
                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                        font-size-small pointer f-mech"> 
                            Upload 
                        </button>    
                    </td>
                </tr>
            </table>                
        
        <div class="right">
            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                            font-size-small pointer"> 
                Next 
            </button>
        </div>
        <div style="clear:right;"></div>
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>
    </div>
</div>
<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script>
    var create_layout;
    $(document).ready(function()
    {

        $('#submit-layout').submit(function(e)
        {
            e.preventDefault();
        //     $.ajax({                            
        //                 url     : "<?php echo site_url('database_method_controller/upload_layout') ?>",
        //                 method  : "POST",
        //                 data    : new FormData(this),
        //                 processData: false,
        //                 contentType: false,
        //                 cache   : false,
        //                 async   : false,
        //                 success : function(data)
        //                 {      
                                                          
        //                     if(data != 0)
        //                     { 
        //                         alert('check');
        //                     }else
        //                     {
        //                         alert('upload layout failed.');
        //                     }
        //                 }
        //             })
        // })    
        $.ajax({                            
                        url     : "<?php echo site_url('database_method_controller/upload_layout') ?>",
                        method  : "POST",
                        secureur: false,
                        fileelementId: 'trimline',
                        dataType: 'json',
                        data    : {'title' : 'check'}
                        success : function(data)
                        {      
                                                          
                            if(data != 0)
                            { 
                                alert('check');
                            }else
                            {
                                alert('upload layout failed.');
                            }
                        }
                    })
        })           
        
    });
</script>