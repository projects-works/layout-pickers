<div class="line-horzontal-1"></div>
<div class="line-horzontal-2"></div>
<div class="line-horzontal-3"></div>
<div class="line-horzontal-4"></div>
<div class="general-library-body">
    <div class="seccond-section-center">
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>

        <h3 class="font-style-montserrat-semi-bold no-margin">Upload drawing for Standard Station.</h3>
        <!-- <div class="space-vertical"></div> -->
        <div class="line-horzontal-3"></div>

        <select class="select-input font-style-montserrat-semi-bold font-size-small pointer" style="atext-align: center;" id="selected_station" onclick="search_station()">                                                  
            <?php  include('list_dropdown_station.php') ?>
        </select>

        <table>
            <tr>
                <td>
                    <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                </td>
                <td></td>
                <td>
                    <input type="file" name="" id="">
                </td>                               
                <td>
                    <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                    font-size-small pointer"> 
                        Upload 
                    </button>
                </td>
            </tr>
            <tr><div class="space-vertical"></div></tr>
            <tr>
                <td>
                    <span class="font-style-montserrat-semi-bold font-size-medium">3D Drawing</span>
                </td>
                <td></td>
                <td>
                    <input type="file" name="" id="">
                </td>
                <td>
                    <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                    font-size-small pointer"> 
                        Upload 
                    </button>    
                </td>
            </tr>
            <tr><div class="space-vertical"></div></tr>
            <tr>
                <td>
                    <span class="font-style-montserrat-semi-bold font-size-medium">Equipment Matrix</span>
                </td>
                <td></td>
                <td>
                    <input type="file" name="" id="">
                </td>
                <td>
                    <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                    font-size-small pointer"> 
                        Upload 
                    </button>    
                </td>
            </tr>
        </table>        
        
        <div class="right">
            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                            font-size-small pointer"> 
                Finish 
            </button>
        </div>
        <div style="clear:right;"></div>
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>
    </div>
</div>