<div class="layout-box-example">    
    <div class="flex">
        <div class="custom-padding-2"  style="width:100%">
            <div class="layout-box-layout layout-box-trimline
                layout-example-2" ></div>            
            <div class="layout-box-layout layout-box-trimline
                layout-example-2" ></div>
        </div>
        <div class="space-vertical"></div>
        <div class="flex" style="width:100%">
            <div class="custom-padding-2" style="width:100%">
                <div class="layout-box-layout layout-box-mechline
                    layout-example" ></div>
                <div class="layout-box-layout layout-box-mechline
                    layout-example" ></div>
            </div>
            <div class="space-vertical"></div>
            <div class="custom-padding-1" style="width:100%">
                <div class="layout-box-layout-6jph-mech
                    layout-example"></div>
            </div>
        </div>
        
    </div>                                                                                
</div>