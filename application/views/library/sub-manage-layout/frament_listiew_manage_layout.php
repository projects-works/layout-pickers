<?php 
if ($layout->num_rows() < 1){ ?>
    <div class="listview">
        <div class="innerlistview">
            <p class="fnt font" style="text-align:center;"> cannot find any layouts in database. </p>
        </div>        
    </div>
<?php
}else
{
    foreach($layout->result_array() as $data){        
        
        if ($data['jph'] == 1.5 && $data['option_layout'] == 1 )
        { 
            
            include("application/views/library/layout-line/1.5.php");
            // echo $data['type'] ;

        }else if ($data['jph'] == 3 && $data['option_layout'] == 1 )
        { 

            include("application/views/library/layout-line/3v-1.php");
            // echo $data['type'] ;

        }else if ($data['jph'] == 3 && $data['option_layout'] == 2 )
        { 

            include("application/views/library/layout-line/3v-2.php");
            // echo $data['type'] ;

        }else if ($data['jph'] == 6 && $data['option_layout'] == 1 )
        {

            include("application/views/library/layout-line/6v-1.php");
            // echo $data['type'] ;

        }else if ($data['jph'] == 6 && $data['option_layout'] == 2 )
        {
            include("application/views/library/layout-line/6v-2.php");
            // echo $data['type'] ;

        }
    }
} ?>