<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Upload Layout</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <input type="number" class="non-visible" id="index-menu-library" value="0"> 

        <div class="new-body">                        
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>            

            <div class="body-new" id="body-new">
                <div class="first-section-center">       
                <?php 
                    $layout_id = $_GET['id_layout']; 
                    $info_id = $_GET['id_info']; 
                    foreach($layout_concept->result_array() as $data){
                        if($data['information_id'] == $info_id){
                ?>
                    <div class="general-library-body">
                        <div class="seccond-section-center">
                        <div class="space-vertical"></div>
                        <div class="space-vertical"></div>

                            <h3 class="font-style-montserrat-semi-bold no-margin">Upload image layout 3D and 2D.</h3>
                            <!-- <div class="space-vertical"></div> -->
                            <div class="line-horzontal-3"></div>
                            <div class="flex">
                                <div style="width:100%;">
                                    <table>
                                        <tr>
                                            <?php echo form_open_multipart('database_method_controller/add_information'); ?>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">Trimline</span>
                                            </td>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">2D</span>
                                            </td>
                                            <td>
                                                <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">                                                
                                                <input type="text"  class="non-visible" name="layout_name" value="trimline_2d">
                                                <input type="text"  class="non-visible" name="field_name" value="layout_concept">
                                                <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                            </td>
                                            <td>
                                                <input type="file" name="file" id="">
                                            </td>                               
                                            <td>
                                                <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                        font-size-small pointer" type="submit"> 
                                                    Upload 
                                                </button>
                                            </td>
                                            <td>
                                                <?php if($data['trimline_2d'] == ""){?>
                                                                                    
                                                <?php }else{ ?>                                            
                                                    <span style="font-size: 1.5em; color:green;"> &#10004; </span>
                                                <?php } ?> 
                                            </td>
                                            <?php echo form_close();?> 
                                        </tr>
                                        <tr><div class="space-vertical"></div></tr>
                                        <tr>
                                            <?php echo form_open_multipart('database_method_controller/add_information'); ?>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">Trimline</span>
                                            </td>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">3D</span>
                                            </td>
                                            <td>
                                                <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">                                                
                                                <input type="text"  class="non-visible" name="layout_name" value="trimline_3d">
                                                <input type="text"  class="non-visible" name="field_name" value="layout_concept">
                                                <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                            </td>
                                            <td>
                                                <input type="file" name="file" id="">
                                            </td>
                                            <td>
                                                <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer" type="submit"> 
                                                    Upload 
                                                </button>    
                                            </td>
                                            <td>
                                                <?php if($data['trimline_3d'] == ""){?>
                                                                                    
                                                <?php }else{ ?>                                            
                                                    <span style="font-size: 1.5em; color:green;"> &#10004; </span>
                                                <?php } ?> 
                                            </td>
                                            <?php echo form_close();?> 
                                        </tr>
                                    </table>
                                </div>
                                <div style="width:100%;">
                                    <table>
                                        <tr>
                                            <?php echo form_open_multipart('database_method_controller/add_information'); ?>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">Mechline</span>
                                            </td>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">2D</span>
                                            </td>
                                            <td>
                                                <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">                                                
                                                <input type="text"  class="non-visible" name="layout_name" value="mechline_2d">
                                                <input type="text"  class="non-visible" name="field_name" value="layout_concept">
                                                <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                            </td>
                                            <td>
                                                <input type="file" name="file" id="">
                                            </td>                               
                                            <td>
                                                <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer" type="submit"> 
                                                    Upload 
                                                </button>
                                            </td>
                                            <td>
                                                <?php if($data['mechline_2d'] == ""){?>
                                                                                    
                                                <?php }else{ ?>                                            
                                                    <span style="font-size: 1.5em; color:green;"> &#10004; </span>
                                                <?php } ?> 
                                            </td>
                                            <?php echo form_close();?> 
                                        </tr>
                                        <tr><div class="space-vertical"></div></tr>
                                        <tr>
                                            <?php echo form_open_multipart('database_method_controller/add_information'); ?>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">Mechline</span>
                                            </td>
                                            <td>
                                                <span class="font-style-montserrat-semi-bold font-size-medium">3D</span>
                                            </td>
                                            <td>
                                                <input type="text"  class="non-visible" name="info_id" value="<?php echo $_GET['id_info']; ?>">                                                
                                                <input type="text"  class="non-visible" name="layout_name" value="mechline_3d">
                                                <input type="text"  class="non-visible" name="field_name" value="layout_concept">
                                                <input type="text"  class="non-visible" name="layout_id" value="<?php echo $layout_id ?>">
                                            </td>
                                            <td>
                                                <input type="file" name="file" id="">
                                            </td>
                                            <td>
                                                <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer" type="submit"> 
                                                    Upload 
                                                </button>    
                                            </td>
                                            <td>
                                                <?php if($data['mechline_2d'] == ""){?>
                                                                                    
                                                <?php }else{ ?>                                            
                                                    <span style="font-size: 1.5em; color:green;"> &#10004; </span>
                                                <?php } ?> 
                                            </td>
                                            <?php echo form_close();?> 
                                        </tr>
                                    </table>
                                </div>
                            </div>                
                            
                            <div class="right">
                                <div class="flex-all">
                                    <div>
                                        <a href="<?php echo base_url()."index.php/activity/fragment_upload_menage_layout_1?id_layout=".$layout_id."&id_info=".$info_id?>">
                                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                                font-size-small pointer"> 
                                                Back 
                                            </button>
                                        </a>
                                    </div>
                                    <div class="space-vertical"></div>
                                    <div>
                                        <a href="<?php echo base_url()."index.php/activity/fragment_upload_menage_layout_3?id_layout=".$layout_id."&id_info=".$info_id?>">
                                            <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                            font-size-small pointer"> 
                                                Next 
                                            </button>
                                        </a>
                                    </div>
                                </div>                               
                            </div>

                            <div style="clear:right;"></div>
                        <div class="space-vertical"></div>
                        <div class="space-vertical"></div>
                        </div>
                    </div>
                    
                        <?php }} ?>
                </div>
            </div>      
        </div>          
        
    </body>
</html>