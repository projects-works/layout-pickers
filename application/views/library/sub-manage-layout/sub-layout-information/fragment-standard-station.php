<div class="general-library-body">    
    <select class="select-input font-style-montserrat-semi-bold font-size-small pointer" style="atext-align: center;" id="selected_station" onclick="search_station()">                                                  
        <?php  include('list_dropdown_station.php') ?>
    </select>
</div>

<input type="text" class="non-visible" id="info_id" value="<?php echo $row['information_id']; ?>">

<div class="space-vertical"></div>

<div id="result_station"></div>


<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script>
    var load_station;
    $(document).ready(function()
    {

        load_station =  function(station, info_id)
                {
                    $.ajax({                            
                        url     : "<?php echo site_url('ajax_search_method/fetch_standard_station') ?>",
                        method  : "POST",
                        data    : {station:station, info_id:info_id},
                        success : function(data)
                        {
                            $('#result_station').html(data);
                        }
                    })
                }
    });

    
</script>