<?php
    foreach($layout->result_array() as $data){
        $file_trim_2d = $data['trimline_2d'];
        $file_trim_3d = $data['trimline_3d'];
        $file_mech_2d = $data['mechline_2d'];
        $file_mech_3d = $data['mechline_3d'];
        if ($id == $row['layout_id'] && $row['information_id'] == $data['information_id']){
            include('fragment-zoomed-image.php');
            echo '<input type="text" class="non-visible" id="id-layout" value="'. $id .'">
            <input type="text" class="data non-visible" value="'. $file_trim_2d .'">
            <input type="text" class="data non-visible" value="'. $file_trim_3d .'">
            <input type="text" class="data non-visible" value="'. $file_mech_2d .'">
            <input type="text" class="data non-visible" value="'. $file_mech_3d .'">
            <input type="text" class="data non-visible" value="'. $url .'">';
        ?> 
<div class="general-library-body">    
    <div class="space-vertical"></div>
    <div class="space-vertical"></div> 
    <div class="space-vertical"></div>                     
    <div class="flex" style="text-align:center;">
        <div style="width:100%">
            <h3 class="font-style-montserrat-semi-bold no-margin">Trimline</h3>
            <div class="body-information-layout-concept">
                <div>
                    <div class="right">
                        <div class="flex-all">
                            <div class="rounded-corousel roundedTrimline pointer"  onclick="corouselCurrentslideTrimline(1)">
                                <span class="font-style-montserrat-semi-bold font-size-small">
                                    2D
                                </span>
                            </div>
                            <div class="space-vertical"></div>
                            <div class="rounded-corousel roundedTrimline pointer"  onclick="corouselCurrentslideTrimline(2)">
                                <span class="font-style-montserrat-semi-bold font-size-small">
                                    3D
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="clear:right;"></div>
                </div>
                <div class="space-vertical"></div> 
                <div class="flex-all">
                    <div>
                        <div>
                            <div class="navbar-position"></div>
                            <div class="prevbtninfo pointer" onclick="corouselCurrentslideTrimline(1)">
                                <span>&#10094;</span>
                            </div>                        
                        </div>
                    </div>
                    <div class="space-vertical"></div>   
                    <div>
                        <div class="corouselbodyTrimline">
                            <image src="<?php echo base_url($url.$file_trim_2d)?>#toolbar=0&navpanes=0&scrollbar=0" \
                            class="trim_and_mech_2d_3d_image fade-in-1 zoom-in" onclick="open_modal_Image(1)">
                        </div>
                        <div class="corouselbodyTrimline">
                            <image src="<?php echo base_url($url.$file_trim_3d)?>#toolbar=0&navpanes=0&scrollbar=0" \
                            class="trim_and_mech_2d_3d_image fade-in-1 zoom-in" onclick="open_modal_Image(2)">
                        </div>
                    </div>
                    <div class="space-vertical"></div>   
                    <div>
                        <div>
                            <div class="navbar-position"></div>
                            <div class="nextbtninfo pointer" onclick="corouselCurrentslideTrimline(0)">
                                <span>&#10095;</span>
                            </div> 
                        </div>
                                           
                    </div>
                </div>                
            </div>
        </div>
        <div class="vertical-line"></div>
        <div style="width:100%">   
            <h3 class="font-style-montserrat-semi-bold no-margin" onclick="getData()">Mechline</h3>      
            <div class="body-information-layout-concept">
                <div>
                    <div class="right">
                        <div class="flex-all">
                            <div class="rounded-corousel roundedMechline pointer"  onclick="corouselCurrentslideMechline(1)">
                                <span class="font-style-montserrat-semi-bold font-size-small">
                                    2D
                                </span>
                            </div>
                            <div class="space-vertical"></div>
                            <div class="rounded-corousel roundedMechline pointer"  onclick="corouselCurrentslideMechline(2)">
                                <span class="font-style-montserrat-semi-bold font-size-small">
                                    3D
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="clear:right;"></div>
                </div>
                <div class="space-vertical"></div> 
                <div class="flex-all">
                    <div>
                        <div class="navbar-position">
                            <div class="prevbtninfo pointer" onclick="corouselCurrentslideMechline(1)">
                                <span>&#10094;</span>
                            </div>                        
                        </div>
                    </div>
                    <div class="space-vertical"></div>   
                    <div>
                        <div class="corouselbodyMechline">
                            <image src="<?php echo base_url($url.$file_mech_2d)?>#toolbar=0&navpanes=0&scrollbar=0" \
                            class="trim_and_mech_2d_3d_image fade-in-1 zoom-in" onclick="open_modal_Image(3)" >
                        </div>
                        <div class="corouselbodyMechline">
                            <image src="<?php echo base_url($url.$file_mech_3d)?>#toolbar=0&navpanes=0&scrollbar=0" \
                            class="trim_and_mech_2d_3d_image fade-in-1 zoom-in" onclick="open_modal_Image(4)" >
                        </div>
                    </div>
                    <div class="space-vertical"></div>   
                    <div>
                        <div class="navbar-position">
                            <div class="nextbtninfo pointer" onclick="corouselCurrentslideMechline(0)">
                                <span>&#10095;</span>
                            </div> 
                        </div>
                                           
                    </div>
                </div>                
            </div>
        </div>
    </div>          
    <div class="space-vertical"></div>         
    <div class="space-vertical"></div> 
    <div class="space-vertical"></div>      
    <div class="space-vertical"></div> 
</div>
<?php }} ?>
<script src="<?php echo base_url('asset/corousel-layout-concept.js'); ?>"></script>
<script>
    base_url = '<?= base_url();?>';
    function zoomed_image(data)
    {
        
        document.getElementById('zoom-img').src = base_url + data;
    }
</script>