<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Manage Layout</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <?php  
            $id = $_GET['id']; 
            foreach($info->result_array() as $data){ 
                $idinfo = $data['information_id'];
            }
            foreach($layoutt->result_array() as $lay){ 
                if($lay['layout_id'] == $id){
                    $type   = $lay['type'];
                    $jph    = $lay['jph'];
                }
            }
        ?>        

        <div class="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
            

            <!-- <a href="<?php echo site_url("login/logout")?>"> <h2>home</h2></a> -->

            <div class="body-new" id="body-new">
                <div class="first-section-center">                                        
                   
                    <div class="general-library-body">
                        <div class="seccond-section-center">
                            <div class="space-vertical"></div>
                            <div>
                                <div class="left">
                                    <button onclick="goBack()"class="font-style-montserrat-semi-bold back-button pointer"><span class="font-size-medium">&#10094;</span> Back</button>
                                </div>
                                <div class="right">
                                    <a href="<?php echo base_url()."index.php/activity/fragment_upload_menage_layout_1?id_layout=".$id."&id_info=".$idinfo?>">
                                        <button  onclick="" class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer"> 
                                            Upload Layout form
                                        </button>
                                    </a>
                                </div>
                                <div style="clear:both;"></div>
                            </div>
                            
                            <div class="space-vertical"></div>
                            <div class="flex">
                                <div style="width:100%" class="flex-all">
                                    <div style="width:100%">
                                        <h3 class="font-style-montserrat-semi-bold no-margin"><?php echo $type ?></h3>
                                    </div>
                                    <div class="vertical-line"></div>
                                    <div style="width:100%">
                                        <h3 class="font-style-montserrat-semi-bold no-margin"><?php echo $jph ?> JPH</h3>
                                    </div>
                                </div>
                                <div style="width:100%">

                                </div>
                                
                            </div>  
                            <div class="space-vertical"></div>
                            <div>
                                <div class="flex">
                                    <div style="width:100%"></div>
                                    <div style="width:100%; text-align:center;" class="flex">
                                        <div style="width:100%" class="corouselListButton pointer" onclick="corouselCurrentslide(1)">
                                            <span  class="font-style-montserrat-semi-bold font-size-small">
                                                Layout Concept
                                            </span> 
                                        </div>
                                        <div style="width:100%" class="corouselListButton pointer" onclick="corouselCurrentslide(2)">
                                            <span  class="font-style-montserrat-semi-bold font-size-small">
                                                Standard Station
                                            </span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                
                    </div>
                    <?php
                    foreach($info->result_array() as $row){                            
                        $url = $row['url_information'];
                        if($row['layout_id'] == $id){
                    ?>
                    <div class="space-vertical"></div>

                    <div class="corouselbody">
                        <?php include('sub-layout-information/fragment-layout-concept.php') ?>
                    </div>                                    
                    
                    <div class="corouselbody">
                        <?php include('sub-layout-information/fragment-standard-station.php') ?>
                    </div>

                    <?php }} ?>
                </div>
            </div> 
            
            
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url('asset/corousel-2.js'); ?>"></script>
<script src="<?php echo base_url('asset/information-layout.js'); ?>"></script>
