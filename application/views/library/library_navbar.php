<div class="flex-all" id="navbar-library">
    <a href="<?php echo site_url('activity/library') ?>"
    class="button-layout-menu first-child-radius font-style-montserrat-semi-bold 
                    font-size-medium pointer navbar-library">
        <div>
            Upload Layout
        </div>      
    </a> 
    <a href="<?php echo site_url('activity/manage_layout') ?>"
    class="button-layout-menu font-style-montserrat-semi-bold 
                    font-size-medium pointer navbar-library">
        <div>
            Manage Layout
        </div>
    </a>
    <a href="<?php echo site_url('activity/create_user') ?>"
    class="button-layout-menu last-child-radius font-style-montserrat-semi-bold 
                    font-size-medium pointer navbar-library">
        <div>
            Create User
        </div>
    </a>
</div>
<script src="<?php echo base_url("asset/navigation-library.js") ?>"></script>