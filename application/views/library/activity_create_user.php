<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">        
        <link rel="stylesheet" href="<?php echo base_url('asset/style.css') ?>">
        <title>Library/Create User</title>
    </head>
    <body>     
        <input type="number" class="non-visible" id="index-menu-content" value="2"> 
        <input type="number" class="non-visible" id="index-menu-library" value="2"> 

        <div class="new-body">
            <?php 
                define("BASE_URL", "application/views/component_content/");
                include(BASE_URL ."header.php"); 
            ?>
            

            <!-- <a href="<?php echo site_url("login/logout")?>"> <h2>home</h2></a> -->

            <div class="body-new" id="body-new">
                <div class="first-section-center">               

                    <?php include('library_navbar.php'); ?>

                    <div class="space-vertical"></div>

                    <div class="general-library-body">
                        <div class="seccond-section-center">
                            <div class="third-section-content">
                                <div class="midle-content">
                                    <div class="outer">
                                        <div class="form-create-user-body">
                                            <div class="form-box-1">
                                                <input type="text" class="form-input-3 fields 
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Username" title="8 words maximum"
                                                id="username" autocomplete="on" required
                                                oninput="count_username()">                                    
                                            </div>
                                            <div class="form-box-1">
                                                <input type="email" class="form-input-3 fields
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Email" 
                                                id="email" autocomplete="on" required>                                    
                                            </div>
                                            <div class="form-box-1">
                                                <input type="password" class="form-input-3 fields
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Password" 
                                                id="password" autocomplete="on" required
                                                oninput="">                                    
                                            </div>
                                            <div class="form-box-1">
                                                <input type="password" class="form-input-3 fields
                                                font-style-montserrat-light font-size-small custom-select-input" 
                                                placeholder="Re-Password" 
                                                id="re-password" autocomplete="on" required
                                                oninput="check_password()">                                    
                                            </div>
                                            <div class="form-box-1">                                
                                                <select name="" id="level" class="form-input-3 pointer
                                                font-style-montserrat-light font-size-small custom-select-input">
                                                    <option value="user">User</option>
                                                    <option value="admin">Admin</option>                                                
                                                </select>
                                            </div>
                                            <div class="space-vertical"></div>
                                            <div class="form-box-1">
                                                <button class="form-button-2 font-style-montserrat-semi-bold
                                                font-size-small pointer" onclick="create_user()">
                                                Create</button>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </div>          
        
    </body>
</html>
<script src="<?php echo base_url();?>asset/jquery/jquery.js"></script>
<script src="<?php echo base_url();?>asset/create-user.js"></script>
<script>
    var saving;
    $(document).ready(function()
    {

        saving =  function(username, email, password, level)
                {
                    $.ajax({                            
                        url     : "<?php echo site_url('database_method_controller/add_new_user') ?>",
                        method  : "POST",
                        data    : {username:username, email:email, password:password, level:level},
                        success : function(data)
                        {                  
                            if(data)
                            {
                                var fields = $(".fileds");
                                for (var i = 0; i < fields.length; i++)
                                {
                                    fields[i].val() = '';
                                }
                                alert('Success to create user');                                
                            }else
                            {
                                alert('failed to craete user, please check all the fields.');
                            }
                        }
                    })
                    
                }
    });
</script>