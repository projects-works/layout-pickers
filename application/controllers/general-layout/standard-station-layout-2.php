<div class="general-library-body">
    <div class="seccond-section-center" style="text-align:center;">
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>
        <?php 
        if($data['3d_drawing']=="")
        {?>
        <div class="flex">
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                <div class="space-vertical"></div>
                <div class="space-vertical"></div>
                <p  class="font-style-montserrat-semi-bold font-size-small"> 2D Drawing not yet uploaded</p>
            </div>
            <div class="vertical-line"></div>
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                <div class="space-vertical"></div>
                <div class="space-vertical"></div>
                <p  class="font-style-montserrat-semi-bold font-size-small"> 3D Drawing not yet uploaded</p>
            </div>
        </div> 

        <?php
        }else
        {?>
        <div class="flex">
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                <div class="space-vertical"></div>
                <div class="layout-image-1">
                    <img src="<?php echo base_url($data['url'].$data['2d_drawing'])?>" class="image pointer">
                </div>                
            </div>
            <div class="vertical-line"></div>
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                <div class="space-vertical"></div>
                <img src="<?php echo base_url($data['url'].$data['3d_drawing'])?>" class="image pointer">
            </div>
        </div> 

        <?php 
        }?>

        <div>
            <div class="space-vertical"></div>
            <span class="font-style-montserrat-semi-bold font-size-medium">Equipment Matrix</span>
            <div class="space-vertical"></div>
            
        </div>    

        <?php
        if($data['equipment_matrix']=="")
        {?>

        <div class="space-vertical"></div>
        <p class="font-style-montserrat-semi-bold font-size-small"> 3D Drawing not yet uploaded</p>

        <?php
        }else
        {
            if($querystation == 'st_08' || $querystation == 'st_16') 
                {?>

            <div class="space-vertical"></div>
            <p class="font-style-montserrat-semi-bold font-size-small"> Equipment Matrix for this station is not available</p>            

            <?php
            }else
            {?>
                                  
            <img src="<?php echo base_url($data['url'].$data['equipment_matrix'])?>" class="image pointer">
        <?php
            }
        }?>
    </div>
</div>