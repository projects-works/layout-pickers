<div class="general-library-body">
    <div class="seccond-section-center" style="text-align:center;">
    <div class="space-vertical"></div>
    <div class="space-vertical"></div>
        <?php 
        if($data['2d_drawing']=="" && $data['3d_drawing']=="" && $data['equipment_matrix']=="")
        {?>
        <div class="flex">
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                <div class="space-vertical"></div>
                <div class="space-vertical"></div>
                <span  class="font-style-montserrat-semi-bold font-size-small"> 2D Drawing not yet uploaded</span>
            </div>
            <div class="vertical-line"></div>
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                <div class="space-vertical"></div>
                <div class="space-vertical"></div>
                <div>

                </div>
                <span  class="font-style-montserrat-semi-bold font-size-small"> 3D Drawing not yet uploaded</span>
            </div>
            <div class="vertical-line"></div>
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">Equipment Matrix</span>
                <div class="space-vertical"></div>
                <div class="space-vertical"></div>
                <div>
                    <span  class="font-style-montserrat-semi-bold font-size-small"> Equipment Matrix not yet uploaded</span>
                </div>                
            </div>
        </div> 

        <?php
        }else
        {?>
        <div class="flex">
        <?php
            if($data['2d_drawing']=="")
            {?>
                <div style="width:100%;">
                    <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div>
                        <span  class="font-style-montserrat-semi-bold font-size-small"> 2D Drawing not yet uploaded</span>
                    </div>
                </div>   

            <?php
            }else 
            {?>
            
            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">2D Drawing</span>
                <div class="space-vertical"></div>
                <div class="layout-image-1">
                    <img src="<?php echo base_url($data['url'].$data['2d_drawing'])?>" class="image pointer">
                </div>                
            </div>

            <?php
            }?>
            <div class="vertical-line"></div>
            <?php
            if($data['3d_drawing']=="")
            {?>
                <div style="width:100%;">
                    <span class="font-style-montserrat-semi-bold font-size-medium">3D Drawing</span>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div>
                        <span  class="font-style-montserrat-semi-bold font-size-small"> 3D Drawing not yet uploaded</span>
                    </div>                
                </div>

            <?php

            }else 
            {?>

            <div style="width:100%;">
                <span class="font-style-montserrat-semi-bold font-size-medium">3D Drawing</span>
                <div class="space-vertical"></div>
                <div class="layout-image-1">
                    <img src="<?php echo base_url($data['url'].$data['3d_drawing'])?>" class="image pointer">
                </div>                
            </div>

            <?php
            }?>
            <div class="vertical-line"></div>
            <?php
            if($data['equipment_matrix']=="")
            {?>
                <div style="width:100%;">
                    <span class="font-style-montserrat-semi-bold font-size-medium"> Equipment Matrix</span>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div>
                        <span  class="font-style-montserrat-semi-bold font-size-small"> equipment_matrix not yet uploaded</span>
                    </div>   
                </div>
                             

            <?php
            }else 
            {
                if($querystation == 'st_08' || $querystation == 'st_16') 
                {?>
                <div style="width:100%;">
                    <span class="font-style-montserrat-semi-bold font-size-medium">Equipment Matrix</span>
                    <div class="space-vertical"></div>
                    <div class="space-vertical"></div>
                    <div>
                        <span  class="font-style-montserrat-semi-bold font-size-small"> Equipment Matrix for this station is not available</span>
                    </div>
                </div>

                <?php
                }else
                {?>
                <div style="width:100%;">
                    <span class="font-style-montserrat-semi-bold font-size-medium">Equipment Matrix</span>
                    <div class="space-vertical"></div>
                    <div class="layout-image-1">
                        <img src="<?php echo base_url($data['url'].$data['equipment_matrix'])?>" class="image pointer">
                    </div>                
                </div>
                <?php
                }
            }?>
        </div> 

        <?php 
        }?>

    <div class="space-vertical"></div>
    <div class="space-vertical"></div>
    </div>
</div>