<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_search_method extends CI_Controller {

    function fetch()
    {
        $output     = '';
        $querytype  = '';   
        $queryjph   = '';
        $this->load->model('database_method_model');
        if($this->input->post('type'))
        {
            $querytype  = $this->input->post('type');
            $queryjph   = $this->input->post('jph');
        }
        if($querytype != '')
        {
            $key   = $this->database_method_model->fetch_data($querytype, $queryjph);
            
            if($key->num_rows() > 0)
            {
                                
                foreach($key->result_array() as $data){ 

                    if(strlen($querytype) == strlen($data['type']))
                    {
                        if ($data['jph'] == 1.5 && $data['option_layout'] == 1 )
                        { 

                            $output .= include('layout-line/1.5.php'); 
            
                        }else if ($data['jph'] == 3 && $data['option_layout'] == 1 )
                        {         
                            $output .= include('layout-line/3v-1.php');                            

                        }else if ($data['jph'] == 3 && $data['option_layout'] == 2 )
                        { 
                            $output .= include('layout-line/3v-2.php');                           

                        }else if ($data['jph'] == 6 && $data['option_layout'] == 1 )
                        {
                            $output .= include('layout-line/6v-1.php');
                            
                        }else if ($data['jph'] == 6 && $data['option_layout'] == 2 )
                        {
                            $output .= include('layout-line/6v-2.php');                            
                        }
                    }                                  
                }
            }else
            {
                $output  .= include('general-layout/blank-search.php');
            }
        }else
        {   
            $output   = '';
        }        
        
        echo $output;
    }

    function fetch_standard_station()
    {
        $output         = '';
        $querystation   = '';   
        $queryinfoid    = '';        
        $this->load->model('database_method_model');
        if($this->input->post('station'))
        {
            $querystation  = $this->input->post('station');
            $queryinfoid  = $this->input->post('info_id'); 
        }
        if($querystation != 'X')
        {
            if($querystation == "X")
            {
                $output  .= include('general-layout/blank-search.php');
            }
            else
            {                
                $key   = $this->database_method_model->fetch_data2($querystation, $queryinfoid);
                if($key->num_rows() > 0)
                {
                    foreach($key->result_array() as $data){                     
                            
                        $output  .= include('general-layout/standard-station-layout.php');
                                            
                    }
                }else
                {
                    $output  .= include('general-layout/blank-search.php');
                }
            }
        }else
        {   
            $output   = '';
        }
        echo $output;
    }

    function fragment_upload_layout_3(){
        $output         = '';
        $querystation   = '';   
        $queryinfoid    = '';        
        $queryfield     = '';
        $this->load->model('database_method_model');
        if($this->input->post('station'))
        {
            $querystation   = $this->input->post('station');
            $queryinfoid    = $this->input->post('info_id'); 
            $queryfield     = $this->input->post('field'); 
        }
        if($querystation != '')
        {
            $key   = $this->database_method_model->fetch_data2($querystation, $queryinfoid);
            if($key->num_rows() > 0)
            {
                foreach($key->result_array() as $data){ 
                    if($queryfield == "2d")
                    {
                        if($data['2d_drawing'] == ""){
                            $output  .=                         
                                    '';
                        }else{
                            $output  .=                        
                                    '<span style="font-size: 1.5em; color:green;"> &#10004; </span>';
                        };
                    }else if($queryfield == "3d")
                    {
                        if($data['3d_drawing'] == ""){
                            $output  .=                         
                                    '';
                        }else{
                            $output  .=                        
                                    '<span style="font-size: 1.5em; color:green;"> &#10004; </span>';
                        };
                    }else if($queryfield == "em")
                    {
                        if($data['equipment_matrix'] == ""){
                            $output  .=                         
                                    '';
                        }else{
                            $output  .=                        
                                    '<span style="font-size: 1.5em; color:green;"> &#10004; </span>';
                        };
                    }
                    
                }
            }else
            {
                $output  .= '';
            }
        }else
        {   
            $output   = '';
        }
        // $output .= '<span style="font-size: 1.5em; color:green;"> &#10004; </span>';
        echo $output;
    }
}

