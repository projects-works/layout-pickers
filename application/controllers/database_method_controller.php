<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Database_method_controller extends CI_Controller {

    function add_new_user()
    {
        $result     = '';

        $userid 	= 	substr(str_shuffle(str_repeat($x=
						'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
						ceil(15/strlen($x)))), 1, 15);
        $username   = $this->input->post('username');
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');
        $level      = $this->input->post('level');
        $datecreated= 	date("y-m-d")."\n";

        $this->load->model('database_method_model');
        if( $username != '' && $email != '' && $password != '')
        {
            $datauser = array('user_id'=>$userid, 'username'=>$username, 
                    'email'=>$email, 'password'=>$password, 'level'=>$level, 'date_created'=>$datecreated);
            $cekdatauser = $this->database_method_model->cekuserexist($email, $username);
            if( !$cekdatauser)
            {
                $result = $this->database_method_model->createaccount('user', $datauser);                
            }
            
        }else
        {
            $result = FALSE;
        }

        echo $result;
	}
	
	function add_new_user()
    {
        $result     = '';

        $userid 	= $this->input->post('user_id');
        $username   = $this->input->post('username');
        $email      = $this->input->post('email');
        $password   = $this->input->post('password');
        $level      = $this->input->post('level');        

        $this->load->model('database_method_model');
        if( $username != '' && $email != '' && $password != '')
        {
			$datauser = array('username'=>$username, 'email'=>$email, 
							'password'=>$password, 'level'=>$level);            
			$result = $this->database_method_model->updateaccount('user', $datauser, $userid);                
            
            
        }else
        {
            $result = FALSE;
        }

        echo $result;
    }

    public function add_new_layout()
    {
        
        $type   = $this->input->post('type');
        $jph    = $this->input->post('jph');
        $option = $this->input->post('option');
        $layoutid 
				= 	substr(str_shuffle(str_repeat($x=
					'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
					ceil(15/strlen($x)))), 1, 15);
		$infoid 
				= 	substr(str_shuffle(str_repeat($x=
					'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
                    ceil(15/strlen($x)))), 1, 15);
        
        $SS_id1
				= 	substr(str_shuffle(str_repeat($x=
					'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
					ceil(15/strlen($x)))), 1, 15);
		$SS_id2
				= 	substr(str_shuffle(str_repeat($x=
					'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
					ceil(15/strlen($x)))), 1, 15);
        $date	= 	date("y-m-d")."\n";
        $userid = 	$this->session->userdata('user_id');

		if ( $type != '' && $jph != '')
		{
			$this->load->model('database_method_model');
			$cekexistinglayout = FALSE;
					// = $this->database_method_model->ceklayoutexist($type, $jph, $option);
			if(!$cekexistinglayout)
			{
			
				
				$url	=	'database/asset/layout/'.$layoutid.'/';
				$url2	=	'database/asset/layout/'.$layoutid.'/'.$infoid.'/';

				
				$data	=	array('layout_id'=>$layoutid, 'user_id'=>$userid,
							'url'=>$url, 'trimline'=>'', 'mechline'=>'', 'final_mechline'=> '', 'type'=>$type,
							'jph'=>$jph, 'option_layout'=>$option, 'date'=>$date);
				$data2	=	array('information_id'=>$infoid,'layout_id'=>$layoutid,'url_information' =>$url2);
				$data3 	=	array('information_id'=>$infoid,'layout_concept_id'=>$SS_id1);
				$data4 	=	array('information_id'=>$infoid,'line_feeding_id'=>$SS_id2);
				$this->database_method_model->create_layout('layout_uploaded', $data);
				$this->database_method_model->uploadInfromationLayout('layout_information', $data2);
				$this->database_method_model->uploadInfromationLayout('layout_concept', $data3);
				$this->database_method_model->uploadInfromationLayout('line_feeding', $data4);
					if(!is_dir('./database/asset/layout/'.$layoutid)){
						mkdir('./database/asset/layout/'.$layoutid,0777,TRUE);
					}

				for ($i = 0; $i<22; $i++)
				{
					$num 		=	$i +1;
					$tablenumber=	$this->generatenumber($i);
					$no_station =	$this->generatenumber($i);
					$stationid	=	substr(str_shuffle(str_repeat($x=
									'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
									ceil(15/strlen($x)))), 1, 15);
					if ($i == 10)
					{
						$station_id_name= 'qgate_01_id';
						$db_name		= 'qgate_01';				
					}else if($i == 20)
					{
						$station_id_name= 'qgate_02_id ';
						$db_name		= 'qgate_02';
					}else if($i == 21)
					{
						$station_id_name= 'sa_panorama_id ';
						$db_name		= 'sa_panorama';
					}else
					{
						$station_id_name= 'st_'.$no_station.'_id';
						$db_name		= 'st_'.$tablenumber;
					}

					if(!is_dir('./database/asset/layout/'.$layoutid.'/information/'.$db_name)){
						mkdir('./database/asset/layout/'.$layoutid.'/information/'.$db_name,0777,TRUE);
					}
					$url_station	= 'database/asset/layout/'.$layoutid.'/information/'.$db_name.'/';
					$data5		= 	array(	$station_id_name 	=> $stationid,
											'information_id'	=> $infoid,
											'2d_drawing'		=> '',
											'3d_drawing'		=> '',
											'equipment_matrix'	=> '',
											'url'				=> $url_station);
					$this->database_method_model->uploadInfromationLayout($db_name, $data5);
				}                
			
					redirect(base_url()."index.php/activity/fragment_upload_layout_1?id_layout=".$layoutid."&id_info=".$infoid);	
			}else
			{
			
			}
		}else
		{
		
		}			                
    }
    public function add_image_layout()
	{
		$file	= 	$_FILES['file']['name'];		
		$idname	= 	$this->input->post('id_name');
		$info_id=	$this->input->post('info_id');
		$layout_name
				=	$this->input->post('layout_name');
		$db_field_name
				=	$this->input->post('field_name');
		$layoutid
				=	$this->input->post('layout_id');
		if($file=='')
		{}else
		{			
			if(!is_dir('./database/asset/layout/'.$layoutid)){
				mkdir('./database/asset/layout/'.$layoutid,0777,TRUE);
			}
			$ext 					= pathinfo($file, PATHINFO_EXTENSION);
			$filename 				= $layout_name.'.'.$ext;
			$config['upload_path']	='./database/asset/layout/'.$layoutid;
			$config['allowed_types']='jpg|png';	
			$config['file_name']	= $filename;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file'))
			{
				$this->session->set_flashdata('alert', '2');	
			}else
			{
				$file 		= $this->upload->data('file_name');
			}
		}
		$data= array($layout_name	=>$file);
		$this->load->model('database_method_model');
		$this->database_method_model->upload_image_layout($db_field_name, $data, $layoutid);
	    redirect(base_url()."index.php/activity/fragment_upload_layout_1?id_layout=".$layoutid."&id_info=".$infoid);	
    }
    public function add_information()
	{
		$SS_id
				= 	substr(str_shuffle(str_repeat($x=
					'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
					ceil(15/strlen($x)))), 1, 15);
		$file	= 	$_FILES['file']['name'];		
		$info_id=	$this->input->post('info_id');
		$idname	= 	$this->input->post('id_name');
		$layout_name
				=	$this->input->post('layout_name');
		$db_field_name
				=	$this->input->post('field_name');
		$layoutid
				=	$this->input->post('layout_id');
		if($file=='')
		{}else
		{			
			if(!is_dir('./database/asset/layout/'.$layoutid.'/'.$info_id)){
				mkdir('./database/asset/layout/'.$layoutid.'/'.$info_id,0777,TRUE);
			}
			$ext 					= pathinfo($file, PATHINFO_EXTENSION);
			$filename 				= $layout_name.'.'.$ext;
			$config['upload_path']	='./database/asset/layout/'.$layoutid.'/'.$info_id;
			$config['allowed_types']='pdf|jpg|png';	
			$config['file_name']	=$filename;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file'))
			{
				$this->session->set_flashdata('alert', '2');	
			}else
			{
				$file 		= $this->upload->data('file_name');
			}
		}
		$url = 'database/asset/layout/'.$layoutid.'/';
		$data= array($layout_name	=>$file);
		$this->load->model('database_method_model');
		$this->database_method_model->upload_information($db_field_name, $data, $info_id);
		redirect(base_url()."index.php/activity/fragment_upload_layout_2?id_layout=".$layoutid."&id_info=".$info_id);	
	}	

	public function add_information_station()
	{
		$SS_id
				= 	substr(str_shuffle(str_repeat($x=
					'0123456789bacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', 
					ceil(15/strlen($x)))), 1, 15);
		$file	= 	$_FILES['file']['name'];		
		$info_id=	$this->input->post('info_id');
		$idname	= 	$this->input->post('id_name');
		$layout_name
				=	$this->input->post('layout_name');
		$db_field_name
				=	$this->input->post('field_name');
		$layoutid
				=	$this->input->post('layout_id');
		if($file=='')
		{}else
		{			
			if(!is_dir('./database/asset/layout/'.$layoutid.'/information/'.$db_field_name.'/')){
				mkdir('./database/asset/layout/'.$layoutid.'/information/'.$db_field_name.'/',0777,TRUE);
			}
			$ext 					= pathinfo($file, PATHINFO_EXTENSION);
			$filename 				= $layout_name.'.'.$ext;
			$config['upload_path']	='./database/asset/layout/'.$layoutid.'/information/'.$db_field_name.'/';
			$config['allowed_types']='pdf|jpg|png';	
			$config['file_name']	=$filename;
			$this->load->library('upload',$config);
			if(!$this->upload->do_upload('file'))
			{
				$this->session->set_flashdata('alert', '2');	
			}else
			{
				$file 		= $this->upload->data('file_name');
			}
		}
		$url = 'database/asset/layout/'.$layoutid.'/information/'.$db_field_name.'/';
		$data= array($layout_name	=>$file);
		$this->load->model('database_method_model');
		$this->database_method_model->upload_information($db_field_name, $data, $info_id);
		redirect(base_url()."index.php/activity/fragment_upload_layout_3?id_layout=".$layoutid."&id_info=".$info_id);	
	}

    public function generatenumber($number)
	{
		if($number < 10)
		{
			$result = '0'.$number;
		}else
		{
			$result = $number;
		}
		return $result;
	}

}