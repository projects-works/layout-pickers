<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {


	public function index()
	{
		$this->load->view('login/activity_login');
	}

	public function home()
	{
		$this->load->view('home/activity_home');
	}
	public function layout()
	{
		$this->load->view('layout/activity_main_line');
	}
	public function mainline()
	{
		$this->load->view('layout/activity_main_line');
	}
	public function information_layout()
	{
		$this->load->model('database_method_model');
		$layout['layout'] = $this->database_method_model->getLayoutInfo('layout_concept');	
		$layout['line'] = $this->database_method_model->getLayoutInfo('line_feeding');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');	
		$layout['layoutt'] = $this->database_method_model->getLayout('layout_uploaded');
		$this->load->view('layout/activity_information', $layout);
	}
	public function eol()
	{
		$this->load->view('layout/activity_eol');
	}
	public function library()
	{
		$this->load->view('library/activity_upload_layout');
	}
	public function manage_layout()
	{
		$this->load->model('database_method_model');
		$layout['layout'] = $this->database_method_model->getLayout2('layout_uploaded');	
		$layout['info'] = $this->database_method_model->getLayout('layout_information');
		$this->load->view('library/activity_manage_layout', $layout);
	}
	public function information_manage_layout()
	{
		$this->load->model('database_method_model');
		$layout['layout'] = $this->database_method_model->getLayoutInfo('layout_concept');	
		$layout['line'] = $this->database_method_model->getLayoutInfo('line_feeding');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');	
		$layout['layoutt'] = $this->database_method_model->getLayout('layout_uploaded');
		$this->load->view('library/sub-manage-layout/activity_menage_information', $layout);
	}
	public function create_user()
	{
		$this->load->view('library/activity_create_user');
	}
	public function usefull_data()
	{
		$this->load->view('usefull_data/activity_usefull_data');
	}
	public function image_information()
	{
		base_url('database/asset/layout/');
	}
	public function edit_profile()
	{
		$this->load->model('database_method_model');
		$data['userData'] = $this->database_method_model->getUserdata('user');	
		$this->load->view('edit_profile/edit-profile', $data);
	}
	public function fragment_upload_layout_1()
	{
		$this->load->model('database_method_model');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');	
		$layout['layout'] = $this->database_method_model->getLayout('layout_uploaded');
		$this->load->view('library/sub-upload-layout/sub-upload-layout-step-2/fragment_upload_layout_1', $layout);
	}
	public function fragment_upload_layout_2()
	{
		$this->load->model('database_method_model');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');
		$layout['layout_concept'] = $this->database_method_model->getLayoutInfo('layout_concept');	
		$this->load->view('library/sub-upload-layout/sub-upload-layout-step-2/fragment_upload_layout_2', $layout);
	}
	public function fragment_upload_layout_3()
	{
		$this->load->model('database_method_model');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');
		$layout['line'] = $this->database_method_model->getLayoutInfo('line_feeding');	
		$this->load->view('library/sub-upload-layout/sub-upload-layout-step-2/fragment_upload_layout_3', $layout);
	}
	public function fragment_upload_menage_layout_1()
	{
		$this->load->model('database_method_model');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');	
		$layout['layout'] = $this->database_method_model->getLayout('layout_uploaded');
		$this->load->view('library/sub-manage-layout/sub-upload-layout-step-2/fragment_upload_layout_1', $layout);
	}
	public function fragment_upload_menage_layout_2()
	{
		$this->load->model('database_method_model');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');
		$layout['layout_concept'] = $this->database_method_model->getLayoutInfo('layout_concept');	
		$this->load->view('library/sub-manage-layout/sub-upload-layout-step-2/fragment_upload_layout_2', $layout);
	}
	public function fragment_upload_menage_layout_3()
	{
		$this->load->model('database_method_model');
		$layout['info'] = $this->database_method_model->getLayout('layout_information');
		$layout['line'] = $this->database_method_model->getLayoutInfo('line_feeding');	
		$this->load->view('library/sub-manage-layout/sub-upload-layout-step-2/fragment_upload_layout_3', $layout);
	}
}
