<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


	public function index()
	{
		$this->load->view('login/activity_login');
	}

	public function auth(){
		$username 	= $this->input->post("uname");
		$password 	= $this->input->post("upass");
		$this->load->model("authenticate");
		$result 	= $this->authenticate->auth($username, $password);
		// $result = "authenticated";
		echo $result;
	}

	public function logout()
	{
		$this->session->set_userdata('username', FALSE);
		$this->session->sess_destroy;
		redirect(login);
	}
}
