var scrollToTopBtn = document.getElementById("scroll-to-top");
document.getElementById('new-body').onscroll = function() 
{
    scrollfuntion();    
}

function scrollfuntion() {
    
    if(document.getElementById('new-body').scrollTop > 140 )
    {        
        scrollToTopBtn.className += " fade-in-1";                       
                        
        scrollToTopBtn.style.display = "block";
    }else 
    {                            
        scrollToTopBtn.style.display = "none";
    }
}

function scrollToTop(){  
    document.getElementById('new-body').scrollTop= 0; 
}