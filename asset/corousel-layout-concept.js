var slideIndex = 1;
corousel();
function corousel(){        
    showSlidesTrimline(slideIndex);
    showSlidesMechline(slideIndex);
}

function corouselCurrentslideTrimline(n){
    showSlidesTrimline(slideIndex = n );
}

function showSlidesTrimline(n){    
    var i;    
    var slides          = document.getElementsByClassName('corouselbodyTrimline');    
    var corouselButton  = document.getElementsByClassName('roundedTrimline');
    if (n> slides.length)
    {
        slideIndex = 1
    }
    if (n<1)
    {
        slideIndex = slides.length;
    }
    for (i = 0; i <slides.length; i++)
    {
        slides[i].style.display = "none";
    }
    for (i=0; i<corouselButton.length; i++)
    {
        corouselButton[i].className = corouselButton[i].className.replace(" active-rounded","");        
    }
    slides[slideIndex-1].style.display      = "block";    
    corouselButton[slideIndex-1].className  += " active-rounded";
}

function corouselCurrentslideMechline(n){
    showSlidesMechline(slideIndex = n );
}

function showSlidesMechline(n){    
    var i;    
    var slides          = document.getElementsByClassName('corouselbodyMechline');    
    var corouselButton  = document.getElementsByClassName('roundedMechline');
    if (n> slides.length)
    {
        slideIndex = 1
    }
    if (n<1)
    {
        slideIndex = slides.length;
    }
    for (i = 0; i <slides.length; i++)
    {
        slides[i].style.display = "none";
    }
    for (i=0; i<corouselButton.length; i++)
    {
        corouselButton[i].className = corouselButton[i].className.replace(" active-rounded","");        
    }
    slides[slideIndex-1].style.display      = "block";    
    corouselButton[slideIndex-1].className  += " active-rounded";
}