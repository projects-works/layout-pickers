var slideIndexnavbar = 3;
var store;
var old_store;
var new_arr = [];
var array_tabs = [];
var currentSlide = 0;
var slideIndex = 1; 
var name_of_things = ['linesupply', 'linebalancing', 'stationreadiness', 'sop']

corousel();

function corousel(){        
    push_array_tabs();    
    showSlidesNavbar(slideIndexnavbar);
    changeBackgroundSlide(currentSlide);
    showSlides(currentSlide);
    showSlidesConcept(currentSlide, name_of_things[currentSlide]);
}

function push_array_tabs(){
    var tabs    = document.getElementsByClassName('name-tabs'); 
    for (var i=0; i<tabs.length; i++)
    {
        array_tabs.push(tabs[i].value);
        
    }    
}

function check_array_tabs(tabsvalue){
    for (var i=0; i<array_tabs.length; i++)
    {
        if(tabsvalue == array_tabs[i])
        {
            tabsvalue = i;
        }        
    }
    return tabsvalue;
}

function corouselCurrentslideNavbar(n){
    showSlidesNavbar(n);    
}

function showSlidesNavbar(n){    
    
    var i;    
    var slides  = document.getElementsByClassName('navbar-useful-data'); 
    var tabs    = document.getElementsByClassName('name-tabs'); 
    var tab;
    store       = slides[n];    
    slideIndexnavbar  = tabs[n].value;
    // alert(tabs[n].value );
    slides[n].remove();      
    if (slides.length == 3)
    {        
        for (var i=0; i<slides.length; i++)
        {
            slides[i].style.display = "block";        
        }
    }else
    {
        if (n == 0)
        {
            tab             = check_array_tabs(old_store);            
            var para        = document.createElement("div");
            para.innerHTML  =   '<div class="navbar-useful-data useful-data-navbar-body pointer '+ 
                                'font-style-montserrat-semi-bold font-size-medium " '+ 
                                'onclick="showCurrentSlide('+ tab +')" id="navbar_'+ tab +'">'+
                                '<span class="">' + old_store+'</span>'+
                                '<input type="text" name="" id="" class="name-tabs"'+
                                'value="' + old_store + '" style="display:none;"></div>';
            document.getElementById("tabs-navbar-menu").appendChild(para.firstChild);              
            for (var i=0; i<slides.length; i++)
            {
                slides[i].style.display = "block";        
            }
            changeBackgroundSlide(currentSlide);
        }else if(n == 2)
        {
            new_arr = [];
            new_arr.push(old_store);
 
            new_arr.push(tabs[0].value);
            new_arr.push(tabs[1].value);
            slides[0].remove();
            slides[0].remove();
                
            // alert(new_arr);
            for (var i = 0; i<new_arr.length; i++)
            {
                tab             = check_array_tabs(new_arr[i]);                
                var para = document.createElement("div");
                para.innerHTML  =   '<div class="navbar-useful-data useful-data-navbar-body pointer '+ 
                                    'font-style-montserrat-semi-bold font-size-medium " '+ 
                                    'onclick="showCurrentSlide('+ tab +')" id="navbar_'+ tab +'">'+
                                    '<span class="">' + new_arr[i]+'</span>'+
                                    '<input type="text" name="" id="" class="name-tabs"'+
                                    'value="' + new_arr[i] + '" style="display:none;"></div>';
                document.getElementById("tabs-navbar-menu").appendChild(para.firstChild);                  
            }
            for (var i=0; i<slides.length; i++)
            {
                slides[i].style.display = "block";        
            }
            changeBackgroundSlide(currentSlide);
        }
        
    }
    old_store = slideIndexnavbar;
    
}
function showCurrentSlide(n){    
    currentSlide = n;
    showSlides(n);
    changeBackgroundSlide(n);
    corouselCurrentslide(1, n);
    // alert(n);
}
function changeBackgroundSlide(n){
    var navbar  = document.getElementsByClassName('navbar-useful-data');
    for(var i = 0; i<navbar.length; i++)
    {
        navbar[i].style.backgroundColor = "white";
    }
    var navbarid = document.getElementById('navbar_'+n);
    if (navbarid)
    {
        navbarid.style.backgroundColor = "teal";
    }
}
function showSlides(n){    
    var i;    
    var slides          = document.getElementsByClassName('corouselbody_usefuldata');        
    for (i = 0; i <slides.length; i++)
    {
        slides[i].style.display = "none";
    }
    slides[n].style.display      = "block";    
}

function corouselCurrentslide(n, x){
    showSlidesConcept(n-1, name_of_things[x]);
    // alert(n);
}

function showSlidesConcept(n, name){    
    var i;    
    var slides          = document.getElementsByClassName('corouselbody_' +name);    
    var rounded         = document.getElementsByClassName('rounded_' +name);
    for (i = 0; i <slides.length; i++)
    {
        slides[i].style.display = "none";
    }
    for (i=0; i<rounded.length; i++)
    {        
        rounded[i].className        = rounded[i].className.replace(" active-round","");
    }
    slides[n].style.display      = "block";    
    rounded[n].className         += " active-round";
}