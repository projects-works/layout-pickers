function triplestrip_on()
{
    var i;
    var strips = document.getElementsByClassName('strip-menu');
    for (i=0; i<strips.length; i++)
    {
        // strips[i].className  = strips[i].className.replace('strip-menu', 'strip-menu-on');
        strips[i].className  +=  ' strip-menu-on';
    }
}
function triplestrip_off()
{
    var i;
    var strips = document.getElementsByClassName('strip-menu');
    for (i=0; i<strips.length; i++)
    {
        strips[i].className  = strips[i].className.replace(' strip-menu-on', '');        
    }
}

function showdropdown(){        
    document.getElementById('dropdown-content-menu').style.display="block";       
}
window.addEventListener("click", function(event) {
    
    if (!event.target.matches('.strip-menu')) {
        if (!event.target.matches('#dropdown-button-menu')) {
            if (!event.target.matches('.dropdown-content-menu' && '.menu-content')) {
                document.getElementById('dropdown-content-menu').style.display="none";
            }
        }
    }       
});

function openmodalsignout(){
    document.getElementById("modal-signout").style.display="block";
}
function close_modal_signout(){
    document.getElementById("modal-signout").style.display="none";

}

function darker(){
    var body_modal = document.getElementsByClassName("body-modal-transparent");
    // body_modal.className += " body-modal-transparent-hover";
    for (var i=0; i<body_modal.length; i++){
        body_modal[i].className = body_modal[i].className.replace("body-modal-transparent", "body-modal-transparent-hover");    
    }
    
   
}
function lightner(){
    var body_modal = document.getElementsByClassName("body-modal-transparent-hover");
    for (var i=0; i<body_modal.length; i++){
        body_modal[i].className = body_modal[i].className.replace("body-modal-transparent-hover","body-modal-transparent");
    }
}