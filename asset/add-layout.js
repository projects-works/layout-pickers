var arrTyp = ["MRA","MFA","MHA","PIH","BEV"];
var option1 = ["option 1", "option 2"];
var option2 = ["option 1", "option 2", "option 3"];
const arrType = [];
const newArrayType = [];
var position = -1;
var positionX = -1;

function add(){
    // alert("button");
    var type = document.getElementById("type-dropdown").value;
    arrType.push(type);
    var newArrType = moveArrayTypes(arrType);
    if (type == "Select type"){
        alert("you didn't select any type");
    }else{
        if(arrType.length > 4){
            alert("maximum of types is 4");
        }else if(checkduplicate(newArrType, type)){
            alert("cannot input the same type");
        }else{
            position = position + 1;
            document.getElementById("inputFormType").value = newArrType;
            getheringType(type, position);
        }
    }
}

function checkduplicate(array, type){
    var count = 0;
    for (var i = 0; i<array.length; i++)
    {
        if(array[i] == type)
        {
            count += 1;
        }
    }
    if (count>1)
    {
        return true;
    }else
    {
        return false;
    }
    
}

function converArrayStringToInteger(array, value){
    for (var i = 0; i<array.length; i++){
        if(value == array[i]){
            return i;
        }
    }
}
function converArrayIntegerToString(array, value){
    return array[value];
}


function getheringType(type, position){

    var x = converArrayStringToInteger(arrTyp, type);
    var y = position;
    var php = "asset/component/image_component/cancel.png";

    var para = document.createElement("div");
    para.innerHTML ='<div class="searchModel flex-all" id="child' + position + '">' +                                         
                    '    <p class="no-margin font-style-montserrat-light font-size-small">' + type + '</p>' +
                    '    <button class="delSearchBut pointer"' +
                    '    id="delsearch" onclick="delAddBut('+ x +','+ y +')"> &#10006 </button> '  +
                    '</div>';
    document.getElementById("flexType").appendChild(para.firstChild);  
}

function delAddBut(type, positionType){
    var parent = document.getElementById("flexType");
    var child = document.getElementById("child" + positionType);
    var newType = converArrayIntegerToString(arrTyp, type);
    // alert(newType)
    position = position - 1;
    for (var i=0; i<arrType.length; i++){
        if(newType == arrType[i]){
            arrType.splice(i,1);
            break;
        }
    }
    var newArrType = moveArrayTypes(arrType);
    parent.removeChild(child);
    document.getElementById("inputFormType").value = newArrType;
}
function moveArrayTypes(array){
    if(newArrayType.length > 3)
    {
        newArrayType.splice(0,1);
    }
    newArrayType.splice(0,2);
    newArrayType.splice(0,1);
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'MRA'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'MFA'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'MHA'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'PIH'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'BEV'){
            newArrayType.push(array[i]);
        }
    }
    return newArrayType;
}

function convertToJPH(){
    var capacity = document.getElementById('capacityLayout').value;
    var selectedoption  = document.getElementById("selectoption").value; 
    var jph = setJPH(capacity);
    document.getElementById('inputFormCapacity').value = jph;
    var option = document.getElementById("selectoption");
    
    if(jph == 1.5){
        option.options[0].disabled = false;
        option.options[1].disabled = true;
    }else if(jph == 3){
        option.options[0].disabled = false;
        option.options[1].disabled = false;
    }else if(jph == 6){
        option.options[0].disabled = false;
        option.options[1].disabled = false;
    }else{
        option.options[0].disabled = false;
        option.options[1].disabled = true;
        option.selectedIndex = "0";
    }
    showlayout(jph, selectedoption);
}
function checklayout()
{
    var selectedoption  = document.getElementById("selectoption").value;
    var jph             = document.getElementById("inputFormCapacity").value;
    showlayout(jph, selectedoption);
}
function showlayout(jph, selectedoption)
{    
    if(!document.getElementById("option1"))
    {
        alert("ok");
    }
    if (jph != 0)
    {
        if (jph == 1.5  && selectedoption == "1")
        {
            document.getElementById("option1").style.display = "block";
            document.getElementById("option2").style.display = "none";
            document.getElementById("option3").style.display = "none";
            document.getElementById("option4").style.display = "none";
        }
        if (jph == 3  && selectedoption == "1")
        {
            document.getElementById("option1").style.display = "block";
            document.getElementById("option2").style.display = "none";
            document.getElementById("option3").style.display = "none";
            document.getElementById("option4").style.display = "none";
        }
        if (jph == 3  && selectedoption == "2")
        {
            document.getElementById("option1").style.display = "none";
            document.getElementById("option2").style.display = "block";            
            document.getElementById("option3").style.display = "none";
            document.getElementById("option4").style.display = "none";
        }
        if (jph == 6 && selectedoption == "1")
        {
            document.getElementById("option1").style.display = "none";
            document.getElementById("option2").style.display = "none";
            document.getElementById("option3").style.display = "block";
            document.getElementById("option4").style.display = "none";
        }
        if (jph == 6 && selectedoption == "2")
        {
            document.getElementById("option1").style.display = "none";
            document.getElementById("option2").style.display = "none";
            document.getElementById("option3").style.display = "none";
            document.getElementById("option4").style.display = "block";
        }
    }else
    {
        document.getElementById("option1").style.display = "none";
        document.getElementById("option2").style.display = "none";
        document.getElementById("option3").style.display = "none";
        document.getElementById("option4").style.display = "none";
    }
    
}

function autoslide(){

}

function setJPH(capacity){
    var jph = 0;
    if(0<capacity && capacity<3501){
        jph = 1.5;
    }else if(3500<capacity && capacity<7201){
        jph = 3;
    }else if(capacity>7200  && capacity<14401){
        jph = 6;
    }

    return jph;
}

function get_data(){
    var type    = document.getElementById('inputFormType').value;
    var jph     = document.getElementById('inputFormCapacity').value;
    var option  = document.getElementById('selectoption').value;

    if (type != "" && jph != "")
    {
        create_layout(type, jph, option);
    }else{
        alert("please fill the form.");
    }
}

function success(){
    var form = document.getElementsByClassName('form-create');
    
    for (var i = 0; i< form.length; i++)
    {
        form[i].disabled = true;
    }
}

function step_1_function(result){
    success();
    var step_1  = document.getElementById('step-1'); 
    var id_lay  = document.getElementById('id-layout').value;
    var id_info = document.getElementById('id-info').value;
    var f_mech  = document.getElementsByClassName('f-mech');
    // alert(id_info);
    if (result != 0)
    {
        if(document.getElementById('inputFormCapacity').value == 6 && document.getElementById('selectoption').value == 2)
        {
            for (var i = 0; i< f_mech.length; i++)
            {
                f_mech[i].style.display = "block";
            }            
        }else
        {
            for (var i = 0; i< f_mech.length; i++)
            {
                f_mech[i].style.display = "none";
            }          
        }
        step_1.style.display = "block";
        step_1.scrollIntoView();
    }
}
