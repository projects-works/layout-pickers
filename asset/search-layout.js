var arrModel = [];
const arrType = [];
const newArrayType = [];
const uniqueArray = [];
var newAmt = 0;
var newJPH = 0;
var position = -1;
var arrMod = ["C Class", "C Class Coupe", "E Class", "E Class Coupe", "S Class", "S Class Maybach", "GLC", "GLC Coupe", "CLS",
            "A Class", "B Class", "GLA", "CLA", "GLB", "GLE", "GLE Coupe", "GLS", "GLS Maybach",
            "C Class H", "C Class Coupe H", "E Class H", "E Class Coupe H", "S Class H", "S Class Maybach H", "GLC H", "GLC Coupe H", "CLS H"];

function search(){
    var model   = document.getElementById('model-dropdown').value;
    var amt     = document.getElementById('capacity').value;
    
    
    position = position + 1;
    if(model == "Select model"){

        alert("You didn't select any model");

    }else if(amt == "" || amt == 0 || amt<0){

        alert("Capacity must be greater than 0");

    }else{

        
        if (arrModel.length > 3){
            alert("4 model is more tha enough");
        }else{     
            arrModel.push(model);        
            type            = modelToType(stringToInt(model));
            arrType.push(type);
            var ArrType     = checkForDuplicateArray(arrType); //deleting duplicate array in arrType
            var newArrType  = moveArrayTypes(ArrType);     
            
            
            newAmt += parseFloat(amt);
            newJPH = setJPH(newAmt);
            displayAssumtion(newJPH, newAmt);
            document.getElementById("searchLayoutwithType").value   = newArrType;
            document.getElementById("searchLayoutwithJPH").value    = newJPH;
            document.getElementById('amount').innerHTML             = newAmt;
            gethering(model, position, amt);
            load(newArrType.toString(), newJPH);
        }
    }   
}

function gethering(model,position, amt){

    var x = position;
    var y = stringToInt(model);
    var z = amt;

    var para = document.createElement("div");
    para.innerHTML =    '<div class="searchModel flex-all" id="child' + position + '">' +                                         
                        '    <p class="no-margin font-style-montserrat-light font-size-small">' + model + '</p>' +
                        '    <button class="delSearchBut pointer"' +
                        '    id="delsearch" onclick="delSearchBut('+ x +','+ y +','+ z +')"> &#10006 </button> '  +
                        '</div>';
    document.getElementById("array-model").appendChild(para.firstChild);  
}
function delSearchBut(positionR, modelInInt, amt){
    var parent = document.getElementById("array-model");
    var child = document.getElementById("child" + positionR);
    model = intToString(modelInInt);

    position = position - 1;
    for (var i = 0; i<arrModel.length; i++){
        if (arrModel[i] == model){
            arrModel.splice(i,1);
            break;
        }
    }

    newAmt              = newAmt - amt;
    newJPH              = setJPH(newAmt);
    parent.removeChild(child);    
    var type            = modelToType(modelInInt);
    var newArrType         = resetArrayType(type);     
    displayAssumtion(newJPH, newAmt);
    load(newArrType.toString(), newJPH);
    
    //result 
    document.getElementById("searchLayoutwithType").value = newArrType;
    document.getElementById("searchLayoutwithJPH").value = newJPH;
    document.getElementById('amount').innerHTML = newAmt;

}
function overtime(amt, annualvolume){
    var amt = amt - annualvolume;
    var x   = amt/250;    
    return Math.round(x);
    
}
function displayAssumtion(jph, amt){
    var cycle   = document.getElementById('cycletime');
    var output  = document.getElementById('outputunit');    
    if (jph != '')
    {

        if(jph == 1.5)
        {            
            cycle.innerHTML = 40;
            output.innerHTML= 12;                        
        }else if(jph == 3)
        {        
            cycle.innerHTML = 20;
            output.innerHTML= 24;  
            
        }else if(jph == 6)
        {
            
            cycle.innerHTML = 10;
            output.innerHTML= 48;  
            
        }
    }else
    {
        cycle.innerHTML = 0;  
        output.innerHTML= 0;              
    }
    
}
function moveArrayTypes(array){
    newArrayType.splice(0,2);
    newArrayType.splice(0,1);
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'MRA'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'MFA'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'MHA'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'PIH'){
            newArrayType.push(array[i]);
        }
    }
    for (var i = 0; i<array.length; i++){
        if (array[i] == 'BEV'){
            newArrayType.push(array[i]);
        }
    }
    return newArrayType;
}

function stringToInt(value){
    for(var i= 0; i<arrMod.length; i++){
        if(value == arrMod[i]){        
            return i;
        }
    }
}

function intToString(value){
    return arrMod[value];
}

function modelToType(position_model){
    if(position_model<9){
        result = "MRA";
    }else if(position_model>8 && position_model<14){
        result = "MFA";
    }else if(position_model>13 && position_model<18){
        result = "MHA";
    }else if(position_model>17 && position_model<26){        
        result = "PIH";
    }
    return result;
}

function setJPH(capacity){
    var jph = 0;
    if(0<capacity && capacity<3501){
        jph = 1.5;
    }else if(3500<capacity && capacity<7201){
        jph = 3;
    }else if(capacity>7200  && capacity<14401){
        jph = 6;
    }

    return jph;
}

function checkForDuplicateArray(array){
    // Loop through array values
    for(i=0; i < array.length; i++){
        if(uniqueArray.indexOf(array[i]) === -1) {
            uniqueArray.push(array[i]);
        }
    }
    return uniqueArray;
}

function resetArrayType(type){
    
    for (var i=0; i<arrType.length; i++){
        if(type == arrType[i]){
            arrType.splice(i,1);
            break;
        }
    }

    var duplicatesfound = countDuplicate(type);
    for (var i=0; i<uniqueArray.length; i++){
        if(arrType.length == 0){
            uniqueArray.splice(0,1);
        }else{
            for (var j=0; j<arrType.length; j++){
                if(type == uniqueArray[i]){
                    if(type != arrType[j]){
                        if(countDuplicate(type) <1 ){
                            uniqueArray.splice(i,1);
                            break;
                        }                        
                    }else{}
                }               
            }
        }        
    }
    // alert(duplicatesfound);
    return uniqueArray;
}

function countDuplicate(type){
    var count = 0;
    for (var i=0; i<arrType.length; i++){
        if(arrType[i] == type){
            count ++;
        }
    }
    return count;
}